﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Drawing;

namespace Downloader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        async void Anim()
        {
            while (true)
            {
                label1.Text = "Downloading \n VMT Engine.";
                label1.Location = new Point((this.Width - label1.Width) / 2, (this.Height - label1.Height) / 2);
                await Task.Delay(1000);
                label1.Text += ".";
                await Task.Delay(1000);
                label1.Text += ".";
                await Task.Delay(1000);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            VersionLabel.Text = Application.ProductVersion + " Beta";
            Anim();

            MessageBox.Show("Auto update features have been disabled");
            Application.Exit();
            return;
           
            if (File.Exists("VMT Engine.exe"))
            {
                var versionInfo = FileVersionInfo.GetVersionInfo("VMT Engine.exe");
                string version = versionInfo.ProductVersion;
                if (version.StartsWith("4."))
                {
                    MessageBox.Show("You have VMT Engine 4 already installed.", "VMT Engine is installed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Application.Exit();
                    return;
                }

                DialogResult dl = MessageBox.Show("Looks like there's already VMT Engine installed. " +
                    "Would you like to proceed anyway?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dl == DialogResult.No)
                {
                    Application.Exit();
                    return;
                }

                File.Delete("VMT Engine.exe");
            }

            DialogResult confirm = MessageBox.Show("Are you sure you want to continue?", "Question", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (confirm == DialogResult.No)
            {
                Application.Exit();
                return;
            }

            if (File.Exists("MaterialSkin.dll"))
                File.Delete("MaterialSkin.dll");

            if (File.Exists("updman.exe"))
                File.Delete("updman.exe");

            if (File.Exists("Ionic.Zip.dll"))
                File.Delete("Ionic.Zip.dll");

            if (File.Exists("engine_x.exe"))
                File.Delete("engine_x.exe");

            DownloadFile(@"http://vmt.kkmr.pl/vmtengine/download/vmtengine.exe", @"VMT Engine.exe");
            DownloadFile(@"http://vmt.kkmr.pl/vmtengine/download/materialskin.dl", "MaterialSkin.dll");
            DownloadFile(@"http://vmt.kkmr.pl/vmtengine/download/updman.exe", "updman.exe");
            DownloadFile(@"http://vmt.kkmr.pl/vmtengine/download/ionic.zip.dl", "Ionic.Zip.dll");
            DownloadFile(@"http://vmt.kkmr.pl/vmtengine/download/engine_x.exe", "engine_x.exe");

            Process.Start(@"VMT Engine.exe");
            Application.Exit();
        }

        void DownloadFile(string From, string To)
        {
            try
            {
                using (WebClient dwn = new WebClient())
                {
                    dwn.DownloadFile(From, To);
                    dwn.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
