﻿using System;
using System.Net;

namespace Project_Continue
{
    class DownloadFile
    {
        public static void GetFile(string URL, string Directory)
        {
            try
            {
                using (WebClient Dnl = new WebClient())
                {
                    Dnl.DownloadFile(URL, Directory);
                    Dnl.Dispose();
                }
            }
            catch (Exception ex)
            {
                Msg.Show("Error has occured: \n\n" + ex.ToString(), "Error");
            }
        }
    }
}
