﻿using System;
using System.Net;
using System.Windows.Forms;
using System.IO;
using Ionic.Zip;
using System.Diagnostics;
using Microsoft.Win32;

namespace Project_Continue
{
    class UpdateDownload
    {
        // to restore update functionality, remove return; or return false; from the first lines of functions
            
        static bool IsThereEngineUpdate()
        {
            return false;

            string VersionFile = "update.txt";
            string SaveTo = @"Update\update.inf";

            DownloadFile.GetFile(@"http://vmt.kkmr.pl/vmtengine/updates/" + VersionFile, SaveTo);

            if (!File.ReadAllText(SaveTo).Contains(UpdateConfig.InternalVersion))
            {
                if (File.ReadAllText(SaveTo).Contains("a") && !Settings.FastRing)
                {
                    File.Delete(SaveTo);
                    return false;
                }
                else
                {
                    File.Delete(SaveTo);
                    return true;
                }
            }
            File.Delete(SaveTo);
            return false;
        }

        public static void LookForUpdate()
        {
            return;

            if (IsThereUpdaterUpdate())
            {
                DownloadFile.GetFile(@"http://vmt.kkmr.pl/vmtengine/updates/updater.zip", @"update\updater.zip");
                UnZip(@"update\updater.zip", @"update");
                File.Delete("updman.exe");
                File.Move(@"update\updman.exe", "updman.exe");
            }
            if (IsThereEngineUpdate())
            {
                update f = new update();
                f.ShowDialog();
            }
        }

        static bool IsThereUpdaterUpdate()
        {
            return false;

            RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine\Updater");
            Key.SetValue("UpdaterUpdateOnly", "true");
            Process.Start("updman.exe").WaitForExit();

            object val = Key.GetValue("UpdaterUpdateNow");

            if (val != null && Key.GetValue("UpdaterUpdateNow").Equals("true"))
                return true;
            else
                return false;
        }

        public static void DownloadUpdate()
        {
            return;

            string Link = @"http://vmt.kkmr.pl/vmtengine/";
            string SaveTo = @"update\vmtengine.zip";

            if (Settings.FastRing)
                Link += "vmtenginebeta.zip";
            else
                Link += "vmtengine.zip";

            DownloadFile.GetFile(Link, SaveTo);
            UnZip(SaveTo, @"update");
        }

        public static void DownloadEngineX()
        {
            return;

            DownloadFile.GetFile("http://vmt.kkmr.pl/vmtengine/updates/enginex.zip", @"update\engine_x.zip");
            UnZip(@"update\engine_x.zip", "update");

            if (File.Exists(@"engine_x.exe"))
                File.Delete(@"engine_x.exe");

            File.Move(@"update\engine_x.exe", "engine_x.exe");
        }        

        static void UnZip (string From, string To)
        {
            ZipFile zip = ZipFile.Read(From);
            zip.ExtractAll(To, ExtractExistingFileAction.OverwriteSilently);
        }
    }
}
