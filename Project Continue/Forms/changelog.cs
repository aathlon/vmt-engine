﻿using System;
using System.Windows.Forms;
using MaterialSkin.Controls;
using System.Diagnostics;
using System.Drawing;

namespace Project_Continue
{
    public partial class changelog : MaterialForm
    {
        public changelog()
        {
            InitializeComponent();
            changelogBut.Left = (this.Width - changelogBut.Width) / 2;
            back.Left = (this.Width - back.Width) / 2;
        }

        private void changelog_Load(object sender, EventArgs e)
        {
            //htmlView.IsWebBrowserContextMenuEnabled = false;
            //htmlView.AllowWebBrowserDrop = true;
            //((Control)htmlView).Enabled = true;
            //htmlView.ScrollBarsEnabled = true;

            if (Misc.IsDarkModeOn)
            {
                this.BackColor = Color.Black;
                back.BackColor = Color.DarkGray;
            }

            if (!Misc.IsDarkModeOn)
            {
                back.BackColor = Color.White;
            }

            //if (!Settings.FastRing)
            //    htmlView.Url = new Uri(String.Format("http://vmt.kkmr.pl/vmtengine/changelog.html"));
            //else if (Settings.FastRing)
            //    htmlView.Url = new Uri(String.Format("http://vmt.kkmr.pl/vmtengine/changprev.html"));
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            Hide();
            Close();
        }

        private void changelogBut_Click(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://gitlab.com/aathlon/vmt-engine/blob/master/CHANGELOG.md");
        }
    }
}
