﻿// Really dumb tool which is supposed to fix VMT Engine
// The full release will have MD5 checksum test (that's why this is beta as for now)

using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace vmtenginerepairtool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            ver_lab.Text = state + version + "\nVMT Engine Version: " + Variables.VMTEngineVersion;
            tabControl1.Appearance = TabAppearance.FlatButtons;
            tabControl1.ItemSize = new Size(0, 1);
            tabControl1.SizeMode = TabSizeMode.Fixed;
            Relocate();
        }

        string state = "Beta ";
        string version = Application.ProductVersion;

        bool material_miss = false;
        bool ionic_miss = false;
        bool updman_miss = false;
        int err_total = 0;

        private void button1_Click(object sender, EventArgs e)
        {
            string l = Environment.NewLine;
            string end = "Press button bellow to fix it.";
            string updman_missT = "updman.exe is missing" + l;

            result_txt.Text = "";

            if (!File.Exists("MaterialSkin.dll"))
            {
                material_miss = true;
                err_total++;
                result_txt.Text += "MaterialSkin.dll is missing" + l;
            }

            if (!File.Exists("Ionic.Zip.dll"))
            {
                ionic_miss = true;
                err_total++;
                result_txt.Text += "Ionic.Zip.dll is missing" + l;
            }

            if (!File.Exists("updman.exe"))
            {
                updman_miss = true;
                err_total++;
                result_txt.Text += updman_missT;
            }

            if (err_total == 0)
            {
                tabControl1.SelectedTab = no_help;
                return;
            }

            tabControl1.SelectedTab = result;
            result_txt.Text += "Total: " + err_total + l;
            result_txt.Text += end + l + l;

            Relocate();
            tabControl1.SelectedTab = result;
        }

        void Relocate()
        {
            label1.Location = new Point(CenterXMath(label1), 0);
            button1.Location = new Point(CenterXMath(button1), 124);
            ver_lab.Location = new Point(0, start.Height - ver_lab.Height);
            button6.Location = new Point(start.Width - button6.Width, start.Height - button6.Height);
            result_txt.Location = new Point(CenterXMath(result_txt), 0);
            button2.Location = new Point(CenterXMath(button2), result_txt.Height);
            label2.Location = new Point(CenterXMath(label2), 0);
            button3.Location = new Point(CenterXMath(button3), 121);
            label3.Location = new Point(CenterXMath(label3), 0);
            button5.Location = new Point(CenterXMath(button5), 80);
            button4.Location = new Point(CenterXMath(button4), button5.Location.Y + button5.Height + 5);

            int CenterXMath(Control sender)
            {
                int objWidth = sender.Width;
                return (start.Width - objWidth) / 2;
            }
        }


        void DownloadFile(string From, string To)
        {
            using (WebClient d = new WebClient())
            {
                d.DownloadFile(From, To);
                d.Dispose();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string source = "http://vmt.kkmr.pl/vmtengine/repair_tool2/";

            try
            {
                if (material_miss == true)
                    DownloadFile(source + "MaterialSkin.dll.exe", "MaterialSkin.dll");

                if (ionic_miss == true)
                    DownloadFile(source + "Ionic.Zip.dll.exe", "Ionic.Zip.dll");

                if (updman_miss == true)
                    DownloadFile(source + "updman.exe", "updman.exe");

                tabControl1.SelectedTab = done;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:\n\n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("VMT Engine.exe");
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string l = Environment.NewLine;

            MessageBox.Show("VMT Engine Repair Tool \n\n" + 
                version +
                "\n\nCode by Athlon from www.athlon.kkmr.pl\n\n" +
                "Icon made by Gregor Cresnar from www.flaticon.com is licensed by CC 3.0 BY",
                "Credits", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
