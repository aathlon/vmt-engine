﻿using System;
using System.Linq;
using System.IO;
using System.Windows.Forms;

namespace Project_Continue
{
    class GameSettings
    {
        public static string credits = File.ReadAllText(@"Options\credits.txt");
        public static int Year;
        public static string Language = File.ReadLines(@"Options\game.dic").Skip(1).Take(1).First().ToString();
        public static string ModName = "F1 " + Year + " VMT";

        public static bool VMTtest ()
        {
            if (!credits.Contains(" Season by VMT Group"))         
                return false;
            

            //Checks what year game is
            while (!credits.Contains("F1 " + Year + " Season by VMT Group"))
            {
                Year++;
                if (Year > 2020) return false;
            }

            ModName = "F1 " + Year + " VMT";            
            return true;
        }

        public static bool FSAA { get => Bool("FSAA", false); set => SetGameSettings.Bool("FSAA", "Config.ini"); }      
        public static bool WindowedMode { get => Bool("WINDOWEDMODE", false); set => SetGameSettings.Bool("WINDOWEDMODE", "Config.ini"); }
        public static bool VSync { get => Bool("VSYNC", false); set => SetGameSettings.Bool("VSYNC", "Config.ini"); }
        public static bool TelemetryMPH { get => Bool("display_in_mph", true); set => SetGameSettings.Bool("display_in_mph	", @"Telemetry\f1_telemetry.cfg"); }
        public static bool VBStrategy { get => Bool("VBSTRATEGY", false); set => SetGameSettings.Bool("VBSTRATEGY", "Config.ini"); }

        public static bool Bool(string Option, bool IsTelemetry)
        {
            string ConfigFile = File.ReadAllText(@"Config.ini");

            if (!IsTelemetry)
            {
                if (ConfigFile.Contains(Option + "=1") || ConfigFile.Contains(Option + "=2"))
                {
                    return true;
                }
            }
            else
            {
                if (File.Exists(@"Telemetry\f1_telemetry.cfg"))
                {
                    ConfigFile = File.ReadAllText(@"Telemetry\f1_telemetry.cfg");
                    if (ConfigFile.Contains(Option + "  =  1"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return false;
        }
    }

    class SetGameSettings
    {
        internal static void Bool(string command, string config)
        {
            try
            {
                string dir = config;

                if (File.ReadAllText(dir).Contains(command + "=0") || File.ReadAllText(dir).Contains(command + "=1") || File.ReadAllText(dir).Contains(command + "=2"))
                {
                    string fal = "=0";
                    string tru = "=1";

                    if (command.Contains("FSAA"))
                    {
                        tru = "=2";
                    }

                    if (File.ReadAllText(dir).Contains(command + fal))
                    {
                        File.WriteAllText(dir, File.ReadAllText(dir).Replace(command + fal, command + tru));
                    }
                    else if (File.ReadAllText(dir).Contains(command + tru))
                    {
                        File.WriteAllText(dir, File.ReadAllText(dir).Replace(command + tru, command + fal));
                    }
                    return;
                }
                if (File.ReadAllText(dir).Contains(command + "=  0") || File.ReadAllText(dir).Contains(command + "=  1"))
                {
                    string fal = "=  0";
                    string tru = "=  1";

                    if (File.ReadAllText(dir).Contains(command + fal))
                    {
                        File.WriteAllText(dir, File.ReadAllText(dir).Replace(command + fal, command + tru));
                    }
                    else if (File.ReadAllText(dir).Contains(command + tru))
                    {
                        File.WriteAllText(dir, File.ReadAllText(dir).Replace(command + tru, command + fal));
                    }
                    return;
                }
                else
                {
                    MessageBox.Show("Couldn't find Config.ini file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + Environment.NewLine + Environment.NewLine + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void ChangeLanguage(string lang)
        {
            try
            {
                string cp = @"Options\Languages\" + lang;
                string ps = @"Options\";

                foreach (string newPath in Directory.GetFiles(cp, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(cp, ps), true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + Environment.NewLine + Environment.NewLine + ex.ToString());
            }
        }
    }

    class PLR
    {
        public static bool ReconnaissanceLap { get => Bool("Reconnaissance", 0, 1); set => SetPLR.Bool("Reconnaissance", 0, 1); }
        public static bool FormationLap { get => Bool("Formation Lap", 0, 1); set => SetPLR.Bool("Formation Lap", 0, 1); }
        public static bool PreventAIControl { get => Bool("No AI Control", 0, 1); set => SetPLR.Bool("No AI Control", 0, 1); }
        public static bool CompareTimeToFirstDriver { get => Bool("Times Comparison", 1, 2); set => SetPLR.Bool("Times Comparison", 1, 2); }
        public static bool ShowChatInCar { get => Bool("Allow Chat In Car", 0, 2); set => SetPLR.Bool("Allow Chat In Car", 0,2); }
        public static bool ExtraConfigurableButtons { get => Bool("Options Num Controls", 26, 44); set => SetPLR.Bool("Options Num Controls", 26, 44); }
        public static bool ShowYourCarInRearview { get => Bool("Self In Rearview", 0, 1); set => SetPLR.Bool("Self In Rearview", 0, 1); }
        public static bool ClosedMPQualifySession { get => Bool("Closed MP Qualify Sessions", 0, 1); set => SetPLR.Bool("Closed MP Qualify Sessions", 0, 1); }
        public static bool TrackLoadCommentary { get => Bool("Track Load Commentary", 0, 1); set => SetPLR.Bool("Track Load Commentary", 0, 1); }
        public static bool LCDStatusMode { get => Bool("LCD Display Modes", 3, 4); set => SetPLR.Bool("LCD Display Modes", 3, 4); }
        public static bool MovingSteeringWheel { get => Bool("Moving Steering Wheel", 0, 1); set => SetPLR.Bool("Moving Steering Wheel", 0 , 1); }

        static bool Bool (string Name, int off, int on)
        {
            if (!File.Exists(@"Save\" + Settings.SelectedProfile + @"\" + Settings.SelectedProfile + @".plr")) return false;
            if (File.ReadAllText(@"Save\" + Settings.SelectedProfile + @"\" + Settings.SelectedProfile + @".plr").Contains(Name + "=\"" + on +"\""))
            {
                return true;
            }
            else if (File.ReadAllText(@"Save\" + Settings.SelectedProfile + @"\" + Settings.SelectedProfile + @".plr").Contains(Name + "=\"" + off + "\""))
            {
                return false;
            }

            return false;
        }
    }

    class SetPLR
    {
        internal static void Bool(string Name, int off, int on)
        {
            string PLR = File.ReadAllText(@"Save\" + Settings.SelectedProfile + @"\" + Settings.SelectedProfile + @".plr");
            
            if (PLR.Contains(Name + "=\"" + off + "\""))
            {
                PLR = PLR.Replace(Name + "=\"" + off + "\"", Name + "=\"" + on + "\"");
                File.WriteAllText(@"Save\" + Settings.SelectedProfile + @"\" + Settings.SelectedProfile + @".plr", PLR);
            }
            else if (PLR.Contains(Name + "=\"" + on + "\""))
            {
                PLR = PLR.Replace(Name + "=\"" + on + "\"", Name + "=\"" + off + "\"");
                File.WriteAllText(@"Save\" + Settings.SelectedProfile + @"\" + Settings.SelectedProfile + @".plr", PLR);
            }
        }
    }
}
