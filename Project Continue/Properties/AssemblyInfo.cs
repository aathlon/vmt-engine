﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("VMT Engine")]
[assembly: AssemblyDescription("The F1 Challenge '99-'02 tool.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Virtual Modding Team & Athlon")]
[assembly: AssemblyProduct("VMT Engine")]
[assembly: AssemblyCopyright("Copyright © Athlon 2018")]
[assembly: AssemblyTrademark("VMT Engine")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("66dd144e-870e-4b44-9a55-02ac3048e6bd")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
//Revision = YYMMDD
[assembly: AssemblyVersion("4.0.2.0")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyFileVersion("4.0.2.0")]
