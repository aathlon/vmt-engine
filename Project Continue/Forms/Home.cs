﻿//Start of works - 06.07.2017 (basing on GamePT)
//3.0 build 17.07.2017
//Release date - 29.07.2017 (3.0)

//3.0.1 Preview 1 build 05.08.2017
//3.0.1 Preview 2 build 21.08.2017
//3.0.1 build 22.08.2017 15:42
//3.0.2 build 22.09.2017 16:15

//Most of the code has been rewritten - Codename Novi
//4.0 Preview 1 build 27.12.2017
//4.0 Preview 2 build 29.12.2017
//4.0 build 31.12.2017

//4.0.1 Preview 1 build 24.01.2018

using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Net.NetworkInformation;
using System.Reflection;
using MaterialSkin.Controls; 
using MaterialSkin;
using System.Diagnostics;
using System.Linq;

namespace Project_Continue
{
    public partial class Home : MaterialForm
    {
        public Home()
        {
            InitializeComponent();

            // Detecting if VMT Engine is running already and if the program is in Program Files
            {
                //Prevents VMT Engine from running two times
                var exists = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Count() > 1;
                if (Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Count() > 1)
                    Process.GetCurrentProcess().Kill();

                string location = Assembly.GetEntryAssembly().Location;
                var programfileX86 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
                var programfile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
                if (location.Contains(programfileX86))
                {
                    Msg.Show("You cannot use VMT Engine in Program Files (x86) directory on C drive. Please move the game to another one", "Error");
                    Application.Exit();
                }
                if (location.Contains(programfile))
                {
                    Msg.Show("You cannot use VMT Engine in Program Files directory on C drive. Please move the game to another one.", "Error");
                    Application.Exit();
                }
            }

            //ver_label name
            if (Version.Build.Contains("0"))
                ver_label.Text = String.Format("{0}.{1}", Version.Major, Version.Minor);
            else
                ver_label.Text = String.Format("{0}.{1}.{2}", Version.Major, Version.Minor, Version.Build);

            // copyright label
            {
                var updmanInfo = FileVersionInfo.GetVersionInfo("updman.exe");
                string updmanVersion = updmanInfo.ProductVersion;

                copyrightLab.Text = 
                    Version.AssemblyCopyright + 
                    "\n\nVMT Engine\n" +
                    Version.Full + " (" + UpdateConfig.InternalVersion + 
                    ")\n\nUpdate Manager\n" +
                    updmanVersion;

                if (File.Exists("engine_x.exe"))
                {
                    var xInfo = FileVersionInfo.GetVersionInfo("engine_x.exe");
                    string xVersion = xInfo.ProductVersion;

                    copyrightLab.Text += "\n\nEngine X\nBeta " + xVersion;
                }

                copyrightLab.Location = new Point((websiteNewBut.Location.X + websiteNewBut.Width / 2) - copyrightLab.Width / 2, GitBtn.Location.Y + GitBtn.Height + 20);
            }

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);

            if (Settings.AlwaysDarkMode)
            {
                AlwaysDarkModeCheck.Checked = true;
                NoDarkModeCheck.Enabled = false;
            }
            
            //Dark mode manager
            {
                int now = DateTime.Now.Hour;
                if (Settings.NoDarkMode)
                {
                    NoDarkModeCheck.Checked = true;
                    AlwaysDarkModeCheck.Enabled = false;
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                }
                else if (now >= 20 | now <= 6 || Settings.AlwaysDarkMode)
                {
                    Misc.IsDarkModeOn = true;
                    DarkThemeControls(this.Controls);
                    materialSkinManager.Theme = MaterialSkinManager.Themes.DARK;                     
                    playAndShutdownVMTEngineToolStripMenuItem.ForeColor = Color.White;
                    bugTrackerToolStripMenuItem.ForeColor = Color.White;
                    setCompatibilityModeToolStripMenuItem.ForeColor = Color.White;
                    toolStripMenuItem1.ForeColor = Color.White;                  
                }
                else
                {
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                }
            }

            //Theme manager
            {
                if (Settings.ThemeBlue)
                {
                    blueAndGrey.Checked = true;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue600, Primary.Blue700, Primary.Blue200, Accent.LightBlue200, TextShade.WHITE);
                }
                else
                {
                    greyOnly.Checked = true;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Grey700, Primary.Grey800, Primary.Grey500, Accent.LightBlue200, TextShade.WHITE);
                }
            }
        }

        void DarkThemeControls (Control.ControlCollection collection)
        {
            foreach (Control ctr in collection)
            {
                if (ctr is Button)
                {
                    (ctr as Button).ForeColor = Color.White;
                }

                if (ctr is ComboBox)
                {
                    (ctr as ComboBox).ForeColor = Color.White;
                    (ctr as ComboBox).BackColor = Color.DimGray;
                }

                DarkThemeControls(ctr.Controls);
            }
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            //STAGE_1
            //ALWAYS ON TOP!!!
            //FIRST LAUNCH
            if (Settings.FirstLaunch)
            {
                welcome f = new welcome();
                Hide();
                f.ShowDialog();
                Close();
            }

            // Form location
            if (Settings.PosX != 0 || Settings.PosY != 0)
            {
                this.Location = new Point(Settings.PosX, Settings.PosY);
            }
            
            //IF dev mode is enabled - show button
            if (Settings.DevModeStatus >= 1)
            {
                //If developer mode is on - enable test panel (experimental)
                if (Settings.DevModeStatus == 2)
                {
                    testPanel.Visible = true;
                    devCheck.Checked = true;
                }
                settingsDev.Visible = true;
                hideDevAvailable.Visible = true;
            }

            //STAGE_2
            //SPLASHES!!
            //new Splash();
            splash.Text = Splash.Text();
            //Game settings
            GameSettingsLoader();       

            //STAGE_3
            //If GotoExperimental is true, open experminental tab
            if (Settings.GotoExperimental)
            {
                settings.SelectedTab = devset;
                Tab.SelectedTab = settingsTab;
                Settings.GotoExperimental ^= true;
            }
            //ditto but with preferences
            if (Settings.GotoPreferences)
            {
                settings.SelectedTab = preferences;
                Tab.SelectedTab = settingsTab;
                Settings.GotoPreferences = Settings.GotoPreferences;
            }

            //DEVELOPER SETTINGS
            //Hide version label
            if (Settings.HideVersion)
            {
                ver_label.Visible = false;
                verNprofVisible.Checked = true;
                ModName.Visible = false;
            }

            //Hide splashes
            if (Settings.HideSplashes)
            {
                splash.Visible = false;
                hideSplashes.Checked = true;
                hideSplash.Checked = true;
            }

            //Makes sure if desktop shortcut exists check box is checked
            if (DesktopShortcut.DoesShortcutExists(GameSettings.ModName))
            {
                deskShortCheck.Checked = true;
            }

            //Create Update directory
            Directory.CreateDirectory(@"update");
            if (!Settings.FastRing)
            {
                stable.Checked = true;
            }
            else if (Settings.FastRing)
            {
                prev.Checked = true;
            }

            //Check "Show Log" button
            if (Settings.ShowLog)
            {
                showLogCheck.Checked = true;
            }
            //Check "Auto close" button
            if (Settings.AutoClose)
            {
                turnOffCheck.Checked = true;
            }

            //Prevents starting the update system twice.
            //Also checks if user is online

            Settings.CheckForUpdates = false;
            // If you want to restore auto update functionality - remove comment bellow
            /*
            if (!Misc.Started)
            {
                try
                {
                    //Check if user is online
                    Ping ping = new Ping();
                    PingReply pingReply = ping.Send("193.143.77.40");

                    //if is online
                    if (pingReply.Status == IPStatus.Success)
                    {
                        if (Settings.CheckForUpdates)
                        {
                            autoUpdCheck.Checked = true;
                            // UpdateDownload.LookForUpdate();
                        }

                        if (!Settings.LanguagePackDownloaded)
                        {
                            Msg.Show("It's recommendated to download game language pack. Would you like to do it now (program may freeze for a few seconds - that's normal)?", "Question", MsgButtons.YesNo);
                            if (Msg.Yes)
                            {
                                LanguagePack.DownloadPack();
                            }
                            Settings.LanguagePackDownloaded = true;
                        }
                    }                 
                }
                catch (Exception)
                {
                    urOffline.Visible = true;
                    settingUpdate.Visible = false;
                    settingAbout.Location = new Point(263, 3);
                    settingsDev.Location = new Point(393, 3);

                    ver_label.Text += " OFFLINE!";
                }

                Misc.Started = true;
            }
            */
            if (Directory.Exists("Temp"))
            {
                DirectoryInfo di = new DirectoryInfo("Temp");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }
        }

        string TiresDir = @"SeasonData\Vehicles\tires_f1.tbc";
        void GameSettingsLoader()
        {
            if (GameSettings.VMTtest())
            {
                ModName.Text = GameSettings.ModName;

                if (GameSettings.Year >= 2014)
                {
                    tirePanel.Visible = true;
                    tireSelect.DropDownStyle = ComboBoxStyle.DropDownList;
                    
                    if (GameSettings.Year == 2016)
                    {
                        TiresDir = @"SeasonData\Vehicles\F1\2016_Pirelli.tbc";
                        tireSelect.Items.Add("Ultra Soft - Super Soft");
                    }
                    string tires_l = File.ReadAllText(TiresDir);
                    string[] tiresOptions = { "!", "Medium - Hard", "Soft - Hard", "Soft - Medium", "Super Soft - Soft", "Ultra Soft - Super Soft"};

                    for (int i = 0; !tires_l.Contains(tiresOptions[i]); i++)
                    {
                        tireSelect.Text = tiresOptions[i + 1];
                    }

                }//Tire support
                else
                {
                    tirePanel.Visible = false;
                }

                //VMT Panel support list
                {
                    vmtPan.Visible = true;

                    if (GameSettings.Year <= 2014)
                    {
                        vmtP.SelectedTab = classic;
                    }
                    else if (GameSettings.Year == 2015)
                    {
                        vmtP.SelectedTab = _15;

                        //RBR Camo
                        {
                            string RBRbmp = "RBR_03.bmp";
                            string RBR = @"SeasonData\Vehicles\F1\Red Bull\";

                            if (File.Exists(RBR + RBRbmp))
                                rbrCamo15.Checked = true;
                        }

                        //Manor
                        {
                            //Bianchi
                            string BianchiHelmet1 = @"F1_MT23H1.bmp";
                            string Manor = @"SeasonData\Vehicles\F1\Manor-Marussia\";

                            if (File.Exists(Manor + BianchiHelmet1))
                                bianchiManor15.Checked = true;

                            //Rossi
                            string veh = "2015_MANOR23.veh";
                            string Rossi = "Alexander Rossi";

                            if (File.ReadAllText(Manor + veh).Contains(Rossi))
                            {
                                rossiManor15.Checked = true;
                            }
                        }

                        //McLaren
                        {
                            string mcl = @"SeasonData\Vehicles\F1\McLaren\";
                            string vehMcl = "2015_McLaren06.veh";
                            string mag = "Kevin Magnussen";

                            //Magnussen
                            if (File.ReadAllText(mcl + vehMcl).Contains(mag))
                                magnussenMcl15.Checked = true;
                        }
                    }
                    else if (GameSettings.Year == 2016)
                    {
                        vmtP.SelectedTab = _16;

                        //Red Bull
                        {
                            string rbr = @"SeasonData\Vehicles\F1\Red Bull\";
                            string str = @"SeasonData\Vehicles\F1\Toro Rosso\";
                            string kvyatSTRVeh = @"2016_STR26.veh";
                            string verstappenRBRVeh = @"2016_RBR33.veh";

                            if (File.Exists(rbr + verstappenRBRVeh) && File.Exists(str + kvyatSTRVeh))
                                swapKvyVer.Checked = true;
                        }

                        //Mclaren
                        {
                            string vandoorneVeh = @"2016_McLaren47.veh"; ;
                            string mcl = @"SeasonData\Vehicles\F1\McLaren\";

                            if (File.Exists(mcl + vandoorneVeh))
                                replaceAloVan.Checked = true;
                        }
                    }
                    else if (GameSettings.Year == 2017)
                    {
                        vmtP.SelectedTab = _17;
                    }
                }
            }
            else
            {
                ModName.Visible = false;
            }

            //Language of game
            {
                langSelect.DropDownStyle = ComboBoxStyle.DropDownList;
                string[] langs = {"!", "ENG_lang", "finnish_lang", "french_lang", "german_lang", "hun_lang", "italian_lang", "pol_lang", "por_lang", "spanish_lang" };
                string[] options = {"!", "English", "Finnish", "French", "German", "Hungarian", "Italian", "Polish", "Portuguese", "Spanish"};

                for (int i = 0; !GameSettings.Language.Contains(langs[i]); i++)
                {
                    langSelect.Text = options[i + 1];
                }
            }

            //Config file checkboxes
            {
                if (GameSettings.FSAA)
                    antiAliasignF1CLab.Checked = true;

                if (GameSettings.WindowedMode)
                    windowedF1CLab.Checked = true;

                if (GameSettings.VSync)
                    vsyncF1CLab.Checked = true;

                if (GameSettings.TelemetryMPH)
                    mphTelF1CLab.Checked = true;

                if (Directory.Exists("MovieD"))
                    vidF1c.Checked = true;
            }

            //Adding the saves to lists
            {
                // Reset
                {
                    foreach (Control ctl in plr.Controls)
                    {
                        if (ctl is MaterialCheckBox) (ctl as CheckBox).Checked = false;
                    }
                    QuickChatProfiles.Items.Clear();
                    ProfileChatProfiles.Items.Clear();
                }

                string[] Directories = Directory.GetDirectories("Save");
                foreach (var dir in Directories)
                {
                    string Save = dir.Substring(5, dir.Length - 5);
                    QuickChatProfiles.Items.Add(Save);
                    ProfileChatProfiles.Items.Add(Save);
                }

                QuickChatProfiles.DropDownStyle = ComboBoxStyle.DropDownList;
                QuickChatProfiles.Text = Settings.SelectedProfile;
                ProfileChatProfiles.DropDownStyle = ComboBoxStyle.DropDownList;
                ProfileChatProfiles.Text = Settings.SelectedProfile;
            }

            // Loading PRL settings
            {
                if (Directory.Exists(@"Save\" + Settings.SelectedProfile))
                {
                    CheckBox[] boxes = { ReconnaissanceLap, FormationLap, PreventAI, CompareTimeToFirstDriver,
                        ShowChat, ExtraConfigurableButtons, ShowYourCar, ClosedMP, TrackLoadCommentary, LCDMode, MovingSteeringWheeleck };

                    Boolean[] bools = { PLR.ReconnaissanceLap, PLR.FormationLap, PLR.PreventAIControl, PLR.CompareTimeToFirstDriver, PLR.ShowChatInCar,
                    PLR.ExtraConfigurableButtons, PLR.ShowYourCarInRearview, PLR.ClosedMPQualifySession, PLR.TrackLoadCommentary, PLR.LCDStatusMode, PLR.MovingSteeringWheel};

                    for (int i = 0; i < boxes.ToList().Count; i++)
                    {
                        boxes[i].Checked = bools[i];
                    }
                }
            }
        }

        private void ver_label_Click(object sender, EventArgs e)
        {
            Tab.SelectedTab = settingsTab;
            settings.SelectedTab = informations;
        }

        private void materialCheckBox2_Click(object sender, EventArgs e)
        {
            GameSettings.WindowedMode ^= true;
        }

        private void materialCheckBox1_Click(object sender, EventArgs e)
        {
            GameSettings.FSAA ^= true;
        }

        private void materialCheckBox3_Click(object sender, EventArgs e)
        {
            GameSettings.TelemetryMPH ^= true;
        }

        private void materialCheckBox4_Click(object sender, EventArgs e)
        {
            GameSettings.VSync ^= true;
        }

        private void materialRaisedButton19_Click(object sender, EventArgs e)
        {
            UpdateDownload.LookForUpdate();
        }

        private void materialRaisedButton20_Click(object sender, EventArgs e)
        {
            changelog f = new changelog();
            f.ShowDialog();
        }

        private void materialCheckBox8_Click(object sender, EventArgs e)
        {
            Settings.CheckForUpdates ^= true;
        }

        private void materialCheckBox7_Click(object sender, EventArgs e)
        {
            Settings.AutoClose ^= true;
        }

        private void materialCheckBox6_Click(object sender, EventArgs e)
        {
            Settings.ShowLog ^= true;
        }

        void Cleaner()
        {
            if (Directory.Exists("Update"))
            {
                DirectoryInfo di = new DirectoryInfo("Update");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }
        }
     
        private void playTile_Click(object sender, EventArgs e) //Play button
        {
            string question = @"conf\CompatibilityQuestion.cfg";

            if (!File.Exists(question))
            {
                Msg.Show("Looks like you haven't set compatibility mode. Would you like to do this now? " +
                    "(You can always do it by right-clicking on the ''Play'' button and selecting ''Compatibility Mode'' option.", "Question", MsgButtons.YesNo);

                if (Msg.Yes)
                {
                    File.Create(question).Close();

                    oNToolStripMenuItem.PerformClick();
                    return;
                }
                else if (Msg.No)
                    File.Create(question).Close();               
            }

            try
            {
                Cleaner();
                
                if (Settings.AutoClose)
                    Application.Exit();
            }
            catch (Exception ex)
            {
                Msg.Show("Error while trying to open the game\n\n" + ex.ToString(), "Error");
            }
        }


        private void simptTile_Click(object sender, EventArgs e)
        {
            string location = Assembly.GetEntryAssembly().Location;
            string executableDirectory = Path.GetDirectoryName(location);

            Process.Start(executableDirectory);
        }

        private void gamedirTile_Click(object sender, EventArgs e)
        {
            Process.Start(Directory.GetCurrentDirectory());
        }

        private void testUpdateWindowTile_Click(object sender, EventArgs e)
        {
            update f = new update();
            f.ShowDialog();
        }

        private void testWelcomeTile_Click(object sender, EventArgs e)
        {
            welcome f = new welcome();
            f.ShowDialog();
        }

        private void back_Click(object sender, EventArgs e)
        {
            settings.SelectedTab = main;
        }

        private void settingGames_Click(object sender, EventArgs e)
        {
            settings.SelectedTab = games;
        }

        private void settingUpdate_Click(object sender, EventArgs e)
        {
            settings.SelectedTab = updates;
        }

        private void settingAbout_Click(object sender, EventArgs e)
        {
            settings.SelectedTab = informations;
        }

        private void TabSelector_Click(object sender, EventArgs e)
        {
            settings.SelectedTab = main;
            game.SelectedTab = basic;
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            settings.SelectedTab = devset;
        }

        int countCopyright;
        private void copyrightLab_Click(object sender, EventArgs e)
        {
            if (Settings.DevModeStatus == 0)
            {
                if (websiteNewBut.Text.Contains("Dev's website"))
                {
                    countCopyright = 4;
                    websiteNewBut.Text = countCopyright.ToString();
                    return;
                }

                if (countCopyright != 1)
                {
                    countCopyright--;
                    websiteNewBut.Text = countCopyright.ToString();
                    return;
                }

                if (countCopyright == 1)
                {
                    websiteNewBut.Text = "Click now";
                    return;
                }
            }
            else
            {
                Msg.Show("Developer Settings are already enabled. " +
                    "You can turn them off in Developer Settings by pressing 'Disable Dev Settings'.", "Information");
            }
        }

        void LoadExperimental()
        {
            SaveFormPosition();
            Settings.GotoExperimental ^= true;
            Home f = new Home();
            Hide();
            f.ShowDialog();
            Close();
        }

        void LoadPreferences()
        {
            SaveFormPosition();
            Settings.GotoPreferences ^= true;
            Home f = new Home();
            Hide();
            f.ShowDialog();
            Close();
        }

        private void verNprofVisible_Click_1(object sender, EventArgs e)
        {
            Settings.HideVersion ^= true;
            LoadExperimental();
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            LoadExperimental();
        }

        private void forceStarters_Click(object sender, EventArgs e)
        {
            Misc.Started = false;
            LoadExperimental();
        }

        private void devCheck_Click_1(object sender, EventArgs e)
        {
            if (Settings.DevModeStatus == 1)
            {
                Settings.DevModeStatus = 2;
                LoadExperimental();
                return;
            }
            else if (Settings.DevModeStatus == 2)
            {
                Settings.DevModeStatus = 1;
                LoadExperimental();
                return;
            }
        }

        private void hideDevAvailable_Click_1(object sender, EventArgs e)
        {
            if (Settings.DevModeStatus == 2 || Settings.HideSplashes || Settings.HideVersion)
            {
                Msg.Show("You can't perform that operation, while any developer setting is enabled.", "Error");
                return;
            }
            
            Settings.DevModeStatus = 0;

            Home f = new Home();
            Hide();
            f.ShowDialog();
            Close();
        }

        private void websiteNewBut_Click(object sender, EventArgs e)
        {
            if (websiteNewBut.Text.Contains("Click now"))
            {
                Msg.Show("You can now access developer mode in Experimental settings.", "Done");
                Settings.DevModeStatus = 1;
                Home f = new Home();
                Hide();
                f.ShowDialog();
                Close();
            }
            else
            {
                ExternalLinks.Run("http://athlon.kkmr.pl");
            }
        }

        private void tile1_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("3DConfig.exe");
            }
            catch (Exception)
            {
                Msg.Show("3DConfig.exe doesn't exists", "Error");
            }
        }

        private void tile2_Click(object sender, EventArgs e)
        {
            try
            {
                string trace = @"LOG\trace.txt";
                string exe = "F1 Challenge 99-02.exe";
                string com = "-trace=1000";

                Cleaner();

                if (Settings.ShowLog)
                {
                    Process.Start(exe, com).WaitForExit();
                    Process.Start(trace);
                }
                else
                {
                    Process.Start(exe, com);
                }
            }
            catch (Exception ex)
            {
                Msg.Show("Error:" + Environment.NewLine + Environment.NewLine + ex.ToString(), "Error");
            }
        }

        private void tile3_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(@"Telemetry"))
            {
                Msg.Show("Telemetry program is missing", "Error");
                return;
            }
            Process.Start(@"Telemetry\f1_telemetry.exe");
        }

        private void tile4_Click(object sender, EventArgs e)
        {
            try
            {
                string log = @"LOG\trace.txt";

                if (File.Exists(log))
                    Process.Start(log);
                else
                    Msg.Show("Couldn't find log file.", "Error");
            }
            catch (Exception ex)
            {
                Msg.Show("Error:" + Environment.NewLine + Environment.NewLine + ex.ToString(), "Error");
            }
        }

        private void tile5_Click(object sender, EventArgs e)
        {
            Tab.SelectedTab = gameTab;
            game.SelectedTab = plr;
        }

        string QuickChatReader(int i)
        {
            string Reading = File.ReadLines(@"Save\" + Settings.SelectedProfile + @"\quickchattext.txt").Skip(i).Take(1).First();
            return Reading.Substring(13, Reading.Length - 14);
        }

        private void tile6_Click(object sender, EventArgs e)
        {
            if (File.Exists(@"Save\" + Settings.SelectedProfile + @"\quickchattext.txt"))
            {
                MaterialSingleLineTextField[] txtBoxes = { one, two, three, four, five, six, seven, eight, nine, ten };

                for (int i = 0; i < 10; i++)
                {
                    txtBoxes[i].Text = QuickChatReader(i);
                }
            }
            else if (!File.Exists(@"Save\" + Settings.SelectedProfile + @"\quickchattext.txt"))
            {
                foreach (Control text in quick_chat.Controls)
                {
                    if (text is MaterialSingleLineTextField) (text as MaterialSingleLineTextField).Text = "";
                }
            }

            Tab.SelectedTab = gameTab;
            game.SelectedTab = quick_chat;
        }

        private void hideSplashes_Click(object sender, EventArgs e)
        {
            Settings.HideSplashes ^= true;
        }
  
        private void stable_Click(object sender, EventArgs e)
        {
            Settings.FastRing = false;
        }

        private void prev_Click(object sender, EventArgs e)
        {
            Msg.Show("You're about to enable Preview Channel - they may be unstable and cause a problems." +
                " We're not responsible for breaking your game, system, thermonuclear war, or getting you fired. " +
                "Just kidding, but seriously, we're not recommending this for everyone. Are you sure you want to continue?", "Warning", MsgButtons.YesNo);
            if (Msg.Yes)
            {
                Settings.FastRing = true;
            }
            else
            {
                Settings.FastRing = false;
                stable.Checked = true;
            }
        }

        private void vmtFBBut_Click(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://facebook.com/f1chworld");
        }

        private void vmtYTbut_Click(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://youtube.com/f1ccenter");
        }

        private void swapKvyVer_Click(object sender, EventArgs e)
        {
            string kvyatRBR = @"Stuffs\Kvy-Ver\Kvyat\RBR\";
            string kvyatRBRVeh = @"2016_RBR26.veh";
            string kvyatSTR = @"Stuffs\Kvy-Ver\Kvyat\STR\";
            string kvyatSTRVeh = @"2016_STR26.veh";

            string verstappenRBR = @"Stuffs\Kvy-Ver\Verstappen\RBR";
            string verstappenRBRVeh = @"2016_RBR33.veh";
            string verstappenSTR = @"Stuffs\Kvy-Ver\Verstappen\STR";
            string verstappenSTRVeh = @"2016_STR33.veh";

            string rbr = @"SeasonData\Vehicles\F1\Red Bull\";
            string str = @"SeasonData\Vehicles\F1\Toro Rosso\";

            string[] verstappenRBRfiles = { "2016_RBR33.veh", "2016_RBR33Icon.bmp", @"team.mas", @"RBRAMainBk.jpg" };
            string[] verstappenSTRfiles = { "2016_STR33.veh", "2016_STR33Icon.bmp", @"team.mas", @"STRAMainBk.jpg" };
            string[] kvyatRBRfiles = { "2016_RBR26.veh", "2016_RBR26Icon.bmp", @"team.mas", @"RBRAMainBk.jpg" };
            string[] kvyatSTRfiles = { "2016_STR26.veh", "2016_STR26Icon.bmp", "Generic_str.gen" };

            if (File.Exists(rbr + kvyatRBRVeh) && File.Exists(str + verstappenSTRVeh))
            {
                for (int i = 0; i < kvyatRBRfiles.Count(); i++)
                {
                    File.Delete(rbr + kvyatRBRfiles[i]);
                }

                for (int i = 0; i < verstappenSTRfiles.Count(); i++)
                {
                    File.Delete(rbr + verstappenSTRfiles[i]);
                }

                foreach (string newPath in Directory.GetFiles(verstappenRBR, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(verstappenRBR, rbr), true);

                foreach (string newPath in Directory.GetFiles(kvyatSTR, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(kvyatSTR, str), true);
            }
            else if (File.Exists(rbr + verstappenRBRVeh) && File.Exists(str + kvyatSTRVeh))
            {
                for (int i = 0; i < verstappenRBRfiles.Count(); i++)
                {
                    File.Delete(rbr + verstappenRBRfiles[i]);
                }

                for (int i = 0; i < kvyatSTRfiles.Count(); i++)
                {
                    File.Delete(rbr + kvyatSTRfiles[i]);
                }

                foreach (string newPath in Directory.GetFiles(kvyatRBR, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(kvyatRBR, rbr), true);

                foreach (string newPath in Directory.GetFiles(verstappenSTR, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(verstappenSTR, str), true);
            }
        }

        private void replaceAloVan_Click(object sender, EventArgs e)
        {
            string alonso = @"Stuffs\ALO-VAN\Alonso\";
            string alonsoVeh = @"2016_McLaren14.veh";

            string vandoorne = @"Stuffs\ALO-VAN\Vandoorne\";
            string vandoorneVeh = @"2016_McLaren47.veh";

            string mcl = @"SeasonData\Vehicles\F1\McLaren\";

            string[] alonsoFiles = { "2016_McLaren14.veh", "2016_McLaren14Icon.bmp", "McLarenAMainBk.jpg", "team.mas" };
            string[] vandoorneFiles = { "2016_McLaren47.veh", "2016_McLaren47Icon.bmp", "McLarenAMainBk.jpg", "team.mas" };

            if (File.Exists(mcl + alonsoVeh))
            {
                for (int i = 0; i < alonsoFiles.Count(); i++)
                {
                    File.Delete(mcl + alonsoFiles[i]);
                }

                foreach (string newPath in Directory.GetFiles(vandoorne, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(vandoorne, mcl), true);
            }
            else if (File.Exists(mcl + vandoorneVeh))
            {
                for (int i = 0; i < vandoorneFiles.Count(); i++)
                {
                    File.Delete(mcl + vandoorneFiles[i]);
                }

                foreach (string newPath in Directory.GetFiles(alonso, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(alonso, mcl), true);
            }
        }

        private void rbrCamo15_Click(object sender, EventArgs e)
        {
            string camo = @"Stuffs\CamoBull\";
            string rbr = @"SeasonData\Vehicles\F1\Red Bull\";

            string f1 = "RBR_03.bmp";
            string[] files = { "RBR_03.bmp", "RBR_03EXTRA2.bmp", "RBR_26.bmp", "RBR_26EXTRA2.bmp" };

            if (!File.Exists(rbr + f1))
            {
                foreach (string newPath in Directory.GetFiles(camo, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(camo, rbr), true);
            }
            else if (File.Exists(rbr + f1))
            {
                for (int i = 0; i <= 4; i++)
                {
                    File.Delete(rbr + files[i]);
                }
            }
        }

        private void bianchiManor15_Click(object sender, EventArgs e)
        {
            string bianchi = @"Stuffs\Jules\";
            string bianchiHel1 = @"F1_MT23H1.bmp";
            string bianchiHel2 = @"F1_MT23H2.bmp";
            string bianchiTxt1 = @"MANOR_98.bmp";

            string merhi = @"Stuffs\Jules\normal";

            string manor = @"SeasonData\Vehicles\F1\Manor-Marussia\";

            if (File.Exists(manor + bianchiHel1))
            {
                File.Delete(manor + bianchiHel1);
                File.Delete(manor + bianchiHel2);
                File.Delete(manor + bianchiTxt1);

                foreach (string newPath in Directory.GetFiles(merhi, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(merhi, manor), true);
            }
            else if (!File.Exists(manor + bianchiHel1))
            {             
                if (rossiManor15.Checked == true)
                {
                    Msg.Show("Disable 'Swap Roberto Merhi with Alexander Rossi in Manor' option first!", "Warning");
                    return;
                }

                foreach (string newPath in Directory.GetFiles(bianchi, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(bianchi, manor), true);
            }
        }

        private void rossiManor15_Click(object sender, EventArgs e)
        {
            string rossi = @"Stuffs\Rossi\";
            string merhi = @"Stuffs\Rossi\normal";
            string manor = @"SeasonData\Vehicles\F1\Manor-Marussia\";
            string veh = "2015_MANOR23.veh";
            string ros = "Alexander Rossi";

            if (File.ReadAllText(manor + veh).Contains(ros))
            {
                foreach (string newPath in Directory.GetFiles(merhi, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(merhi, manor), true);
            }
            else if (!File.ReadAllText(manor + veh).Contains(ros))
            {
                if (bianchiManor15.Checked == true)
                {
                    Msg.Show("Disable 'Swap Roberto Merhi with Jules Bianchi in Manor' option first!", "Warning");
                    return;
                }

                foreach (string newPath in Directory.GetFiles(rossi, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(rossi, manor), true);
            }
        }

        private void magnussenMcl15_Click(object sender, EventArgs e)
        {
            string magnussen = @"Stuffs\Kevin\";
            string alonso = @"Stuffs\Kevin\normal";   
            string mcl = @"SeasonData\Vehicles\F1\McLaren\";
            string veh = "2015_McLaren06.veh";
            string mag = "Kevin Magnussen";

            if (File.ReadAllText(mcl + veh).Contains(mag))
            {
                foreach (string newPath in Directory.GetFiles(alonso, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(alonso, mcl), true);
            }
            else if (!File.ReadAllText(mcl + veh).Contains(mag))
            {
                foreach (string newPath in Directory.GetFiles(magnussen, "*.*",
                SearchOption.AllDirectories))
                    File.Copy(newPath, newPath.Replace(magnussen, mcl), true);
            }
        }

        private void hideSplashes_Click_1(object sender, EventArgs e)
        {
            Settings.HideSplashes ^= true;
            LoadExperimental();
        }

        private void preferencesSettings_Click(object sender, EventArgs e)
        {
            settings.SelectedTab = preferences;
        }

        private void bugTrackerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string trace = @"LOG\trace.txt";
                string exe = "F1 Challenge 99-02.exe";
                string com = "-trace=1000";

                Cleaner();

                if (Settings.ShowLog)
                {
                    Process.Start(exe, com).WaitForExit();
                    Process.Start(trace);
                }
                else
                {
                    Process.Start(exe, com);
                }
            }
            catch (Exception ex)
            {
                Msg.Show("Error:" + Environment.NewLine + Environment.NewLine + ex.ToString(), "Error" );
            }
        }

        private void deskShortCheck_Click(object sender, EventArgs e)
        {
            string dir_bracket = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\VMT Engine (" + GameSettings.ModName + ").lnk";
            string dir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\VMT Engine.lnk";

            if (File.Exists(dir) || File.Exists(dir_bracket))
            { 
                if (File.Exists(dir_bracket))
                {
                    File.Delete(dir_bracket);
                    return;
                }

                File.Delete(dir);
                return;
            }
            else
            {
                DesktopShortcut.Create(GameSettings.ModName);
            }
        }

        private void hideSplash_Click(object sender, EventArgs e)
        {
            Settings.HideSplashes ^= true;
            LoadPreferences();
        }

        private void playAndShutdownVMTEngineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cleaner();
                Process.Start("F1 Challenge 99-02.exe");
                Application.Exit();
            }
            catch (Exception ex)
            {
                Msg.Show("Error while trying to open the game" + Environment.NewLine + Environment.NewLine + ex.ToString(), "Error");
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string log = @"LOG\trace.txt";

            if (File.Exists(log))
            {
                File.Delete(log);
            }
        }

        private void vidF1c_Click(object sender, EventArgs e)
        {
            string mov = @"Movies";
            string movD = @"MoviesD";

            if (Directory.Exists(mov))
                File.Move(mov, movD);
            else if (Directory.Exists(movD))
               File.Move(movD, mov);
        }

        private void materialRaisedButton1_Click_2(object sender, EventArgs e)
        {
            Msg.Show("Are you sure you want to remove all replays?", "Question", MsgButtons.YesNo);
            if (Msg.Yes)
            {
                DirectoryInfo di = new DirectoryInfo("REPLAYS");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            }
        }

        private void oNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (!File.Exists("engine_x.exe"))
            {
                Msg.Show("Engine_X is missing. To download it, head to project's repository at GitLab.", "Error", MsgButtons.OK);
                return;
                /*
                Msg.Show("To use this feature you need to download VMT Engine Extra. Would you like to do this now?", "Error", MsgButtons.YesNo);
                if (Msg.Yes)
                {
                    UpdateDownload.DownloadEngineX();
                }
                return;
                */
            }
            
            Process.Start("engine_X.exe").WaitForExit();
            return;
            if (File.Exists(@"update\UpdateNowX.inf"))
            {
                File.Delete(@"update\UpdateNowX.inf");
                UpdateDownload.DownloadEngineX();
            }
        }

        private void didntHelpedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Msg.Show("If enabling compatibility mode didn't helped, this option will try to help you. Would you like to continue?", "Information", MsgButtons.YesNo);

            if (Msg.Yes)
            {
                string myDoc = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
                string f1CDoc = myDoc + @"\F1 Challenge 99-02";

                if (Directory.Exists(f1CDoc))
                {
                    Directory.Delete(f1CDoc, true);
                }

                string cfile = @"Config.ini";

                if (File.Exists(cfile))
                {
                    File.Delete(cfile);
                }

                Msg.Show("In order to proceed, you need to make graphic configuration. Press OK to continue.", "Information");

                Process.Start("3DConfig.exe").WaitForExit();

                while (!File.Exists(cfile))
                {
                    Msg.Show("Looks like you didn't completed graphic configuration. You need to do it in order to continue.", "Information");
                    Process.Start("3DConfig.exe").WaitForExit();
                }

                GameSettings.VBStrategy ^= true;
            }
        }

        private void AlwaysDarkModeCheck_Click(object sender, EventArgs e)
        {
            if (Settings.NoDarkMode)
            {
                AlwaysDarkModeCheck.Checked = false;
                Msg.Show("Disable ''Don't use Dark Mode'' first", "Warning");
                return;
            }

            Settings.AlwaysDarkMode ^= true;
            LoadPreferences();
        }

        private void NoDarkModeCheck_Click(object sender, EventArgs e)
        {
            if (Settings.AlwaysDarkMode)
            {
                NoDarkModeCheck.Checked = false;
                Msg.Show("Disable ''Always use Dark Mode'' first", "Warning");
                return;
            }
            
            Settings.NoDarkMode ^= true;
            LoadPreferences();
        }

        private void blueAndGrey_Click(object sender, EventArgs e)
        {
            Settings.ThemeBlue ^= true;
            LoadPreferences();
        }

        private void greyOnly_Click(object sender, EventArgs e)
        {
            Settings.ThemeBlue ^= true;
            LoadPreferences();
        }

        private void turnOffCheck_Click(object sender, EventArgs e)
        {
            Settings.AutoClose ^= true;
        }

        private void langSelect_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string[] langs = { "!", "English", "Finnish", "French", "German", "Hungarian", "Italian", "Polish", "Portuguese", "Spanish" };
            
            for (int i = 0; !langSelect.Text.Contains(langs[i]); i++)
            {
                SetGameSettings.ChangeLanguage(langs[i + 1]);
            } 
        }

        private void tireSelect_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string SeasonDataVehicles = @"SeasonData\Vehicles";
            string varType = "";

            string[] tireTypes = { "!", "Medium - Hard", "Soft - Hard", "Soft - Medium", "Super Soft - Soft", "Ultra Soft - Super Soft" };
            string[] tirePaths = { "!", "MediumHard", "SoftHard", "SoftMedium", "SuperSoftSoft", "UltraSoftSuperSoft" };
            
            for (int i = 0; !tireSelect.Text.Contains(tireTypes[i]); i++)
            {
                varType = tirePaths[i + 1];
            }

            string cp = SeasonDataVehicles + @"\TIRES\" + varType + @"\";
            string ps = SeasonDataVehicles + @"\";


            foreach (string newPath in Directory.GetFiles(cp, "*.*",
               SearchOption.AllDirectories))
            {
                if (!newPath.Contains("Thumbs.db"))
                    File.Copy(newPath, newPath.Replace(cp, ps), true);
            }
        }

        private void QuickChatProfiles_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Directory.Exists(@"Save\" + QuickChatProfiles.Text))
            {
                Settings.SelectedProfile = QuickChatProfiles.Text;
                ProfileChatProfiles.Text = QuickChatProfiles.Text;
                GameSettingsLoader();

                if (File.Exists(@"Save\" + Settings.SelectedProfile + @"\quickchattext.txt"))
                {
                    MaterialSingleLineTextField[] txtBoxes = { one, two, three, four, five, six, seven, eight, nine, ten };

                    for (int i = 0; i < 10; i++)
                    {
                        txtBoxes[i].Text = QuickChatReader(i);
                    }
                }
                else
                {
                    foreach (Control text in quick_chat.Controls)
                    {
                        if (text is MaterialSingleLineTextField) (text as MaterialSingleLineTextField).Text = "";
                    }
                }
            }
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            if (File.Exists(@"Save\" + QuickChatProfiles.Text + @"\quickchattext.txt"))
                Process.Start(@"Save\" + QuickChatProfiles.Text + @"\quickchattext.txt");
        }

        private void saveBut_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(@"Save\" + QuickChatProfiles.Text))
            {
                string[] Texts = {
                    one.Text,
                    two.Text,
                    three.Text,
                    four.Text,
                    five.Text,
                    six.Text,
                    seven.Text,
                    eight.Text,
                    nine.Text,
                    ten.Text
                };

                if (File.Exists(@"Save\" + QuickChatProfiles.Text + @"\quickchattext.txt"))
                    File.Delete(@"Save\" + QuickChatProfiles.Text + @"\quickchattext.txt");

                for (int i = 1; i <= 10; i++)
                {
                    string num = "0" + i;

                    if (i == 10)
                    {
                        num = i.ToString();
                    }

                    File.AppendAllText(@"Save\" + QuickChatProfiles.Text + @"\quickchattext.txt", "autochat_" + num + "=\"" + Texts[i-1] + "\"" + Environment.NewLine);
                }
                System.Media.SystemSounds.Beep.Play();
            }            
        }

        private void howToBut_Click(object sender, EventArgs e)
        {
            Msg.Show("In game press CTRL and one of numbers on the top of your keyboard (from 1 to 0). Quick Chat works only in multiplayer!", "Information");
        }

        private void ProfileChatProfiles_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Directory.Exists(@"Save\" + ProfileChatProfiles.Text))
            {
                Settings.SelectedProfile = ProfileChatProfiles.Text;
                QuickChatProfiles.Text = ProfileChatProfiles.Text;
                new Settings();
                GameSettingsLoader();
            }           
        }

        private void ReconnaissanceLap_Click(object sender, EventArgs e)
        {
            PLR.ReconnaissanceLap ^= true;
        }

        private void FormationLap_Click(object sender, EventArgs e)
        {
            PLR.FormationLap ^= true;
        }

        private void PreventAI_Click(object sender, EventArgs e)
        {
            PLR.PreventAIControl ^= true;
        }

        private void CompareTimeToFirstDriver_Click(object sender, EventArgs e)
        {
            PLR.CompareTimeToFirstDriver ^= true;
        }

        private void ShowChat_Click(object sender, EventArgs e)
        {
            PLR.ShowChatInCar ^= true;
        }

        private void ExtraConfigurableButtons_Click(object sender, EventArgs e)
        {
            PLR.ExtraConfigurableButtons ^= true;
        }

        private void ShowYourCar_Click(object sender, EventArgs e)
        {
            PLR.ShowYourCarInRearview ^= true;
        }

        private void ClosedMP_Click(object sender, EventArgs e)
        {
            PLR.ClosedMPQualifySession ^= true;
        }

        private void OpenInNotepadPLR_Click(object sender, EventArgs e)
        {
            string path = @"Save\" + ProfileChatProfiles.Text + @"\" + ProfileChatProfiles.Text + @".plr";
            if (File.Exists(path))
                Process.Start(path);
        }

        void SaveFormPosition()
        {
            Settings.PosX = this.Location.X;
            Settings.PosY = this.Location.Y;

            if (this.Left < 0)
                Settings.PosX = 0;

            if (Top < 0)
                Settings.PosY = 0;
        }

        private void Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveFormPosition();
        }

        private void GitBtn_Click(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://gitlab.com/aathlon/vmt-engine/");
        }

        private void TestMsgBoxBut_Click(object sender, EventArgs e)
        {            
            Msg.Show("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. " +
                "Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. " +
                "Nunc viverra imperdiet enim.Fusce est.Vivamus a tellus. " +
                "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Proin pharetra nonummy pede.Mauris et orci. " +
                "Aenean nec lorem.In porttitor.Donec laoreet nonummy augue. " +
                "Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc." +
                " Mauris eget neque at sem venenatis eleifend.Ut nonummy.", "OK Test Message");
                
        }

        private void BtnYesNo_Click(object sender, EventArgs e)
        {
            Msg.Show("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. " +
                "Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. " +
                "Nunc viverra imperdiet enim.Fusce est.Vivamus a tellus. " +
                "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Proin pharetra nonummy pede.Mauris et orci. " +
                "Aenean nec lorem.In porttitor.Donec laoreet nonummy augue. " +
                "Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc." +
                " Mauris eget neque at sem venenatis eleifend.Ut nonummy.", "YesNo Test Message", MsgButtons.YesNo);

            if (Msg.Yes)
            {
                Msg.Show("Yes", "Result");
            }

            if (Msg.No)
            {
                Msg.Show("No", "Result");
            }
        }

        int MValX, MValY;
        bool TogMove;

        private void splash_MouseDown(object sender, MouseEventArgs e)
        {
            TogMove = true;
            MValX = e.X;
            MValY = e.Y;
        }

        private void splash_MouseMove(object sender, MouseEventArgs e)
        {
            if (TogMove)
            {
                this.Left = MousePosition.X - MValX - splash.Location.X;
                this.Top = MousePosition.Y - MValY - splash.Location.Y;
            }
        }

        private void TrackLoadCommentary_Click(object sender, EventArgs e)
        {
            PLR.TrackLoadCommentary ^= true;
        }

        private void LCDMode_Click(object sender, EventArgs e)
        {
            PLR.LCDStatusMode ^= true;
        }

        private void MovingSteeringWheeleck_Click(object sender, EventArgs e)
        {
            PLR.MovingSteeringWheel ^= true;
        }

        private void DownloadLangPackBut_Click(object sender, EventArgs e)
        {
            Msg.Show("VMT Engine servers are disabled. Therefore you cannot download Language Pack from server. To do that, visit VMT ENgine's repository at GitLab.", "Information");
            //Msg.Show("The program may freeze for a few seconds (that's normal!). Are you sure you want to continue?", "Question", MsgButtons.YesNo);
            //if (Msg.Yes)
            //{
            //    LanguagePack.DownloadPack();
            //}
        }

        private void splash_MouseUp(object sender, MouseEventArgs e)
        {
            TogMove = false;
        }
    }
}