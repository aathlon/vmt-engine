﻿namespace vmtenginerepairtool
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.start = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.ver_lab = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.result_txt = new System.Windows.Forms.Label();
            this.no_help = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.done = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.start.SuspendLayout();
            this.result.SuspendLayout();
            this.no_help.SuspendLayout();
            this.done.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label1.Location = new System.Drawing.Point(31, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 56);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to VMT Engine Repair Tool.\r\nPress button bellow to start the scan.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.start);
            this.tabControl1.Controls.Add(this.result);
            this.tabControl1.Controls.Add(this.no_help);
            this.tabControl1.Controls.Add(this.done);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(398, 258);
            this.tabControl1.TabIndex = 1;
            // 
            // start
            // 
            this.start.BackColor = System.Drawing.Color.White;
            this.start.Controls.Add(this.button6);
            this.start.Controls.Add(this.ver_lab);
            this.start.Controls.Add(this.button1);
            this.start.Controls.Add(this.label1);
            this.start.Location = new System.Drawing.Point(4, 22);
            this.start.Name = "start";
            this.start.Padding = new System.Windows.Forms.Padding(3);
            this.start.Size = new System.Drawing.Size(390, 232);
            this.start.TabIndex = 0;
            this.start.Text = "start";
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button6.Location = new System.Drawing.Point(324, 200);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(63, 29);
            this.button6.TabIndex = 3;
            this.button6.Text = "Credits";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ver_lab
            // 
            this.ver_lab.AutoSize = true;
            this.ver_lab.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ver_lab.Location = new System.Drawing.Point(8, 212);
            this.ver_lab.Name = "ver_lab";
            this.ver_lab.Size = new System.Drawing.Size(46, 15);
            this.ver_lab.TabIndex = 2;
            this.ver_lab.Text = "0.0.0.00";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(134, 124);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 49);
            this.button1.TabIndex = 1;
            this.button1.Text = "Start the scan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // result
            // 
            this.result.AutoScroll = true;
            this.result.BackColor = System.Drawing.Color.White;
            this.result.Controls.Add(this.button2);
            this.result.Controls.Add(this.result_txt);
            this.result.Location = new System.Drawing.Point(4, 22);
            this.result.Name = "result";
            this.result.Padding = new System.Windows.Forms.Padding(3);
            this.result.Size = new System.Drawing.Size(390, 232);
            this.result.TabIndex = 1;
            this.result.Text = "result";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(3, 25);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 49);
            this.button2.TabIndex = 2;
            this.button2.Text = "Fix it!";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // result_txt
            // 
            this.result_txt.AutoSize = true;
            this.result_txt.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.result_txt.Location = new System.Drawing.Point(3, 3);
            this.result_txt.Name = "result_txt";
            this.result_txt.Size = new System.Drawing.Size(27, 19);
            this.result_txt.TabIndex = 1;
            this.result_txt.Text = "res";
            this.result_txt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // no_help
            // 
            this.no_help.BackColor = System.Drawing.Color.White;
            this.no_help.Controls.Add(this.button3);
            this.no_help.Controls.Add(this.label2);
            this.no_help.Location = new System.Drawing.Point(4, 22);
            this.no_help.Name = "no_help";
            this.no_help.Size = new System.Drawing.Size(390, 232);
            this.no_help.TabIndex = 2;
            this.no_help.Text = "no_help";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(109, 121);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(135, 49);
            this.button3.TabIndex = 3;
            this.button3.Text = "Quit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label2.Location = new System.Drawing.Point(21, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(321, 84);
            this.label2.TabIndex = 2;
            this.label2.Text = "We haven\'t found any errors. If you \r\nstill have problems contact VMT \r\nat Facebo" +
    "ok.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // done
            // 
            this.done.BackColor = System.Drawing.Color.White;
            this.done.Controls.Add(this.button5);
            this.done.Controls.Add(this.button4);
            this.done.Controls.Add(this.label3);
            this.done.Location = new System.Drawing.Point(4, 22);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(390, 232);
            this.done.TabIndex = 3;
            this.done.Text = "done";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button5.Location = new System.Drawing.Point(107, 80);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(135, 49);
            this.button5.TabIndex = 6;
            this.button5.Text = "Run VMT Engine and quit";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4.Location = new System.Drawing.Point(107, 135);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(135, 49);
            this.button4.TabIndex = 5;
            this.button4.Text = "Quit";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label3.Location = new System.Drawing.Point(27, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(292, 56);
            this.label3.TabIndex = 4;
            this.label3.Text = "VMT Engine should now work.\r\nIf not, contact VMT at Facebook.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(398, 258);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VMT Engine Repair Tool";
            this.tabControl1.ResumeLayout(false);
            this.start.ResumeLayout(false);
            this.start.PerformLayout();
            this.result.ResumeLayout(false);
            this.result.PerformLayout();
            this.no_help.ResumeLayout(false);
            this.no_help.PerformLayout();
            this.done.ResumeLayout(false);
            this.done.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage start;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage result;
        private System.Windows.Forms.Label result_txt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label ver_lab;
        private System.Windows.Forms.TabPage no_help;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage done;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button6;
    }
}

