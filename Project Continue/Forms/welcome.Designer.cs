﻿namespace Project_Continue
{
    partial class welcome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(welcome));
            this.welcomeLab = new System.Windows.Forms.Label();
            this.label = new MaterialSkin.Controls.MaterialLabel();
            this.startBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.changelogBut = new MaterialSkin.Controls.MaterialFlatButton();
            this.tabControlM = new MaterialSkin.Controls.MaterialTabControl();
            this.first_screen = new System.Windows.Forms.TabPage();
            this.start = new MaterialSkin.Controls.MaterialRaisedButton();
            this.done = new System.Windows.Forms.TabPage();
            this.devWebsiteBut = new MaterialSkin.Controls.MaterialFlatButton();
            this.cds = new MaterialSkin.Controls.MaterialFlatButton();
            this.websiteBut = new MaterialSkin.Controls.MaterialFlatButton();
            this.label3 = new System.Windows.Forms.Label();
            this.whatsnew = new System.Windows.Forms.TabPage();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tabControlM.SuspendLayout();
            this.first_screen.SuspendLayout();
            this.done.SuspendLayout();
            this.whatsnew.SuspendLayout();
            this.SuspendLayout();
            // 
            // welcomeLab
            // 
            this.welcomeLab.AutoSize = true;
            this.welcomeLab.BackColor = System.Drawing.Color.Transparent;
            this.welcomeLab.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.welcomeLab.Location = new System.Drawing.Point(93, 78);
            this.welcomeLab.Name = "welcomeLab";
            this.welcomeLab.Size = new System.Drawing.Size(326, 32);
            this.welcomeLab.TabIndex = 0;
            this.welcomeLab.Text = "Welcome to VMT Engine 0.0.0";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.BackColor = System.Drawing.Color.Transparent;
            this.label.Depth = 0;
            this.label.Font = new System.Drawing.Font("Roboto", 11F);
            this.label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label.Location = new System.Drawing.Point(113, 79);
            this.label.MouseState = MaterialSkin.MouseState.HOVER;
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(304, 19);
            this.label.TabIndex = 2;
            this.label.Text = "Check out changelog and VMT social feeds.";
            // 
            // startBut
            // 
            this.startBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startBut.Depth = 0;
            this.startBut.Location = new System.Drawing.Point(202, 229);
            this.startBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.startBut.Name = "startBut";
            this.startBut.Primary = true;
            this.startBut.Size = new System.Drawing.Size(106, 34);
            this.startBut.TabIndex = 8;
            this.startBut.Text = "Done!";
            this.startBut.UseVisualStyleBackColor = true;
            this.startBut.Click += new System.EventHandler(this.materialRaisedButton7_Click);
            // 
            // changelogBut
            // 
            this.changelogBut.AutoSize = true;
            this.changelogBut.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.changelogBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changelogBut.Depth = 0;
            this.changelogBut.Location = new System.Drawing.Point(194, 104);
            this.changelogBut.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.changelogBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.changelogBut.Name = "changelogBut";
            this.changelogBut.Primary = false;
            this.changelogBut.Size = new System.Drawing.Size(93, 36);
            this.changelogBut.TabIndex = 9;
            this.changelogBut.Text = "Changelog";
            this.changelogBut.UseVisualStyleBackColor = true;
            this.changelogBut.Click += new System.EventHandler(this.materialFlatButton1_Click);
            // 
            // tabControlM
            // 
            this.tabControlM.Controls.Add(this.first_screen);
            this.tabControlM.Controls.Add(this.done);
            this.tabControlM.Controls.Add(this.whatsnew);
            this.tabControlM.Depth = 0;
            this.tabControlM.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlM.Location = new System.Drawing.Point(0, 63);
            this.tabControlM.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabControlM.Name = "tabControlM";
            this.tabControlM.SelectedIndex = 0;
            this.tabControlM.Size = new System.Drawing.Size(527, 304);
            this.tabControlM.TabIndex = 26;
            // 
            // first_screen
            // 
            this.first_screen.BackColor = System.Drawing.Color.White;
            this.first_screen.Controls.Add(this.start);
            this.first_screen.Controls.Add(this.welcomeLab);
            this.first_screen.Location = new System.Drawing.Point(4, 22);
            this.first_screen.Name = "first_screen";
            this.first_screen.Padding = new System.Windows.Forms.Padding(3);
            this.first_screen.Size = new System.Drawing.Size(519, 278);
            this.first_screen.TabIndex = 0;
            this.first_screen.Text = "first_screen";
            // 
            // start
            // 
            this.start.Cursor = System.Windows.Forms.Cursors.Hand;
            this.start.Depth = 0;
            this.start.Location = new System.Drawing.Point(206, 193);
            this.start.MouseState = MaterialSkin.MouseState.HOVER;
            this.start.Name = "start";
            this.start.Primary = true;
            this.start.Size = new System.Drawing.Size(106, 34);
            this.start.TabIndex = 9;
            this.start.Text = "Let\'s start!";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // done
            // 
            this.done.BackColor = System.Drawing.Color.White;
            this.done.Controls.Add(this.devWebsiteBut);
            this.done.Controls.Add(this.cds);
            this.done.Controls.Add(this.websiteBut);
            this.done.Controls.Add(this.label3);
            this.done.Controls.Add(this.label);
            this.done.Controls.Add(this.startBut);
            this.done.Controls.Add(this.changelogBut);
            this.done.Location = new System.Drawing.Point(4, 22);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(519, 278);
            this.done.TabIndex = 3;
            this.done.Text = "done";
            // 
            // devWebsiteBut
            // 
            this.devWebsiteBut.AutoSize = true;
            this.devWebsiteBut.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.devWebsiteBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.devWebsiteBut.Depth = 0;
            this.devWebsiteBut.Location = new System.Drawing.Point(294, 104);
            this.devWebsiteBut.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.devWebsiteBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.devWebsiteBut.Name = "devWebsiteBut";
            this.devWebsiteBut.Primary = false;
            this.devWebsiteBut.Size = new System.Drawing.Size(120, 36);
            this.devWebsiteBut.TabIndex = 29;
            this.devWebsiteBut.Text = "Project\'s repo";
            this.devWebsiteBut.UseVisualStyleBackColor = true;
            this.devWebsiteBut.Click += new System.EventHandler(this.materialFlatButton1_Click_1);
            // 
            // cds
            // 
            this.cds.AutoSize = true;
            this.cds.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cds.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cds.Depth = 0;
            this.cds.Location = new System.Drawing.Point(156, 189);
            this.cds.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cds.MouseState = MaterialSkin.MouseState.HOVER;
            this.cds.Name = "cds";
            this.cds.Primary = false;
            this.cds.Size = new System.Drawing.Size(200, 36);
            this.cds.TabIndex = 28;
            this.cds.Text = "Create desktop shortcut";
            this.cds.UseVisualStyleBackColor = true;
            this.cds.Click += new System.EventHandler(this.cds_Click);
            // 
            // websiteBut
            // 
            this.websiteBut.AutoSize = true;
            this.websiteBut.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.websiteBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.websiteBut.Depth = 0;
            this.websiteBut.Location = new System.Drawing.Point(117, 104);
            this.websiteBut.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.websiteBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.websiteBut.Name = "websiteBut";
            this.websiteBut.Primary = false;
            this.websiteBut.Size = new System.Drawing.Size(69, 36);
            this.websiteBut.TabIndex = 27;
            this.websiteBut.Text = "Website";
            this.websiteBut.UseVisualStyleBackColor = true;
            this.websiteBut.Click += new System.EventHandler(this.materialFlatButton1_Click_2);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(173, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 32);
            this.label3.TabIndex = 26;
            this.label3.Text = "You are ready!";
            // 
            // whatsnew
            // 
            this.whatsnew.Controls.Add(this.materialRaisedButton2);
            this.whatsnew.Location = new System.Drawing.Point(4, 22);
            this.whatsnew.Name = "whatsnew";
            this.whatsnew.Size = new System.Drawing.Size(519, 278);
            this.whatsnew.TabIndex = 4;
            this.whatsnew.Text = "whatsnew";
            this.whatsnew.UseVisualStyleBackColor = true;
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.materialRaisedButton2.Location = new System.Drawing.Point(0, 244);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(519, 34);
            this.materialRaisedButton2.TabIndex = 9;
            this.materialRaisedButton2.Text = "Next";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            // 
            // welcome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(527, 367);
            this.ControlBox = false;
            this.Controls.Add(this.tabControlM);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "welcome";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Getting started!";
            this.Load += new System.EventHandler(this.welcome_Load);
            this.tabControlM.ResumeLayout(false);
            this.first_screen.ResumeLayout(false);
            this.first_screen.PerformLayout();
            this.done.ResumeLayout(false);
            this.done.PerformLayout();
            this.whatsnew.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label welcomeLab;
        private MaterialSkin.Controls.MaterialLabel label;
        private MaterialSkin.Controls.MaterialRaisedButton startBut;
        private MaterialSkin.Controls.MaterialFlatButton changelogBut;
        private MaterialSkin.Controls.MaterialTabControl tabControlM;
        private System.Windows.Forms.TabPage first_screen;
        private System.Windows.Forms.TabPage done;
        private MaterialSkin.Controls.MaterialRaisedButton start;
        private System.Windows.Forms.Label label3;
        private MaterialSkin.Controls.MaterialFlatButton websiteBut;
        private System.Windows.Forms.TabPage whatsnew;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private MaterialSkin.Controls.MaterialFlatButton cds;
        private MaterialSkin.Controls.MaterialFlatButton devWebsiteBut;
    }
}