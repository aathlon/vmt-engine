﻿namespace Project_Continue
{
    partial class changelog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(changelog));
            this.back = new MaterialSkin.Controls.MaterialFlatButton();
            this.changelogBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.label = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.AutoSize = true;
            this.back.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.back.Depth = 0;
            this.back.Location = new System.Drawing.Point(241, 336);
            this.back.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.back.MouseState = MaterialSkin.MouseState.HOVER;
            this.back.Name = "back";
            this.back.Primary = false;
            this.back.Size = new System.Drawing.Size(47, 36);
            this.back.TabIndex = 41;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.materialFlatButton1_Click);
            // 
            // changelogBut
            // 
            this.changelogBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changelogBut.Depth = 0;
            this.changelogBut.Location = new System.Drawing.Point(212, 301);
            this.changelogBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.changelogBut.Name = "changelogBut";
            this.changelogBut.Primary = true;
            this.changelogBut.Size = new System.Drawing.Size(106, 34);
            this.changelogBut.TabIndex = 43;
            this.changelogBut.Text = "Updates history";
            this.changelogBut.UseVisualStyleBackColor = true;
            this.changelogBut.Click += new System.EventHandler(this.changelogBut_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.BackColor = System.Drawing.Color.Transparent;
            this.label.Depth = 0;
            this.label.Font = new System.Drawing.Font("Roboto", 11F);
            this.label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label.Location = new System.Drawing.Point(140, 142);
            this.label.MouseState = MaterialSkin.MouseState.HOVER;
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(227, 38);
            this.label.TabIndex = 44;
            this.label.Text = "- Disabled all online functionality\r\n- Fixed few small bugs";
            this.label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // changelog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(529, 375);
            this.ControlBox = false;
            this.Controls.Add(this.label);
            this.Controls.Add(this.changelogBut);
            this.Controls.Add(this.back);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "changelog";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Changelog";
            this.Load += new System.EventHandler(this.changelog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialFlatButton back;
        private MaterialSkin.Controls.MaterialRaisedButton changelogBut;
        private MaterialSkin.Controls.MaterialLabel label;
    }
}