﻿using System;
using System.Windows.Forms;
using MaterialSkin.Controls;
using System.IO;
using MaterialSkin;
using System.Diagnostics;
using System.Drawing;

namespace Project_Continue
{
    public partial class welcome : MaterialForm
    {
        public welcome()
        {
            InitializeComponent();

            if (Version.Build.Contains("0"))
            {
                welcomeLab.Text = String.Format("Welcome to VMT Engine {0}.{1}!", Version.Major, Version.Minor);
            }
            else
            {
                welcomeLab.Text = String.Format("Welcome to VMT Engine {0}.{1}.{2}!", Version.Major, Version.Minor, Version.Build);
            }

            welcomeLab.Anchor = ((AnchorStyles)
                       ((AnchorStyles.Top | AnchorStyles.Right)));

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue600, Primary.Blue700, Primary.Blue200, Accent.LightBlue200, TextShade.WHITE);

            CenterControls(this.Controls);
            websiteBut.Left = (this.Width - websiteBut.Width) / 3 - 20;
            changelogBut.Left = (this.Width - changelogBut.Width) / 2;
            startBut.Left = (this.Width - startBut.Width) / 2;
            start.Location = new Point((this.Width - start.Width) / 2, (this.Height - start.Height) / 2 + 20);
            cds.Left = (this.Width - cds.Width) / 2;
        }

        void CenterControls(Control.ControlCollection collection)
        {
            foreach (Control ctr in collection)
            {
                if (ctr is Label)
                {
                    (ctr as Label).Left = (this.Width - ctr.Width) / 2;
                }

                if (ctr is MaterialLabel)
                {
                    (ctr as MaterialLabel).Left = (this.Width - ctr.Width) / 2;
                }
            }
        }

        void RestoreSettings ()
        {
            string temp = @"temp\";

            if (File.Exists(temp + "update.txt"))
                Settings.CheckForUpdates = true;

            if (File.Exists(temp + @"fast.txt"))
                Settings.FastRing = true;

            if (File.Exists(temp + "autoclose.txt"))
                Settings.AutoClose = true;

            if (File.Exists(temp + "log.txt"))
                Settings.ShowLog = true;

            if (File.Exists(temp + "nick.txt"))
                Settings.SelectedProfile = File.ReadAllText(temp + "nick.txt");

            DirectoryInfo inf = new DirectoryInfo("temp");
            foreach (FileInfo file in inf.GetFiles())
            {
                file.Delete();
            }

            Directory.Delete("temp");
            Directory.Delete("conf", true);
        }

        private void materialRaisedButton7_Click(object sender, EventArgs e)
        {
            if (!Settings.FirstLaunch)
            {
                Settings.FirstLaunch = true;

                if (File.Exists(@"conf\conf.ini"))
                {
                    RestoreSettings();
                    File.Delete(@"conf\conf.ini");
                }

                if (File.Exists(@"Update\vmtengine.exe"))
                {
                    File.Delete(@"Update\vmtengine.exe");
                }

                if (File.Exists(@"Update\vmtenginebeta.exe"))
                {
                    File.Delete(@"Update\vmtenginebeta.exe");
                }

                if (File.Exists(@"upgrade_tool.exe"))
                {
                    File.Delete(@"upgrade_tool.exe");
                }

                Home f = new Home();
                Hide();
                f.ShowDialog();
                Close();
            }
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            changelog f = new changelog();
            f.ShowDialog();
        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://facebook.com/f1chworld");
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            tabControlM.SelectedTab = done;
        }           

        private void materialFlatButton1_Click_2(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://docs.google.com/document/d/1YtGh5mvGanm2LHfxth2rKKt4DN7tjFXcp_L-5iTNCAg/edit");
        }

        private void cds_Click(object sender, EventArgs e)
        {
            int year = 2011;
            string credits = File.ReadAllText(@"Options\credits.txt");

            while (!credits.Contains("F1 " + year + " Season by VMT Group"))
            {
                year++;
            }

            string mod_name = "F1 " + year + " VMT";
            DesktopShortcut.Create(mod_name);
        }

        private void materialFlatButton1_Click_1(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://gitlab.com/aathlon/vmt-engine");
        }

        private void welcome_Load(object sender, EventArgs e)
        {

        }
    }
}
