﻿using System;
using System.Linq;

namespace Project_Continue
{
    class Splash
    {
        // Splashes are randomly choosen "funny" messages which are inspired by ones in Minecraft main menu.

        public static string Text()
        {
            string[] Splash = {
                "Hello, world!",
                "Text'n'Stuff",
                "Moar text'n'stuff",
                "Sugar free!",
                "Less crashes than Maldonado!",
                "Runs faster than Watch Dogs!",
                "Yay!",
                "All your base are belong to us!",
                "overflow",
                "50 Shades of Raikkonen!",
                "Made in Poland!",
                "I'll take two number 9s",
                "SBLSTT",
                "Made with stackoverflow!",
                "Made with C#!",
                "88 Miles Per Hour!",
                "Great Scott!",
                "Watch The Grand Tour!",
                "Oh dear!",
                "Just leave me alone!",
                "Your Splash Here!",
                ">insert splash text here<",
                ">splash text goes here<",
                "For lack of better splash",
                "Try F1 Challenge VB!",
                "Yes, we can!",
                "Professionally unprofessional!",
                "Aloha!",
                "Como te llamas?",
                "Now with more crashes!",
                "DDIY!",
                "RTFM!",
                "There's no such thing as \"2\"!",
                "I'll have what he's having!",
                "DTTAH!",
                "All innuendo unintentional!",
                "Bwoah!",
                "Han Solo shot first!",
                "password is password!",
                "Nyan Cat ain't got nothing on me!",
                "The dog ate this splash!",
                "I'd buy that for a dollar!",
                "Gravity: What is it?",
                "Is your PC running? Go catch it!",
                "It's over 9000!",
                "Let's go bowling!",
                "There is no real life, only AFK!",
                "But wait, there's more!",
                "WILSON!",
                "We are number one!",
                "Whoa, technology!",
                "Super Fernando Bros!",
                "Your Name Here",
                "This splash has been added in last update",
                "Visit r/formuladank!"
            };

            int list = Splash.Count();
            Random rnd = new Random();
            int SplashNum = rnd.Next(0, list);
            string SplashReturn = Splash[SplashNum];
            return SplashReturn;
        }
    }
}
