﻿using System;
using Microsoft.Win32;

namespace Project_Continue
{
    class Settings
    {
        //VMT Engine
        //Themes
        public static bool AlwaysDarkMode { get => Bool("AlwaysDarkMode"); set => SetSettings.Bool("AlwaysDarkMode"); }
        public static bool NoDarkMode { get => Bool("NoDarkMode"); set => SetSettings.Bool("NoDarkMode"); }
        public static bool ThemeBlue { get => Bool("ThemeBlue"); set => SetSettings.Bool("ThemeBlue"); }
        //First launch
        public static bool FirstLaunch { get => Bool("FirstLaunch"); set => SetSettings.Bool("FirstLaunch"); }
        //DevMode
        public static int DevModeStatus { get => Int("DevModeStatus"); set => SetSettings.Int("DevModeStatus", value); }
        //Goto
        public static bool GotoExperimental { get => Bool("GotoExperimental"); set => SetSettings.Bool("GotoExperimental"); }
        public static bool GotoPreferences { get => Bool("GotoPreferences"); set => SetSettings.Bool("GotoPreferences"); }
        //Preferences
        public static bool HideVersion { get => Bool("HideVersion"); set => SetSettings.Bool("HideVersion"); }
        public static bool HideSplashes { get => Bool("HideSplashes"); set => SetSettings.Bool("HideSplashes"); }
        //Updates
        public static bool CheckForUpdates { get => Bool("CheckForUpdates"); set => SetSettings.Bool("CheckForUpdates"); }
        public static bool FastRing { get => Bool("FastRing"); set => SetSettings.Bool("FastRing"); }
        //Game
        public static bool ShowLog { get => Bool("ShowLog"); set => SetSettings.Bool("ShowLog"); }
        public static bool AutoClose { get => Bool("AutoClose"); set => SetSettings.Bool("AutoClose"); }
        public static string SelectedProfile { get => String("SelectedProfile"); set => SetSettings.String("SelectedProfile", value); }
        public static bool LanguagePackDownloaded { get => Bool("LanguagePackDownloaded"); set => SetSettings.Bool("LanguagePackDownloaded"); }
        //Location
        public static int PosX { get => Int("PosX"); set => SetSettings.Int("PosX", value); }
        public static int PosY { get => Int("PosY"); set => SetSettings.Int("PosY", value); }

        static bool Bool(string Name)
        {
            try
            {
                using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
                {
                    if (Key.GetValue(Name).Equals("true"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        static int Int(string Name)
        {
            try
            {
                using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
                {
                    return Int32.Parse(Key.GetValue(Name).ToString());
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        static string String(string Name)
        {
            try
            {
                using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
                {
                    return Key.GetValue(Name).ToString();
                }
            }
            catch (Exception)
            {
                return "";
            }
        }
    }

    class SetSettings
    {
        internal static void Bool(string Name)
        {
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
            {
                object Value = Key.GetValue(Name);

                if (Value == null || Key.GetValue(Name).Equals("false"))
                {
                    Key.SetValue(Name, "true");
                }
                else
                {
                    Key.SetValue(Name, "false");
                }           
            }
        }

        internal static void BoolTrue(string Name)
        {
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
            {
                Key.SetValue(Name, "true");
            }
        }

        internal static void BoolFalse(string Name)
        {
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
            {
                Key.SetValue(Name, "false");
            }
        }

        internal static void Int(string Name, int Value)
        {
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
            {
                Key.SetValue(Name, Value.ToString());
            }
        }

        internal static void String(string Name, string Value)
        {
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine"))
            {
                Key.SetValue(Name, Value);
            }
        }
    }
}
