# VMT Engine
VMT Engine (a.k.a. Project Continue) has been in development since... 2014? It's been rewritten multiple times and this will be probably one of the last versions. This tool is based on GamePT 1.8 (which was mostly based on VMT Engine 2.9), but to be honest - I think there is no code left from GamePT 1.8 at all.
Originaly 4.0 version was supposed to be called 3.1, but there were so many changes in code, I've realised that it would be better to call it version "4.0". Basically most of the code has been scrapped. At this point, there are only few lines of code from VMT Engine 3.0.2, and none from GamePT 1.8. While only "Home" class had 5249 lines of code in GamePT 1.8, and 2785 in VMT Engine 3.0.2, now it has 1663 lines of code... Which I think is a good sign. Originaly I thought that this will be "small code cleanup", which became a rewritting of the program.
VMT Engine was supposed to be only compatible with VMT mods, but it should work with any F1 Challenge 99-02 mod with no problems (except of the "VMT" tab features, of course).

Still - the big part of code is... Bad. But I will not bother to fix that, or maybe I will - who knows?

## Used third party libraries:
- .NET Framework 4.6
- MaterialSkin
- DotNetZip Library

## Compiling the source code
The program was compiled with Microsoft Visual Studio 2017 Community, so I think it's the best way to compile it.

## Author
- [Athlon](http://athlon.kkmr.pl/)

## License
This program is distributed under GNU General Public License V3. Feel free to use its source code as long as you mention the author. You're allowed to redistribute it, as long as you mention changes in the code, provide the link to the original repository and it will be licensed under GNU license too.

## Support list

|      Mod Name      |                  Info                 |
|:------------------:|:-------------------------------------:|
|     F1 2017 VMT    | Full support with tires and VMT panel |
|     F1 2016 VMT    | Full support with tires and VMT panel |
|     F1 2015 VMT    | Full support with tires and VMT panel |
|     F1 2014 VMT    |      Tires selector support only      |
|     F1 2013 VMT    |          Basic features only          |
|     F1 2012 VMT    |          Basic features only          |
|     F1 2011 VMT    |          Basic features only          |
| Other non VMT mods |             Not supported             |