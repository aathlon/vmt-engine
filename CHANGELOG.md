# 4.0.2 (18.10.18
- Disabled update system
- Fixed few issues

# 4.0.1

### Added:
- New message box matching the VMT Engine design
- Added warning if the program is going to open external link
- You can move the window when the cursor is on splash text
- You can disable track commentary while loading it
- Added "Temperatures on LCD screen" option
- Moving Steering Wheels button
- You can check Engine X and Update Manager version in About screen
- Added "Dev's website" button to welcome screen
- "Create Desktop Shortcut" button for Welcome screen
- You can re-download language pack (it's recommendated to do that for the first time)

### Changes:
- Improved language detecting & changing
- Improved tires detecting & changing
- Small changes in Polish and Spanish language files
- Improved loading Quick Chat text from file
- Improved settings changing and loading
- Improved dark theme loading
- Improved Welcome screen

### Bug fixes:
- Fixed bug which prevented VMT Engine from changing tires type because of Thumbs.db file
- Fixed bug with changing Red Bull camouflage in F1 2015 VMT
- Fixed error occuring while changing the profile in Quick Chat editor
- Fixed color of back button in Changelog and Update forms on dark and light modes
- Text boxes in Quick Chat Creator now cleans itselves if there's no Quick Chat file
- Fixed link to update history in Changelog window

### Removed:
- Broken code
- Read version button for Engine X

## Repair Tool (Beta 2.0):
- Added Repair Tool into the official repositry
- Now it only supports VMT Engine 4

## Downloader (Beta 1.1):
- Program now detects if VMT Engine 4 is already installed
- Small changes under the hood

Upgrade Tool will NEVER BE RELEASED because its code sucks badly

# 4.0 (31.12.2017)
Basically everything has been rewritten. It's the same tool, with the same functionality, but with much better code base.

### Added:
- Installer is now available to download and install the program
- Added some new cringy splashes
- Quick Splash Editor is now loading last quick chat file
- Some new bugs to fix later
- You can't disable dev settings if any option in Dev Settings is enabled anymore
- VMT Engine now remembers last form location on screen
- Support for non-VMT mods
- Git repository button

### Changes:
- VMT Engine is now open-source!
- Now it's licensed under GNU GPL V3
- Most of the code has been rewritten and much improved
- Settings aren't stored in conf.ini file, instead they're in Windows Registry (HKEY_CURRENT_USER/SOFTWARE/VMT/VMT Engine)
- Splashes are stored inside of Splash.cs class
- Splashes are generated after every single Home window refresh
- Changed context boxes
- Quick Chat Editor is now built into Home form
- PLR Editor is now built into Home form
- Small changes in texts
- Redesigned update system
- Changed directories in source code
- Renamed "Reset started" to Reload Form
- Renamed "Developer mode" to "Extra developers tools"
- Many, many small changes
- Changed versions for updater and engine X

### Bug fixes:
- Fixed bugs with dark mode
- Fixed many small bugs
- "Fixed" colors of button in Changelog and Update forms
- Fixed critical error in updater

### Removed:
- Removed telemetry
- Removed "Add custom splash"
- Removed some cringy splashes
- Removed quick chat editor window
- Removed PLR Editor form
- Reload form
- 

# 3.0.2 (22.08.2017)

### Bug fixes
- Fixed "Delete Replays" button
- Removed "Create Desktop Shortcut" from "Getting started!" window

# 3.0.1 (22.08.2017)

### Added
- You can now create desktop shortcut in program
- Added ability to disable splashes
- Added context menu for Play and Check Log buttons (experimental)
- updman.exe is looking for updates when VMT Engine is starting
- Added manifest file
- You can now download Repair Tool which lets you fix VMT Engine if it doesn't work properly (3.0 or above)
- Added "Open with text editor" button in Quick Chat Creator
- You can now disable video files in game
- More info in credits

### Changes
- Renamed Quick Chat Editor to Quick Chat Creator
- Updated message boxes
- Fixed typos
- Removed obsolete code
- VMT Engine suggest downloading of GamePT if non-VMT mod has been detected
- Changed message box text in updman.exe
- Text changes in PLR Editor

### Bug fixes
- Now if error occured VMT Engine shouldn't be frozen
- Preview updates are checking correct server destination
- Fixed updman.exe detection of Preview update which caused error
- Fixed updman.exe update path
- Fixed some bugs that may occured. Now they can't occure because we've fixed it

# 3.0 (29.07.2017)
- Completly new design
- Added much better compatibility with ALL VMT mods

# 2.8.1
- All forms moved to MetroForms
- Very importand changes under the hood
- Bug fixes

# 2.8.0
- "VMT Mods" renamed to "More VMT"
- In "More VMT" you can download also apps &amp; other stuffs from VMT
- DLC Center moved to More VMT
- Added apps section
- Added F1CH-WORLD League to Apps
- Redesigned Quick Chat Editor icon
- Another pre-2016 VMT changes
- "VMT Central" renamed to "VMT"
- Optimalisation
- Themes has been disabled
- Minor changes in translation UI
- Small changes
- Bug fixes

# 2.7.0
- Added dark theme to VMT Engine
- Minor changes
- Fixed bug with Quick chat editor name &amp; icon

# 2.6.0
- Added Quick Chat Editor
- New tiles layout
- Christmas Pack Config button has been moved to DLC Centre (if you've downloaded it, it'll replace "Download" button)
- Added VMT Center app to DLC Centre
- When press most of social buttons, there will be question to download VMT Center
- Added FAQ
- You can now refresh or reinstall VMT Engine
- VMT Engine Preview is now only available throught official version (it means, you need to install official version first)
- Minor UI changes (eg. few labels was moved 5-6 pixels)
- Many small changes and fixes

# 2.5.1
- Bug fixes

# 2.5.0
- More options in PLR Editor
- Support for F1 2015 VMT - Winter Update
- You can now select Magnussen as McLaren driver (requires Winter Update)
- You can now select Rossi as Manor driver (requires Winter Update)
- Better support for F1 2011/2012 VMT and other mods
- Added League button in "Social"
- Deleted OneDrive &amp; Dropbox settings backup
- Minor changes
- Bug fixes

# 2.4.1
- Bug fixes

# 2.4.0
- VMT Mods Center
- Updated first start menu
- Added Christmas Pack to download in DLC Centre
- Anti-aliasing and V-Sync support for game
- Game in window option has been moved to "Game"
- DLC Centre has come out from Beta!
- Manor changes in UI layout...
- ...and some not so big UI changes :)
- Bug fixes

# 2.3.2
- Redesigned DLC Centre
- Small change in Check log tile
- Minor changes and bug fixes

# 2.3.1
- Added Check Log tile
- Fixed DLC Centre tile picture (for some users there was still bug tracker picture)

# 2.3.0
- DLC Centre BETA
- PLR Editor 2.0 Beta with more settings
- Better optimalisation and performance
- Spotify button now works better 
- Minor changes
- Bug fixes

# 2.2.3
- You can now disable movies in game
- Many bug fixes

# 2.2.2
- VMT Center link in VMT Center (duh...)
- After pressing VMT image in credits, you'll be requested to our Facebook site
- Minor changes
- Bug fixes

# 2.2.1
- Fixed critical error

# 2.2.0
- Tiles are closer
- Tiles pictures
- Minor changes
- UI fixes

# 2.1.0
- New updates system - now it's separated to "Slow Ring" (old Official) and "Fast Ring" (old Preview) (for Slow Ring)
- Fixed bug, where sometimes old buttons were visible (for Fast Ring)
- Lots of bug fixes and improvments
- Some changes in conf.ini file, please don't restore conf.ini file from older versions of VMT Engine!

# 2.0.1
- Social links
- Save VMT Engine settings to OneDrive and Dropbox
- Change of naming preview versions
- Minor changes in update code
- Minor changes
- Bug fixes

# 2.0
- Redesinged UI thanks to MetroFramework
- Every single window is in one form
- VMT Central
- You can now see, which tire pack and language you use
- Program isn't crashing when pressing Windows exit button
- You can now see which tires and language you use in game
- When you put pointer on the interactive object, cursor is changing to hand
- On the top right there's VMT logo
- F1 2015 VMT force update option
- Minor fixes

# 1.9.1
- Added Jules Bianchi option in Manor
- Fixed CamoBull error

# 1.9
- Added F1 2015 VMT section
- Added notifications
- You can now report bugs by mail (preview version only)!
- All F1 2015 VMT stuffs has been moved to F1 2015 VMT section
- F1 2015 VMT auto udpdates
- Developer mode
- The description of the program has been changed
- Added button to open telemetry
- You can now access game folder by the program
- In "Game" section you can now enable showing speed units in mph for telemetry
- Minor text changes
- New program installer
- Lots of fixes

# 1.8.1
- Added Camo Red Bull option

# 1.8
- New black theme
- Fixed background
- Red buttons and switches
- Settings will restore if:
a.) You updating from VMT Engine 1.8 Preview 3 version or higher
b.) You used "VMT Settings Backup" for 1.7.1 or 1.8 Preview 2 versions
- Fixed bugs with updates buttons

# 1.7.1
- Little hotfix

# 1.7
- Program was ported to C# (the old code will be available to download!)
- UI refreshed
- New "About" screen
- Minor stuffs
- Bug fixes

# 1.6
- Modern Windows patch for F1 Challenge
- ENB Series (as Beta version)
- New welcome screen
- New exit button

# 1.5.2
- In preview mode, you can enable visibility of build version in settings
- When automatic checking for PREVIEW updates is enabled, you can't disable preview mode
- After updating program, you can see welcome screen
- "F1 2015 VMT mode" will be enabled automatically in F1 2015 VMT
- "Options" renamed to "Settings"
- Now Beta versions will be named Preview (old name may cause confuse)
- Instalator improvments
- Text fixes
- New error information system
- Minor interface changes
- Bug fixes

# 1.5.1
- Changes in update system
- You can now enable automatic preview updates and preview updates channel
- Deleted changelog site viewer in Updates and added button to check it
- After launching the game, downloaded updates temps will be deleted automatically
- UI fixes
- Some minor bug fixes

# 1.5
- F1 2015 VMT download updates button is restored
- Some codes changes
- Minor UI fixes

# 1.4
- Completly re-created UI (inspired by Adaptive UX)
- You can run game in windowed mode
- Lot of changes
- Deleted physic changing
- "1.3 Fix" button deleted
- 2015 VMT Beta updates is deleted (we're really sorry for it, but our server can't handle this)
- Some MsgBox fixes
- "Start with log" is now called "Play with bug tracker"
- Code of program was recreated

# 1.3
- Auto-update
- Options for VMT Engine
- Red Bull Racing Jerez paintjob (F1 2015 VMT only, button only)
- Now you can swap bettewen Magnussen or Alonso (F1 2015 VMT only)
- Now in main menu you see, what game you're launching
- New icon
- Windows XP support (required .NET Framework 4.0)
- Now program is in 256x435 resolution
- Updates now not contains Physic
- Minor changes fixes

# 1.2.1
- Bug fixes

# 1.2
It's propably the biggest update since old F1 VMT Engine 1.2 and 2.0
 
- Support for F1 2015 VMT
- Support for F1 2014 VMT - F1 2002 Porting
- UI improvments (Segoe UI font) and larger fonts
- "2015 VMT Beta" updates (for F1 2015 VMT only)
- Update system improvments
- Physic changing (2015 VMT only, when select Classic (basically it's from F1 2014 VMT) you must setup propebly car to "be fast")
- Log will be show after exiting game, when pressing "Start with log"
- "Turn off/on auto-close" option allows you turning on or off auto closing program, after pressing "Start"
- Buttons layout in "Game Options" changed
- Lot bug fixes

# 1.1
- You can open log from program
- Refresh button in Update
- Minor fixes

# 1.0
- First official release
Comparing with RC3:
- Changes "Game options" to "Config Game"
- Minor fixes

# Release Candidate 3

- Minor fix

# Release Candidate 2
- "Update" fix

# Release Candidate 1
- "Super Soft - Soft" tires now working
- Code from "Hungarian" language now is in every language and tires
- Added "Upgrade cleaner"
- Tires codes fixes
- Minor changes
- Minor fixes

# 1.1 Beta
- Background in every screen
- "Start with log" finally working
- Fixes in languages code
- "Contact" button deleted from main menu
- Special edition of code in Hungarian language (test only)
- Added icon
- Update downloader
- Lots of minor changes and fixes
- Some manor bug fixes

# 1.0 Beta
- Some code fixes
- Testing new download system
- All languages
- UI changes
- Background
- Minor fixes

# Alpha 0.1.1
- All game settings moved to "Game config"
- zChangelog
- 256x480 window
- zStart with log

# Alpha 0.1:
- First release
