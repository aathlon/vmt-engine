﻿namespace Project_Continue
{
    partial class update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(update));
            this.backBut = new MaterialSkin.Controls.MaterialFlatButton();
            this.updateNowBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.htmlView = new System.Windows.Forms.WebBrowser();
            this.learnMoreBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker3 = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // backBut
            // 
            this.backBut.AutoSize = true;
            this.backBut.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.backBut.BackColor = System.Drawing.Color.Transparent;
            this.backBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Depth = 0;
            this.backBut.Location = new System.Drawing.Point(243, 339);
            this.backBut.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.backBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.backBut.Name = "backBut";
            this.backBut.Primary = false;
            this.backBut.Size = new System.Drawing.Size(47, 36);
            this.backBut.TabIndex = 39;
            this.backBut.Text = "Back";
            this.backBut.UseVisualStyleBackColor = false;
            this.backBut.Click += new System.EventHandler(this.materialFlatButton1_Click);
            // 
            // updateNowBut
            // 
            this.updateNowBut.BackColor = System.Drawing.Color.Transparent;
            this.updateNowBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updateNowBut.Depth = 0;
            this.updateNowBut.Location = new System.Drawing.Point(158, 296);
            this.updateNowBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.updateNowBut.Name = "updateNowBut";
            this.updateNowBut.Primary = true;
            this.updateNowBut.Size = new System.Drawing.Size(106, 34);
            this.updateNowBut.TabIndex = 38;
            this.updateNowBut.Text = "Update now!";
            this.updateNowBut.UseVisualStyleBackColor = false;
            this.updateNowBut.Click += new System.EventHandler(this.materialRaisedButton19_Click);
            // 
            // htmlView
            // 
            this.htmlView.AllowNavigation = false;
            this.htmlView.AllowWebBrowserDrop = false;
            this.htmlView.Location = new System.Drawing.Point(12, 68);
            this.htmlView.MinimumSize = new System.Drawing.Size(20, 20);
            this.htmlView.Name = "htmlView";
            this.htmlView.Size = new System.Drawing.Size(505, 222);
            this.htmlView.TabIndex = 40;
            this.htmlView.WebBrowserShortcutsEnabled = false;
            // 
            // learnMoreBut
            // 
            this.learnMoreBut.BackColor = System.Drawing.Color.Transparent;
            this.learnMoreBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.learnMoreBut.Depth = 0;
            this.learnMoreBut.Location = new System.Drawing.Point(270, 296);
            this.learnMoreBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.learnMoreBut.Name = "learnMoreBut";
            this.learnMoreBut.Primary = true;
            this.learnMoreBut.Size = new System.Drawing.Size(106, 34);
            this.learnMoreBut.TabIndex = 41;
            this.learnMoreBut.Text = "Updates history";
            this.learnMoreBut.UseVisualStyleBackColor = false;
            this.learnMoreBut.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.WorkerReportsProgress = true;
            // 
            // backgroundWorker3
            // 
            this.backgroundWorker3.WorkerReportsProgress = true;
            // 
            // update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 375);
            this.ControlBox = false;
            this.Controls.Add(this.updateNowBut);
            this.Controls.Add(this.learnMoreBut);
            this.Controls.Add(this.backBut);
            this.Controls.Add(this.htmlView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "update";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update is here!";
            this.Load += new System.EventHandler(this.update_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialFlatButton backBut;
        private MaterialSkin.Controls.MaterialRaisedButton updateNowBut;
        private System.Windows.Forms.WebBrowser htmlView;
        private MaterialSkin.Controls.MaterialRaisedButton learnMoreBut;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.ComponentModel.BackgroundWorker backgroundWorker3;
    }
}