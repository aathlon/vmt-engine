﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vmtenginerepairtool
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (!VMTEngineTest())
            {
                Application.Exit();
                return;
            }
            Application.Run(new Form1());
        }

        private static bool VMTEngineTest()
        {
            if (!File.Exists(@"VMT Engine.exe"))
            {
                MessageBox.Show("Couldn't find VMT Engine.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            var versionInfo = FileVersionInfo.GetVersionInfo(@"VMT Engine.exe");
            Variables.VMTEngineVersion = versionInfo.ProductVersion;
            if (!Variables.VMTEngineVersion.StartsWith("4."))
            {
                MessageBox.Show("VMT Engine 4.0 or newer is only supported. Your version is " + Variables.VMTEngineVersion +
                    ". Try to upgrade VMT Engine with Upgrade Tool first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}
