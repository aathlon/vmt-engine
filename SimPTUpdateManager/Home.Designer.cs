﻿namespace SimPTUpdateManager
{
    partial class Home
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.info_lab = new MaterialSkin.Controls.MaterialLabel();
            this.ver_lab = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // info_lab
            // 
            this.info_lab.AutoSize = true;
            this.info_lab.Depth = 0;
            this.info_lab.Font = new System.Drawing.Font("Roboto", 11F);
            this.info_lab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.info_lab.Location = new System.Drawing.Point(72, 79);
            this.info_lab.MouseState = MaterialSkin.MouseState.HOVER;
            this.info_lab.Name = "info_lab";
            this.info_lab.Size = new System.Drawing.Size(139, 19);
            this.info_lab.TabIndex = 0;
            this.info_lab.Text = "Installing updates...";
            // 
            // ver_lab
            // 
            this.ver_lab.AutoSize = true;
            this.ver_lab.BackColor = System.Drawing.Color.Transparent;
            this.ver_lab.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.ver_lab.ForeColor = System.Drawing.Color.White;
            this.ver_lab.Location = new System.Drawing.Point(236, 34);
            this.ver_lab.Name = "ver_lab";
            this.ver_lab.Size = new System.Drawing.Size(44, 15);
            this.ver_lab.TabIndex = 1;
            this.ver_lab.Text = "ver_lab";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(284, 116);
            this.ControlBox = false;
            this.Controls.Add(this.ver_lab);
            this.Controls.Add(this.info_lab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Home";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VMT Engine Update Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel info_lab;
        private System.Windows.Forms.Label ver_lab;
    }
}

