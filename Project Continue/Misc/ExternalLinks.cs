﻿using System.Diagnostics;

namespace Project_Continue
{
    class ExternalLinks
    {
        static public void Run(string Link)
        {
            string Show()
            {
                if (Link.StartsWith("https://"))
                {
                    return Link.Substring(8);
                }
                else if (Link.StartsWith("http://"))
                {
                    return Link.Substring(7);
                }
                return Link;
            }
                Msg.Show("Would you like to open\n" + Show() + "\nin your browser?", "Question", MsgButtons.YesNo);

            if (Msg.Yes)
            {
                Process.Start(Link);
            }
        }

    }
}
