﻿using System;
using IWshRuntimeLibrary;
using System.IO;
using System.Windows.Forms;

namespace Project_Continue
{
    class DesktopShortcut
    {
        public static bool DoesShortcutExists (string ModName)
        {
            if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\VMT Engine.lnk") || 
                System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\VMT Engine (" + ModName + ").lnk"))
            {
                return true;
            }
            return false;
        }

        public static void Create(string ModName)
        {
            string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            + Path.DirectorySeparatorChar + Application.ProductName + " (" + ModName + ").lnk";
            var shell = new WshShell();
            var shortcut = shell.CreateShortcut(link) as IWshShortcut;
            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Save();
        }
    }
}
