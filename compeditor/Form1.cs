﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;

namespace compeditor
{
    public partial class Form1 : Form
    {
        string ver = "18420";
        string Version = Application.ProductVersion;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RegistryKey Key = Registry.LocalMachine.OpenSubKey(keyName);

            if (Key.GetValue(valuename) != null || Key.GetValue(valueConf) != null)
            {
                RegistryRemove();
            }
            else
            {
                RegistryAdd();
            }

            DownloadFile(@"http://vmt.kkmr.pl/vmtengine/updates/engine_X.txt", @"update\engine_x.txt");

            //if (!File.ReadAllText(@"update\engine_x.txt").Contains(ver))
            //{
                //File.Delete(@"update\engine_x.txt");
                //File.Create(@"update\UpdateNowX.inf").Close();
            //}

            Application.Exit();
        }

        string keyName = "Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers";
        string valuename = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\F1 Challenge 99-02.exe";
        string valueConf = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\3DConfig.exe";

        private void RegistryAdd()
        {
            string value = "~ WIN98 RUNASADMIN";

            RegistryKey key;
            key = Registry.LocalMachine.CreateSubKey(keyName);
            key.SetValue(valuename, value);
            key.SetValue(valueConf, value);
            key.Close();

            DialogResult result = MessageBox.Show("It's recommended to run 3DConfig. Would you like to do this now?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                if (File.Exists("Config.ini"))
                    File.Delete("Config.ini");

                Process.Start("3DConfig.exe").WaitForExit();

                string cfile = @"Config.ini";

                if (File.ReadAllText(cfile).Contains("VBSTRATEGY=0"))
                    File.WriteAllText(cfile, File.ReadAllText(cfile).Replace("VBSTRATEGY=0", "VBSTRATEGY=2"));

                else if (File.ReadAllText(cfile).Contains("VBSTRATEGY=1"))
                    File.WriteAllText(cfile, File.ReadAllText(cfile).Replace("VBSTRATEGY=1", "VBSTRATEGY=2"));
            }

            MessageBox.Show("Operation completed! If your game is still crashing or freezing, " +
                "you may try to use \"Didn't helped?\" option.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void RegistryRemove()
        {
            RegistryKey key;
            key = Registry.LocalMachine.OpenSubKey(keyName, true);
            key.DeleteValue(valuename);
            key.DeleteValue(valueConf);
            key.Close();
        }

        void DownloadFile(string Name, string Path)
        {
            using (WebClient dwn = new WebClient())
            {
                dwn.DownloadFile(Name, Path);
                dwn.Dispose();
            }
        }
    }
}