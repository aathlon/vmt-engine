﻿namespace Project_Continue
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.Tab = new MaterialSkin.Controls.MaterialTabControl();
            this.homeTab = new System.Windows.Forms.TabPage();
            this.gPanel = new System.Windows.Forms.Panel();
            this.urOffline = new MaterialSkin.Controls.MaterialLabel();
            this.gamedirTile = new System.Windows.Forms.Button();
            this.testPanel = new System.Windows.Forms.Panel();
            this.BtnYesNo = new System.Windows.Forms.Button();
            this.TestMsgBoxBut = new System.Windows.Forms.Button();
            this.testUpdateWindowTile = new System.Windows.Forms.Button();
            this.testWelcomeTile = new System.Windows.Forms.Button();
            this.bugTrackTile = new System.Windows.Forms.Button();
            this.playTile = new System.Windows.Forms.Button();
            this.playContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.playAndShutdownVMTEngineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bugTrackerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.setCompatibilityModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.didntHelpedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.telemetryTile = new System.Windows.Forms.Button();
            this.chatTile = new System.Windows.Forms.Button();
            this.plrTile = new System.Windows.Forms.Button();
            this.confTile = new System.Windows.Forms.Button();
            this.logTile = new System.Windows.Forms.Button();
            this.logButContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.webPanel = new System.Windows.Forms.Panel();
            this.htmlView = new System.Windows.Forms.WebBrowser();
            this.gameTab = new System.Windows.Forms.TabPage();
            this.game = new MaterialSkin.Controls.MaterialTabControl();
            this.basic = new System.Windows.Forms.TabPage();
            this.DownloadLangPackBut = new MaterialSkin.Controls.MaterialFlatButton();
            this.langSelect = new System.Windows.Forms.ComboBox();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.vsyncF1CLab = new MaterialSkin.Controls.MaterialCheckBox();
            this.settingsF1CLab = new MaterialSkin.Controls.MaterialLabel();
            this.vidF1c = new MaterialSkin.Controls.MaterialCheckBox();
            this.windowedF1CLab = new MaterialSkin.Controls.MaterialCheckBox();
            this.mphTelF1CLab = new MaterialSkin.Controls.MaterialCheckBox();
            this.tirePanel = new System.Windows.Forms.Panel();
            this.tireSelect = new System.Windows.Forms.ComboBox();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.antiAliasignF1CLab = new MaterialSkin.Controls.MaterialCheckBox();
            this.langLab = new MaterialSkin.Controls.MaterialLabel();
            this.plr = new System.Windows.Forms.TabPage();
            this.MovingSteeringWheeleck = new MaterialSkin.Controls.MaterialCheckBox();
            this.LCDMode = new MaterialSkin.Controls.MaterialCheckBox();
            this.TrackLoadCommentary = new MaterialSkin.Controls.MaterialCheckBox();
            this.OpenInNotepadPLR = new MaterialSkin.Controls.MaterialRaisedButton();
            this.ClosedMP = new MaterialSkin.Controls.MaterialCheckBox();
            this.ShowYourCar = new MaterialSkin.Controls.MaterialCheckBox();
            this.ExtraConfigurableButtons = new MaterialSkin.Controls.MaterialCheckBox();
            this.ShowChat = new MaterialSkin.Controls.MaterialCheckBox();
            this.CompareTimeToFirstDriver = new MaterialSkin.Controls.MaterialCheckBox();
            this.PreventAI = new MaterialSkin.Controls.MaterialCheckBox();
            this.ReconnaissanceLap = new MaterialSkin.Controls.MaterialCheckBox();
            this.FormationLap = new MaterialSkin.Controls.MaterialCheckBox();
            this.ProfileChatProfiles = new System.Windows.Forms.ComboBox();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.quick_chat = new System.Windows.Forms.TabPage();
            this.QuickChatProfiles = new System.Windows.Forms.ComboBox();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.saveBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.howToBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tenLab = new MaterialSkin.Controls.MaterialLabel();
            this.nineLab = new MaterialSkin.Controls.MaterialLabel();
            this.eightLab = new MaterialSkin.Controls.MaterialLabel();
            this.sevenLab = new MaterialSkin.Controls.MaterialLabel();
            this.sixLab = new MaterialSkin.Controls.MaterialLabel();
            this.fiveLab = new MaterialSkin.Controls.MaterialLabel();
            this.fourLab = new MaterialSkin.Controls.MaterialLabel();
            this.threeLab = new MaterialSkin.Controls.MaterialLabel();
            this.twoLab = new MaterialSkin.Controls.MaterialLabel();
            this.oneLab = new MaterialSkin.Controls.MaterialLabel();
            this.ten = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.nine = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.eight = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.seven = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.six = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.five = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.four = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.three = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.two = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.one = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.saveLab = new MaterialSkin.Controls.MaterialLabel();
            this.vmtTab = new System.Windows.Forms.TabPage();
            this.vmtPan = new System.Windows.Forms.Panel();
            this.vmtP = new MaterialSkin.Controls.MaterialTabControl();
            this.classic = new System.Windows.Forms.TabPage();
            this.supportedWarning = new MaterialSkin.Controls.MaterialLabel();
            this._15 = new System.Windows.Forms.TabPage();
            this.rossiManor15 = new MaterialSkin.Controls.MaterialCheckBox();
            this.magnussenMcl15 = new MaterialSkin.Controls.MaterialCheckBox();
            this.rbrCamo15 = new MaterialSkin.Controls.MaterialCheckBox();
            this.bianchiManor15 = new MaterialSkin.Controls.MaterialCheckBox();
            this._16 = new System.Windows.Forms.TabPage();
            this.swapKvyVer = new MaterialSkin.Controls.MaterialCheckBox();
            this.replaceAloVan = new MaterialSkin.Controls.MaterialCheckBox();
            this._17 = new System.Windows.Forms.TabPage();
            this.replaceAloBut = new MaterialSkin.Controls.MaterialCheckBox();
            this.settingsTab = new System.Windows.Forms.TabPage();
            this.settings = new MaterialSkin.Controls.MaterialTabControl();
            this.main = new System.Windows.Forms.TabPage();
            this.preferencesSettings = new System.Windows.Forms.Button();
            this.settingsDev = new System.Windows.Forms.Button();
            this.settingAbout = new System.Windows.Forms.Button();
            this.settingUpdate = new System.Windows.Forms.Button();
            this.settingGames = new System.Windows.Forms.Button();
            this.games = new System.Windows.Forms.TabPage();
            this.turnOffCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.showLogCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.updates = new System.Windows.Forms.TabPage();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.prev = new MaterialSkin.Controls.MaterialRadioButton();
            this.stable = new MaterialSkin.Controls.MaterialRadioButton();
            this.changelogButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.autoUpdCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.fupdButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.informations = new System.Windows.Forms.TabPage();
            this.GitBtn = new MaterialSkin.Controls.MaterialRaisedButton();
            this.vmtYTbut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.vmtFBBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.websiteNewBut = new MaterialSkin.Controls.MaterialRaisedButton();
            this.copyrightLab = new MaterialSkin.Controls.MaterialLabel();
            this.infoLab = new MaterialSkin.Controls.MaterialLabel();
            this.devset = new System.Windows.Forms.TabPage();
            this.hideSplashes = new MaterialSkin.Controls.MaterialCheckBox();
            this.forceStarters = new MaterialSkin.Controls.MaterialRaisedButton();
            this.hideDevAvailable = new MaterialSkin.Controls.MaterialFlatButton();
            this.verNprofVisible = new MaterialSkin.Controls.MaterialCheckBox();
            this.devCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.preferences = new System.Windows.Forms.TabPage();
            this.blueAndGrey = new MaterialSkin.Controls.MaterialRadioButton();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.greyOnly = new MaterialSkin.Controls.MaterialRadioButton();
            this.NoDarkModeCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.AlwaysDarkModeCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.hideSplash = new MaterialSkin.Controls.MaterialCheckBox();
            this.deskShortCheck = new MaterialSkin.Controls.MaterialCheckBox();
            this.updateTab = new System.Windows.Forms.TabPage();
            this.TabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.ver_label = new System.Windows.Forms.Label();
            this.splash = new System.Windows.Forms.Label();
            this.ModName = new System.Windows.Forms.Label();
            this.Tab.SuspendLayout();
            this.homeTab.SuspendLayout();
            this.gPanel.SuspendLayout();
            this.testPanel.SuspendLayout();
            this.playContext.SuspendLayout();
            this.logButContext.SuspendLayout();
            this.webPanel.SuspendLayout();
            this.gameTab.SuspendLayout();
            this.game.SuspendLayout();
            this.basic.SuspendLayout();
            this.tirePanel.SuspendLayout();
            this.plr.SuspendLayout();
            this.quick_chat.SuspendLayout();
            this.vmtTab.SuspendLayout();
            this.vmtPan.SuspendLayout();
            this.vmtP.SuspendLayout();
            this.classic.SuspendLayout();
            this._15.SuspendLayout();
            this._16.SuspendLayout();
            this._17.SuspendLayout();
            this.settingsTab.SuspendLayout();
            this.settings.SuspendLayout();
            this.main.SuspendLayout();
            this.games.SuspendLayout();
            this.updates.SuspendLayout();
            this.informations.SuspendLayout();
            this.devset.SuspendLayout();
            this.preferences.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tab
            // 
            this.Tab.Controls.Add(this.homeTab);
            this.Tab.Controls.Add(this.gameTab);
            this.Tab.Controls.Add(this.vmtTab);
            this.Tab.Controls.Add(this.settingsTab);
            this.Tab.Depth = 0;
            this.Tab.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Tab.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Tab.Location = new System.Drawing.Point(0, 87);
            this.Tab.MouseState = MaterialSkin.MouseState.HOVER;
            this.Tab.Name = "Tab";
            this.Tab.SelectedIndex = 0;
            this.Tab.Size = new System.Drawing.Size(712, 375);
            this.Tab.TabIndex = 2;
            // 
            // homeTab
            // 
            this.homeTab.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.homeTab.Controls.Add(this.gPanel);
            this.homeTab.Controls.Add(this.webPanel);
            this.homeTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.homeTab.Location = new System.Drawing.Point(4, 22);
            this.homeTab.Name = "homeTab";
            this.homeTab.Padding = new System.Windows.Forms.Padding(3);
            this.homeTab.Size = new System.Drawing.Size(704, 349);
            this.homeTab.TabIndex = 0;
            this.homeTab.Text = "Home";
            // 
            // gPanel
            // 
            this.gPanel.AutoScroll = true;
            this.gPanel.Controls.Add(this.urOffline);
            this.gPanel.Controls.Add(this.gamedirTile);
            this.gPanel.Controls.Add(this.testPanel);
            this.gPanel.Controls.Add(this.bugTrackTile);
            this.gPanel.Controls.Add(this.playTile);
            this.gPanel.Controls.Add(this.telemetryTile);
            this.gPanel.Controls.Add(this.chatTile);
            this.gPanel.Controls.Add(this.plrTile);
            this.gPanel.Controls.Add(this.confTile);
            this.gPanel.Controls.Add(this.logTile);
            this.gPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gPanel.Location = new System.Drawing.Point(3, 3);
            this.gPanel.Name = "gPanel";
            this.gPanel.Size = new System.Drawing.Size(698, 343);
            this.gPanel.TabIndex = 0;
            // 
            // urOffline
            // 
            this.urOffline.AutoSize = true;
            this.urOffline.BackColor = System.Drawing.Color.Transparent;
            this.urOffline.Depth = 0;
            this.urOffline.Font = new System.Drawing.Font("Roboto", 11F);
            this.urOffline.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.urOffline.Location = new System.Drawing.Point(425, 181);
            this.urOffline.MouseState = MaterialSkin.MouseState.HOVER;
            this.urOffline.Name = "urOffline";
            this.urOffline.Size = new System.Drawing.Size(220, 38);
            this.urOffline.TabIndex = 62;
            this.urOffline.Text = "You\'re offline!\r\nAll online features are disabled.";
            this.urOffline.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.urOffline.Visible = false;
            // 
            // gamedirTile
            // 
            this.gamedirTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.gamedirTile.FlatAppearance.BorderSize = 0;
            this.gamedirTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gamedirTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.gamedirTile.ForeColor = System.Drawing.Color.Black;
            this.gamedirTile.Image = global::SimPT.Properties.Resources.dirIcon;
            this.gamedirTile.Location = new System.Drawing.Point(407, 3);
            this.gamedirTile.Name = "gamedirTile";
            this.gamedirTile.Size = new System.Drawing.Size(124, 124);
            this.gamedirTile.TabIndex = 61;
            this.gamedirTile.Text = "Game folder";
            this.gamedirTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.gamedirTile.UseVisualStyleBackColor = true;
            this.gamedirTile.Click += new System.EventHandler(this.gamedirTile_Click);
            // 
            // testPanel
            // 
            this.testPanel.AutoScroll = true;
            this.testPanel.Controls.Add(this.BtnYesNo);
            this.testPanel.Controls.Add(this.TestMsgBoxBut);
            this.testPanel.Controls.Add(this.testUpdateWindowTile);
            this.testPanel.Controls.Add(this.testWelcomeTile);
            this.testPanel.Location = new System.Drawing.Point(17, 263);
            this.testPanel.Name = "testPanel";
            this.testPanel.Size = new System.Drawing.Size(644, 137);
            this.testPanel.TabIndex = 51;
            this.testPanel.Visible = false;
            // 
            // BtnYesNo
            // 
            this.BtnYesNo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.BtnYesNo.FlatAppearance.BorderSize = 0;
            this.BtnYesNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnYesNo.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.BtnYesNo.ForeColor = System.Drawing.Color.Black;
            this.BtnYesNo.Image = global::SimPT.Properties.Resources.testIcon;
            this.BtnYesNo.Location = new System.Drawing.Point(390, 0);
            this.BtnYesNo.Name = "BtnYesNo";
            this.BtnYesNo.Size = new System.Drawing.Size(124, 124);
            this.BtnYesNo.TabIndex = 61;
            this.BtnYesNo.Text = "TEST_MSG_BOX_YESNO";
            this.BtnYesNo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnYesNo.UseVisualStyleBackColor = true;
            this.BtnYesNo.Click += new System.EventHandler(this.BtnYesNo_Click);
            // 
            // TestMsgBoxBut
            // 
            this.TestMsgBoxBut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.TestMsgBoxBut.FlatAppearance.BorderSize = 0;
            this.TestMsgBoxBut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TestMsgBoxBut.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.TestMsgBoxBut.ForeColor = System.Drawing.Color.Black;
            this.TestMsgBoxBut.Image = global::SimPT.Properties.Resources.testIcon;
            this.TestMsgBoxBut.Location = new System.Drawing.Point(260, 0);
            this.TestMsgBoxBut.Name = "TestMsgBoxBut";
            this.TestMsgBoxBut.Size = new System.Drawing.Size(124, 124);
            this.TestMsgBoxBut.TabIndex = 60;
            this.TestMsgBoxBut.Text = "TEST_MSG_BOX";
            this.TestMsgBoxBut.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TestMsgBoxBut.UseVisualStyleBackColor = true;
            this.TestMsgBoxBut.Click += new System.EventHandler(this.TestMsgBoxBut_Click);
            // 
            // testUpdateWindowTile
            // 
            this.testUpdateWindowTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.testUpdateWindowTile.FlatAppearance.BorderSize = 0;
            this.testUpdateWindowTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testUpdateWindowTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.testUpdateWindowTile.ForeColor = System.Drawing.Color.Black;
            this.testUpdateWindowTile.Image = global::SimPT.Properties.Resources.testIcon;
            this.testUpdateWindowTile.Location = new System.Drawing.Point(3, -3);
            this.testUpdateWindowTile.Name = "testUpdateWindowTile";
            this.testUpdateWindowTile.Size = new System.Drawing.Size(124, 124);
            this.testUpdateWindowTile.TabIndex = 57;
            this.testUpdateWindowTile.Text = "TEST_UPDATE WINDOW";
            this.testUpdateWindowTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.testUpdateWindowTile.UseVisualStyleBackColor = true;
            this.testUpdateWindowTile.Click += new System.EventHandler(this.testUpdateWindowTile_Click);
            // 
            // testWelcomeTile
            // 
            this.testWelcomeTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.testWelcomeTile.FlatAppearance.BorderSize = 0;
            this.testWelcomeTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testWelcomeTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.testWelcomeTile.ForeColor = System.Drawing.Color.Black;
            this.testWelcomeTile.Image = global::SimPT.Properties.Resources.testIcon;
            this.testWelcomeTile.Location = new System.Drawing.Point(133, -3);
            this.testWelcomeTile.Name = "testWelcomeTile";
            this.testWelcomeTile.Size = new System.Drawing.Size(124, 124);
            this.testWelcomeTile.TabIndex = 59;
            this.testWelcomeTile.Text = "TEST_WELCOME";
            this.testWelcomeTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.testWelcomeTile.UseVisualStyleBackColor = true;
            this.testWelcomeTile.Click += new System.EventHandler(this.testWelcomeTile_Click);
            // 
            // bugTrackTile
            // 
            this.bugTrackTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bugTrackTile.FlatAppearance.BorderSize = 0;
            this.bugTrackTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bugTrackTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.bugTrackTile.ForeColor = System.Drawing.Color.Black;
            this.bugTrackTile.Image = global::SimPT.Properties.Resources.bugIcon;
            this.bugTrackTile.Location = new System.Drawing.Point(277, 3);
            this.bugTrackTile.Name = "bugTrackTile";
            this.bugTrackTile.Size = new System.Drawing.Size(124, 124);
            this.bugTrackTile.TabIndex = 57;
            this.bugTrackTile.Text = "Bug Tracker";
            this.bugTrackTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bugTrackTile.UseVisualStyleBackColor = true;
            this.bugTrackTile.Click += new System.EventHandler(this.tile2_Click);
            // 
            // playTile
            // 
            this.playTile.BackgroundImage = global::SimPT.Properties.Resources.contextIco;
            this.playTile.ContextMenuStrip = this.playContext;
            this.playTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.playTile.FlatAppearance.BorderSize = 0;
            this.playTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.playTile.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.playTile.ForeColor = System.Drawing.Color.Black;
            this.playTile.Image = global::SimPT.Properties.Resources.playIco;
            this.playTile.Location = new System.Drawing.Point(17, 3);
            this.playTile.Name = "playTile";
            this.playTile.Size = new System.Drawing.Size(124, 124);
            this.playTile.TabIndex = 52;
            this.playTile.Text = "Play";
            this.playTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.playTile.UseVisualStyleBackColor = true;
            this.playTile.Click += new System.EventHandler(this.playTile_Click);
            // 
            // playContext
            // 
            this.playContext.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.playContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playAndShutdownVMTEngineToolStripMenuItem,
            this.bugTrackerToolStripMenuItem,
            this.toolStripSeparator1,
            this.setCompatibilityModeToolStripMenuItem});
            this.playContext.Name = "contextMenuStrip1";
            this.playContext.ShowImageMargin = false;
            this.playContext.Size = new System.Drawing.Size(258, 88);
            // 
            // playAndShutdownVMTEngineToolStripMenuItem
            // 
            this.playAndShutdownVMTEngineToolStripMenuItem.Name = "playAndShutdownVMTEngineToolStripMenuItem";
            this.playAndShutdownVMTEngineToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
            this.playAndShutdownVMTEngineToolStripMenuItem.Text = "Play and turn off VMT Engine";
            this.playAndShutdownVMTEngineToolStripMenuItem.Click += new System.EventHandler(this.playAndShutdownVMTEngineToolStripMenuItem_Click);
            // 
            // bugTrackerToolStripMenuItem
            // 
            this.bugTrackerToolStripMenuItem.Name = "bugTrackerToolStripMenuItem";
            this.bugTrackerToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
            this.bugTrackerToolStripMenuItem.Text = "Bug Tracker";
            this.bugTrackerToolStripMenuItem.Click += new System.EventHandler(this.bugTrackerToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(254, 6);
            // 
            // setCompatibilityModeToolStripMenuItem
            // 
            this.setCompatibilityModeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.setCompatibilityModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oNToolStripMenuItem,
            this.toolStripSeparator2,
            this.didntHelpedToolStripMenuItem});
            this.setCompatibilityModeToolStripMenuItem.Name = "setCompatibilityModeToolStripMenuItem";
            this.setCompatibilityModeToolStripMenuItem.Size = new System.Drawing.Size(257, 26);
            this.setCompatibilityModeToolStripMenuItem.Text = "Compatibility mode";
            // 
            // oNToolStripMenuItem
            // 
            this.oNToolStripMenuItem.Name = "oNToolStripMenuItem";
            this.oNToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.oNToolStripMenuItem.Text = "Run the tool";
            this.oNToolStripMenuItem.Click += new System.EventHandler(this.oNToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // didntHelpedToolStripMenuItem
            // 
            this.didntHelpedToolStripMenuItem.Name = "didntHelpedToolStripMenuItem";
            this.didntHelpedToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.didntHelpedToolStripMenuItem.Text = "Didn\'t helped?";
            this.didntHelpedToolStripMenuItem.Click += new System.EventHandler(this.didntHelpedToolStripMenuItem_Click);
            // 
            // telemetryTile
            // 
            this.telemetryTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.telemetryTile.FlatAppearance.BorderSize = 0;
            this.telemetryTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.telemetryTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.telemetryTile.ForeColor = System.Drawing.Color.Black;
            this.telemetryTile.Image = global::SimPT.Properties.Resources.telemetryIcon;
            this.telemetryTile.Location = new System.Drawing.Point(537, 3);
            this.telemetryTile.Name = "telemetryTile";
            this.telemetryTile.Size = new System.Drawing.Size(124, 124);
            this.telemetryTile.TabIndex = 59;
            this.telemetryTile.Text = "Telemetry";
            this.telemetryTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.telemetryTile.UseVisualStyleBackColor = true;
            this.telemetryTile.Click += new System.EventHandler(this.tile3_Click);
            // 
            // chatTile
            // 
            this.chatTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.chatTile.FlatAppearance.BorderSize = 0;
            this.chatTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chatTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.chatTile.ForeColor = System.Drawing.Color.Black;
            this.chatTile.Image = global::SimPT.Properties.Resources.chatIcon;
            this.chatTile.Location = new System.Drawing.Point(277, 133);
            this.chatTile.Name = "chatTile";
            this.chatTile.Size = new System.Drawing.Size(124, 124);
            this.chatTile.TabIndex = 53;
            this.chatTile.Text = "Quick Chat Creator";
            this.chatTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.chatTile.UseVisualStyleBackColor = true;
            this.chatTile.Click += new System.EventHandler(this.tile6_Click);
            // 
            // plrTile
            // 
            this.plrTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.plrTile.FlatAppearance.BorderSize = 0;
            this.plrTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.plrTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.plrTile.ForeColor = System.Drawing.Color.Black;
            this.plrTile.Image = global::SimPT.Properties.Resources.profileIcon;
            this.plrTile.Location = new System.Drawing.Point(147, 133);
            this.plrTile.Name = "plrTile";
            this.plrTile.Size = new System.Drawing.Size(124, 124);
            this.plrTile.TabIndex = 58;
            this.plrTile.Text = "PLR Editor";
            this.plrTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.plrTile.UseVisualStyleBackColor = true;
            this.plrTile.Click += new System.EventHandler(this.tile5_Click);
            // 
            // confTile
            // 
            this.confTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.confTile.FlatAppearance.BorderSize = 0;
            this.confTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.confTile.ForeColor = System.Drawing.Color.Black;
            this.confTile.Image = global::SimPT.Properties.Resources.configIco;
            this.confTile.Location = new System.Drawing.Point(147, 3);
            this.confTile.Name = "confTile";
            this.confTile.Size = new System.Drawing.Size(124, 124);
            this.confTile.TabIndex = 55;
            this.confTile.Text = "Config";
            this.confTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.confTile.UseVisualStyleBackColor = true;
            this.confTile.Click += new System.EventHandler(this.tile1_Click);
            // 
            // logTile
            // 
            this.logTile.BackgroundImage = global::SimPT.Properties.Resources.contextIco;
            this.logTile.ContextMenuStrip = this.logButContext;
            this.logTile.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.logTile.FlatAppearance.BorderSize = 0;
            this.logTile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logTile.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.logTile.ForeColor = System.Drawing.Color.Black;
            this.logTile.Image = global::SimPT.Properties.Resources.logIcon;
            this.logTile.Location = new System.Drawing.Point(17, 133);
            this.logTile.Name = "logTile";
            this.logTile.Size = new System.Drawing.Size(124, 124);
            this.logTile.TabIndex = 56;
            this.logTile.Text = "Check log";
            this.logTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.logTile.UseVisualStyleBackColor = true;
            this.logTile.Click += new System.EventHandler(this.tile4_Click);
            // 
            // logButContext
            // 
            this.logButContext.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.logButContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.logButContext.Name = "contextMenuStrip1";
            this.logButContext.ShowImageMargin = false;
            this.logButContext.Size = new System.Drawing.Size(126, 30);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(125, 26);
            this.toolStripMenuItem1.Text = "Delete log";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // webPanel
            // 
            this.webPanel.Controls.Add(this.htmlView);
            this.webPanel.Location = new System.Drawing.Point(774, 205);
            this.webPanel.Name = "webPanel";
            this.webPanel.Size = new System.Drawing.Size(263, 338);
            this.webPanel.TabIndex = 51;
            // 
            // htmlView
            // 
            this.htmlView.Location = new System.Drawing.Point(46, 33);
            this.htmlView.MinimumSize = new System.Drawing.Size(20, 20);
            this.htmlView.Name = "htmlView";
            this.htmlView.Size = new System.Drawing.Size(263, 338);
            this.htmlView.TabIndex = 0;
            // 
            // gameTab
            // 
            this.gameTab.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.gameTab.Controls.Add(this.game);
            this.gameTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.gameTab.Location = new System.Drawing.Point(4, 22);
            this.gameTab.Name = "gameTab";
            this.gameTab.Padding = new System.Windows.Forms.Padding(3);
            this.gameTab.Size = new System.Drawing.Size(704, 349);
            this.gameTab.TabIndex = 1;
            this.gameTab.Text = "Game";
            // 
            // game
            // 
            this.game.Controls.Add(this.basic);
            this.game.Controls.Add(this.plr);
            this.game.Controls.Add(this.quick_chat);
            this.game.Depth = 0;
            this.game.Dock = System.Windows.Forms.DockStyle.Fill;
            this.game.Location = new System.Drawing.Point(3, 3);
            this.game.MouseState = MaterialSkin.MouseState.HOVER;
            this.game.Name = "game";
            this.game.SelectedIndex = 0;
            this.game.Size = new System.Drawing.Size(698, 343);
            this.game.TabIndex = 24;
            // 
            // basic
            // 
            this.basic.BackColor = System.Drawing.Color.White;
            this.basic.Controls.Add(this.DownloadLangPackBut);
            this.basic.Controls.Add(this.langSelect);
            this.basic.Controls.Add(this.materialRaisedButton1);
            this.basic.Controls.Add(this.vsyncF1CLab);
            this.basic.Controls.Add(this.settingsF1CLab);
            this.basic.Controls.Add(this.vidF1c);
            this.basic.Controls.Add(this.windowedF1CLab);
            this.basic.Controls.Add(this.mphTelF1CLab);
            this.basic.Controls.Add(this.tirePanel);
            this.basic.Controls.Add(this.antiAliasignF1CLab);
            this.basic.Controls.Add(this.langLab);
            this.basic.ForeColor = System.Drawing.SystemColors.ControlText;
            this.basic.Location = new System.Drawing.Point(4, 22);
            this.basic.Name = "basic";
            this.basic.Padding = new System.Windows.Forms.Padding(3);
            this.basic.Size = new System.Drawing.Size(690, 317);
            this.basic.TabIndex = 0;
            this.basic.Text = "basic";
            // 
            // DownloadLangPackBut
            // 
            this.DownloadLangPackBut.AutoSize = true;
            this.DownloadLangPackBut.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.DownloadLangPackBut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DownloadLangPackBut.Depth = 0;
            this.DownloadLangPackBut.Location = new System.Drawing.Point(27, 81);
            this.DownloadLangPackBut.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.DownloadLangPackBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.DownloadLangPackBut.Name = "DownloadLangPackBut";
            this.DownloadLangPackBut.Primary = false;
            this.DownloadLangPackBut.Size = new System.Drawing.Size(201, 36);
            this.DownloadLangPackBut.TabIndex = 62;
            this.DownloadLangPackBut.Text = "Download Language pack";
            this.DownloadLangPackBut.UseVisualStyleBackColor = true;
            this.DownloadLangPackBut.Click += new System.EventHandler(this.DownloadLangPackBut_Click);
            // 
            // langSelect
            // 
            this.langSelect.BackColor = System.Drawing.SystemColors.Window;
            this.langSelect.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.langSelect.ItemHeight = 25;
            this.langSelect.Items.AddRange(new object[] {
            "English",
            "Finnish",
            "French",
            "German",
            "Hungarian",
            "Italian",
            "Polish",
            "Portuguese",
            "Spanish"});
            this.langSelect.Location = new System.Drawing.Point(28, 39);
            this.langSelect.Name = "langSelect";
            this.langSelect.Size = new System.Drawing.Size(255, 33);
            this.langSelect.TabIndex = 16;
            this.langSelect.SelectionChangeCommitted += new System.EventHandler(this.langSelect_SelectionChangeCommitted);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(413, 208);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(106, 34);
            this.materialRaisedButton1.TabIndex = 25;
            this.materialRaisedButton1.Text = "Delete replays";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click_2);
            // 
            // vsyncF1CLab
            // 
            this.vsyncF1CLab.AutoSize = true;
            this.vsyncF1CLab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.vsyncF1CLab.Depth = 0;
            this.vsyncF1CLab.Font = new System.Drawing.Font("Roboto", 10F);
            this.vsyncF1CLab.Location = new System.Drawing.Point(413, 98);
            this.vsyncF1CLab.Margin = new System.Windows.Forms.Padding(0);
            this.vsyncF1CLab.MouseLocation = new System.Drawing.Point(-1, -1);
            this.vsyncF1CLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.vsyncF1CLab.Name = "vsyncF1CLab";
            this.vsyncF1CLab.Ripple = true;
            this.vsyncF1CLab.Size = new System.Drawing.Size(73, 30);
            this.vsyncF1CLab.TabIndex = 22;
            this.vsyncF1CLab.Text = "V-Sync";
            this.vsyncF1CLab.UseVisualStyleBackColor = true;
            this.vsyncF1CLab.Click += new System.EventHandler(this.materialCheckBox4_Click);
            // 
            // settingsF1CLab
            // 
            this.settingsF1CLab.AutoSize = true;
            this.settingsF1CLab.Depth = 0;
            this.settingsF1CLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.settingsF1CLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.settingsF1CLab.Location = new System.Drawing.Point(409, 7);
            this.settingsF1CLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.settingsF1CLab.Name = "settingsF1CLab";
            this.settingsF1CLab.Size = new System.Drawing.Size(53, 19);
            this.settingsF1CLab.TabIndex = 19;
            this.settingsF1CLab.Text = "Config";
            // 
            // vidF1c
            // 
            this.vidF1c.AutoSize = true;
            this.vidF1c.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.vidF1c.Depth = 0;
            this.vidF1c.Font = new System.Drawing.Font("Roboto", 10F);
            this.vidF1c.Location = new System.Drawing.Point(413, 158);
            this.vidF1c.Margin = new System.Windows.Forms.Padding(0);
            this.vidF1c.MouseLocation = new System.Drawing.Point(-1, -1);
            this.vidF1c.MouseState = MaterialSkin.MouseState.HOVER;
            this.vidF1c.Name = "vidF1c";
            this.vidF1c.Ripple = true;
            this.vidF1c.Size = new System.Drawing.Size(172, 30);
            this.vidF1c.TabIndex = 24;
            this.vidF1c.Text = "Disable videos in game";
            this.vidF1c.UseVisualStyleBackColor = true;
            this.vidF1c.Click += new System.EventHandler(this.vidF1c_Click);
            // 
            // windowedF1CLab
            // 
            this.windowedF1CLab.AutoSize = true;
            this.windowedF1CLab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.windowedF1CLab.Depth = 0;
            this.windowedF1CLab.Font = new System.Drawing.Font("Roboto", 10F);
            this.windowedF1CLab.Location = new System.Drawing.Point(413, 68);
            this.windowedF1CLab.Margin = new System.Windows.Forms.Padding(0);
            this.windowedF1CLab.MouseLocation = new System.Drawing.Point(-1, -1);
            this.windowedF1CLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.windowedF1CLab.Name = "windowedF1CLab";
            this.windowedF1CLab.Ripple = true;
            this.windowedF1CLab.Size = new System.Drawing.Size(132, 30);
            this.windowedF1CLab.TabIndex = 20;
            this.windowedF1CLab.Text = "Windowed mode";
            this.windowedF1CLab.UseVisualStyleBackColor = true;
            this.windowedF1CLab.Click += new System.EventHandler(this.materialCheckBox2_Click);
            // 
            // mphTelF1CLab
            // 
            this.mphTelF1CLab.AutoSize = true;
            this.mphTelF1CLab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.mphTelF1CLab.Depth = 0;
            this.mphTelF1CLab.Font = new System.Drawing.Font("Roboto", 10F);
            this.mphTelF1CLab.Location = new System.Drawing.Point(413, 128);
            this.mphTelF1CLab.Margin = new System.Windows.Forms.Padding(0);
            this.mphTelF1CLab.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mphTelF1CLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.mphTelF1CLab.Name = "mphTelF1CLab";
            this.mphTelF1CLab.Ripple = true;
            this.mphTelF1CLab.Size = new System.Drawing.Size(249, 30);
            this.mphTelF1CLab.TabIndex = 21;
            this.mphTelF1CLab.Text = "Use MPH as speed unit in telemetry";
            this.mphTelF1CLab.UseVisualStyleBackColor = true;
            this.mphTelF1CLab.Click += new System.EventHandler(this.materialCheckBox3_Click);
            // 
            // tirePanel
            // 
            this.tirePanel.Controls.Add(this.tireSelect);
            this.tirePanel.Controls.Add(this.materialLabel5);
            this.tirePanel.Location = new System.Drawing.Point(28, 128);
            this.tirePanel.Name = "tirePanel";
            this.tirePanel.Size = new System.Drawing.Size(265, 72);
            this.tirePanel.TabIndex = 23;
            this.tirePanel.Visible = false;
            // 
            // tireSelect
            // 
            this.tireSelect.BackColor = System.Drawing.SystemColors.Window;
            this.tireSelect.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.tireSelect.ItemHeight = 25;
            this.tireSelect.Items.AddRange(new object[] {
            "Medium - Hard",
            "Soft - Hard",
            "Soft - Medium",
            "Super Soft - Soft"});
            this.tireSelect.Location = new System.Drawing.Point(0, 33);
            this.tireSelect.Name = "tireSelect";
            this.tireSelect.Size = new System.Drawing.Size(255, 33);
            this.tireSelect.TabIndex = 16;
            this.tireSelect.SelectionChangeCommitted += new System.EventHandler(this.tireSelect_SelectionChangeCommitted);
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(0, 0);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(43, 19);
            this.materialLabel5.TabIndex = 15;
            this.materialLabel5.Text = "Tires";
            // 
            // antiAliasignF1CLab
            // 
            this.antiAliasignF1CLab.AutoSize = true;
            this.antiAliasignF1CLab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.antiAliasignF1CLab.Depth = 0;
            this.antiAliasignF1CLab.Font = new System.Drawing.Font("Roboto", 10F);
            this.antiAliasignF1CLab.Location = new System.Drawing.Point(413, 38);
            this.antiAliasignF1CLab.Margin = new System.Windows.Forms.Padding(0);
            this.antiAliasignF1CLab.MouseLocation = new System.Drawing.Point(-1, -1);
            this.antiAliasignF1CLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.antiAliasignF1CLab.Name = "antiAliasignF1CLab";
            this.antiAliasignF1CLab.Ripple = true;
            this.antiAliasignF1CLab.Size = new System.Drawing.Size(107, 30);
            this.antiAliasignF1CLab.TabIndex = 18;
            this.antiAliasignF1CLab.Text = "Anti-aliasing";
            this.antiAliasignF1CLab.UseVisualStyleBackColor = true;
            this.antiAliasignF1CLab.Click += new System.EventHandler(this.materialCheckBox1_Click);
            // 
            // langLab
            // 
            this.langLab.AutoSize = true;
            this.langLab.Depth = 0;
            this.langLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.langLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.langLab.Location = new System.Drawing.Point(23, 7);
            this.langLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.langLab.Name = "langLab";
            this.langLab.Size = new System.Drawing.Size(73, 19);
            this.langLab.TabIndex = 11;
            this.langLab.Text = "Language";
            // 
            // plr
            // 
            this.plr.AutoScroll = true;
            this.plr.BackColor = System.Drawing.Color.White;
            this.plr.Controls.Add(this.MovingSteeringWheeleck);
            this.plr.Controls.Add(this.LCDMode);
            this.plr.Controls.Add(this.TrackLoadCommentary);
            this.plr.Controls.Add(this.OpenInNotepadPLR);
            this.plr.Controls.Add(this.ClosedMP);
            this.plr.Controls.Add(this.ShowYourCar);
            this.plr.Controls.Add(this.ExtraConfigurableButtons);
            this.plr.Controls.Add(this.ShowChat);
            this.plr.Controls.Add(this.CompareTimeToFirstDriver);
            this.plr.Controls.Add(this.PreventAI);
            this.plr.Controls.Add(this.ReconnaissanceLap);
            this.plr.Controls.Add(this.FormationLap);
            this.plr.Controls.Add(this.ProfileChatProfiles);
            this.plr.Controls.Add(this.materialLabel3);
            this.plr.Location = new System.Drawing.Point(4, 22);
            this.plr.Name = "plr";
            this.plr.Padding = new System.Windows.Forms.Padding(3);
            this.plr.Size = new System.Drawing.Size(690, 317);
            this.plr.TabIndex = 1;
            this.plr.Text = "plr";
            // 
            // MovingSteeringWheeleck
            // 
            this.MovingSteeringWheeleck.AutoSize = true;
            this.MovingSteeringWheeleck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.MovingSteeringWheeleck.Depth = 0;
            this.MovingSteeringWheeleck.Font = new System.Drawing.Font("Roboto", 10F);
            this.MovingSteeringWheeleck.Location = new System.Drawing.Point(10, 380);
            this.MovingSteeringWheeleck.Margin = new System.Windows.Forms.Padding(0);
            this.MovingSteeringWheeleck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.MovingSteeringWheeleck.MouseState = MaterialSkin.MouseState.HOVER;
            this.MovingSteeringWheeleck.Name = "MovingSteeringWheeleck";
            this.MovingSteeringWheeleck.Ripple = true;
            this.MovingSteeringWheeleck.Size = new System.Drawing.Size(172, 30);
            this.MovingSteeringWheeleck.TabIndex = 69;
            this.MovingSteeringWheeleck.Text = "Moving Steering Wheel";
            this.MovingSteeringWheeleck.UseVisualStyleBackColor = true;
            this.MovingSteeringWheeleck.Click += new System.EventHandler(this.MovingSteeringWheeleck_Click);
            // 
            // LCDMode
            // 
            this.LCDMode.AutoSize = true;
            this.LCDMode.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.LCDMode.Depth = 0;
            this.LCDMode.Font = new System.Drawing.Font("Roboto", 10F);
            this.LCDMode.Location = new System.Drawing.Point(10, 350);
            this.LCDMode.Margin = new System.Windows.Forms.Padding(0);
            this.LCDMode.MouseLocation = new System.Drawing.Point(-1, -1);
            this.LCDMode.MouseState = MaterialSkin.MouseState.HOVER;
            this.LCDMode.Name = "LCDMode";
            this.LCDMode.Ripple = true;
            this.LCDMode.Size = new System.Drawing.Size(209, 30);
            this.LCDMode.TabIndex = 68;
            this.LCDMode.Text = "Temperatures on LCD screen";
            this.LCDMode.UseVisualStyleBackColor = true;
            this.LCDMode.Click += new System.EventHandler(this.LCDMode_Click);
            // 
            // TrackLoadCommentary
            // 
            this.TrackLoadCommentary.AutoSize = true;
            this.TrackLoadCommentary.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.TrackLoadCommentary.Depth = 0;
            this.TrackLoadCommentary.Font = new System.Drawing.Font("Roboto", 10F);
            this.TrackLoadCommentary.Location = new System.Drawing.Point(10, 320);
            this.TrackLoadCommentary.Margin = new System.Windows.Forms.Padding(0);
            this.TrackLoadCommentary.MouseLocation = new System.Drawing.Point(-1, -1);
            this.TrackLoadCommentary.MouseState = MaterialSkin.MouseState.HOVER;
            this.TrackLoadCommentary.Name = "TrackLoadCommentary";
            this.TrackLoadCommentary.Ripple = true;
            this.TrackLoadCommentary.Size = new System.Drawing.Size(199, 30);
            this.TrackLoadCommentary.TabIndex = 67;
            this.TrackLoadCommentary.Text = "Commentator while loading";
            this.TrackLoadCommentary.UseVisualStyleBackColor = true;
            this.TrackLoadCommentary.Click += new System.EventHandler(this.TrackLoadCommentary_Click);
            // 
            // OpenInNotepadPLR
            // 
            this.OpenInNotepadPLR.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.OpenInNotepadPLR.Depth = 0;
            this.OpenInNotepadPLR.Location = new System.Drawing.Point(212, 31);
            this.OpenInNotepadPLR.MouseState = MaterialSkin.MouseState.HOVER;
            this.OpenInNotepadPLR.Name = "OpenInNotepadPLR";
            this.OpenInNotepadPLR.Primary = true;
            this.OpenInNotepadPLR.Size = new System.Drawing.Size(98, 34);
            this.OpenInNotepadPLR.TabIndex = 66;
            this.OpenInNotepadPLR.Text = "Open with text editor";
            this.OpenInNotepadPLR.UseVisualStyleBackColor = true;
            this.OpenInNotepadPLR.Click += new System.EventHandler(this.OpenInNotepadPLR_Click);
            // 
            // ClosedMP
            // 
            this.ClosedMP.AutoSize = true;
            this.ClosedMP.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ClosedMP.Depth = 0;
            this.ClosedMP.Font = new System.Drawing.Font("Roboto", 10F);
            this.ClosedMP.Location = new System.Drawing.Point(10, 290);
            this.ClosedMP.Margin = new System.Windows.Forms.Padding(0);
            this.ClosedMP.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ClosedMP.MouseState = MaterialSkin.MouseState.HOVER;
            this.ClosedMP.Name = "ClosedMP";
            this.ClosedMP.Ripple = true;
            this.ClosedMP.Size = new System.Drawing.Size(365, 30);
            this.ClosedMP.TabIndex = 65;
            this.ClosedMP.Text = "Closed MP Qualify Sessions (Multiplayer, HOST ONLY)";
            this.ClosedMP.UseVisualStyleBackColor = true;
            this.ClosedMP.Click += new System.EventHandler(this.ClosedMP_Click);
            // 
            // ShowYourCar
            // 
            this.ShowYourCar.AutoSize = true;
            this.ShowYourCar.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ShowYourCar.Depth = 0;
            this.ShowYourCar.Font = new System.Drawing.Font("Roboto", 10F);
            this.ShowYourCar.Location = new System.Drawing.Point(10, 260);
            this.ShowYourCar.Margin = new System.Windows.Forms.Padding(0);
            this.ShowYourCar.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ShowYourCar.MouseState = MaterialSkin.MouseState.HOVER;
            this.ShowYourCar.Name = "ShowYourCar";
            this.ShowYourCar.Ripple = true;
            this.ShowYourCar.Size = new System.Drawing.Size(180, 30);
            this.ShowYourCar.TabIndex = 64;
            this.ShowYourCar.Text = "Show your car in mirrors";
            this.ShowYourCar.UseVisualStyleBackColor = true;
            this.ShowYourCar.Click += new System.EventHandler(this.ShowYourCar_Click);
            // 
            // ExtraConfigurableButtons
            // 
            this.ExtraConfigurableButtons.AutoSize = true;
            this.ExtraConfigurableButtons.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ExtraConfigurableButtons.Depth = 0;
            this.ExtraConfigurableButtons.Font = new System.Drawing.Font("Roboto", 10F);
            this.ExtraConfigurableButtons.Location = new System.Drawing.Point(10, 230);
            this.ExtraConfigurableButtons.Margin = new System.Windows.Forms.Padding(0);
            this.ExtraConfigurableButtons.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ExtraConfigurableButtons.MouseState = MaterialSkin.MouseState.HOVER;
            this.ExtraConfigurableButtons.Name = "ExtraConfigurableButtons";
            this.ExtraConfigurableButtons.Ripple = true;
            this.ExtraConfigurableButtons.Size = new System.Drawing.Size(193, 30);
            this.ExtraConfigurableButtons.TabIndex = 63;
            this.ExtraConfigurableButtons.Text = "Extra configurable buttons";
            this.ExtraConfigurableButtons.UseVisualStyleBackColor = true;
            this.ExtraConfigurableButtons.Click += new System.EventHandler(this.ExtraConfigurableButtons_Click);
            // 
            // ShowChat
            // 
            this.ShowChat.AutoSize = true;
            this.ShowChat.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ShowChat.Depth = 0;
            this.ShowChat.Font = new System.Drawing.Font("Roboto", 10F);
            this.ShowChat.Location = new System.Drawing.Point(10, 200);
            this.ShowChat.Margin = new System.Windows.Forms.Padding(0);
            this.ShowChat.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ShowChat.MouseState = MaterialSkin.MouseState.HOVER;
            this.ShowChat.Name = "ShowChat";
            this.ShowChat.Ripple = true;
            this.ShowChat.Size = new System.Drawing.Size(213, 30);
            this.ShowChat.TabIndex = 62;
            this.ShowChat.Text = "Chat while in car (Multiplayer)";
            this.ShowChat.UseVisualStyleBackColor = true;
            this.ShowChat.Click += new System.EventHandler(this.ShowChat_Click);
            // 
            // CompareTimeToFirstDriver
            // 
            this.CompareTimeToFirstDriver.AutoSize = true;
            this.CompareTimeToFirstDriver.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.CompareTimeToFirstDriver.Depth = 0;
            this.CompareTimeToFirstDriver.Font = new System.Drawing.Font("Roboto", 10F);
            this.CompareTimeToFirstDriver.Location = new System.Drawing.Point(10, 170);
            this.CompareTimeToFirstDriver.Margin = new System.Windows.Forms.Padding(0);
            this.CompareTimeToFirstDriver.MouseLocation = new System.Drawing.Point(-1, -1);
            this.CompareTimeToFirstDriver.MouseState = MaterialSkin.MouseState.HOVER;
            this.CompareTimeToFirstDriver.Name = "CompareTimeToFirstDriver";
            this.CompareTimeToFirstDriver.Ripple = true;
            this.CompareTimeToFirstDriver.Size = new System.Drawing.Size(385, 30);
            this.CompareTimeToFirstDriver.TabIndex = 61;
            this.CompareTimeToFirstDriver.Text = "Compare time to first driver rather than to your fastest lap";
            this.CompareTimeToFirstDriver.UseVisualStyleBackColor = true;
            this.CompareTimeToFirstDriver.Click += new System.EventHandler(this.CompareTimeToFirstDriver_Click);
            // 
            // PreventAI
            // 
            this.PreventAI.AutoSize = true;
            this.PreventAI.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.PreventAI.Depth = 0;
            this.PreventAI.Font = new System.Drawing.Font("Roboto", 10F);
            this.PreventAI.Location = new System.Drawing.Point(10, 140);
            this.PreventAI.Margin = new System.Windows.Forms.Padding(0);
            this.PreventAI.MouseLocation = new System.Drawing.Point(-1, -1);
            this.PreventAI.MouseState = MaterialSkin.MouseState.HOVER;
            this.PreventAI.Name = "PreventAI";
            this.PreventAI.Ripple = true;
            this.PreventAI.Size = new System.Drawing.Size(399, 30);
            this.PreventAI.TabIndex = 60;
            this.PreventAI.Text = "Prevent AI from taking control over your car after race finish";
            this.PreventAI.UseVisualStyleBackColor = true;
            this.PreventAI.Click += new System.EventHandler(this.PreventAI_Click);
            // 
            // ReconnaissanceLap
            // 
            this.ReconnaissanceLap.AutoSize = true;
            this.ReconnaissanceLap.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ReconnaissanceLap.Depth = 0;
            this.ReconnaissanceLap.Font = new System.Drawing.Font("Roboto", 10F);
            this.ReconnaissanceLap.Location = new System.Drawing.Point(10, 80);
            this.ReconnaissanceLap.Margin = new System.Windows.Forms.Padding(0);
            this.ReconnaissanceLap.MouseLocation = new System.Drawing.Point(-1, -1);
            this.ReconnaissanceLap.MouseState = MaterialSkin.MouseState.HOVER;
            this.ReconnaissanceLap.Name = "ReconnaissanceLap";
            this.ReconnaissanceLap.Ripple = true;
            this.ReconnaissanceLap.Size = new System.Drawing.Size(157, 30);
            this.ReconnaissanceLap.TabIndex = 58;
            this.ReconnaissanceLap.Text = "Reconnaissance Lap";
            this.ReconnaissanceLap.UseVisualStyleBackColor = true;
            this.ReconnaissanceLap.Click += new System.EventHandler(this.ReconnaissanceLap_Click);
            // 
            // FormationLap
            // 
            this.FormationLap.AutoSize = true;
            this.FormationLap.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormationLap.Depth = 0;
            this.FormationLap.Font = new System.Drawing.Font("Roboto", 10F);
            this.FormationLap.Location = new System.Drawing.Point(10, 110);
            this.FormationLap.Margin = new System.Windows.Forms.Padding(0);
            this.FormationLap.MouseLocation = new System.Drawing.Point(-1, -1);
            this.FormationLap.MouseState = MaterialSkin.MouseState.HOVER;
            this.FormationLap.Name = "FormationLap";
            this.FormationLap.Ripple = true;
            this.FormationLap.Size = new System.Drawing.Size(119, 30);
            this.FormationLap.TabIndex = 59;
            this.FormationLap.Text = "Formation Lap";
            this.FormationLap.UseVisualStyleBackColor = true;
            this.FormationLap.Click += new System.EventHandler(this.FormationLap_Click);
            // 
            // ProfileChatProfiles
            // 
            this.ProfileChatProfiles.BackColor = System.Drawing.SystemColors.Window;
            this.ProfileChatProfiles.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.ProfileChatProfiles.ItemHeight = 25;
            this.ProfileChatProfiles.Location = new System.Drawing.Point(10, 32);
            this.ProfileChatProfiles.Name = "ProfileChatProfiles";
            this.ProfileChatProfiles.Size = new System.Drawing.Size(196, 33);
            this.ProfileChatProfiles.TabIndex = 57;
            this.ProfileChatProfiles.SelectionChangeCommitted += new System.EventHandler(this.ProfileChatProfiles_SelectionChangeCommitted);
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(6, 3);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(98, 19);
            this.materialLabel3.TabIndex = 56;
            this.materialLabel3.Text = "Select profile";
            // 
            // quick_chat
            // 
            this.quick_chat.BackColor = System.Drawing.Color.White;
            this.quick_chat.Controls.Add(this.QuickChatProfiles);
            this.quick_chat.Controls.Add(this.materialRaisedButton2);
            this.quick_chat.Controls.Add(this.saveBut);
            this.quick_chat.Controls.Add(this.howToBut);
            this.quick_chat.Controls.Add(this.tenLab);
            this.quick_chat.Controls.Add(this.nineLab);
            this.quick_chat.Controls.Add(this.eightLab);
            this.quick_chat.Controls.Add(this.sevenLab);
            this.quick_chat.Controls.Add(this.sixLab);
            this.quick_chat.Controls.Add(this.fiveLab);
            this.quick_chat.Controls.Add(this.fourLab);
            this.quick_chat.Controls.Add(this.threeLab);
            this.quick_chat.Controls.Add(this.twoLab);
            this.quick_chat.Controls.Add(this.oneLab);
            this.quick_chat.Controls.Add(this.ten);
            this.quick_chat.Controls.Add(this.nine);
            this.quick_chat.Controls.Add(this.eight);
            this.quick_chat.Controls.Add(this.seven);
            this.quick_chat.Controls.Add(this.six);
            this.quick_chat.Controls.Add(this.five);
            this.quick_chat.Controls.Add(this.four);
            this.quick_chat.Controls.Add(this.three);
            this.quick_chat.Controls.Add(this.two);
            this.quick_chat.Controls.Add(this.one);
            this.quick_chat.Controls.Add(this.saveLab);
            this.quick_chat.Location = new System.Drawing.Point(4, 22);
            this.quick_chat.Name = "quick_chat";
            this.quick_chat.Size = new System.Drawing.Size(690, 317);
            this.quick_chat.TabIndex = 2;
            this.quick_chat.Text = "quick_chat";
            // 
            // QuickChatProfiles
            // 
            this.QuickChatProfiles.BackColor = System.Drawing.SystemColors.Window;
            this.QuickChatProfiles.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.QuickChatProfiles.ItemHeight = 25;
            this.QuickChatProfiles.Location = new System.Drawing.Point(23, 36);
            this.QuickChatProfiles.Name = "QuickChatProfiles";
            this.QuickChatProfiles.Size = new System.Drawing.Size(196, 33);
            this.QuickChatProfiles.TabIndex = 55;
            this.QuickChatProfiles.SelectionChangeCommitted += new System.EventHandler(this.QuickChatProfiles_SelectionChangeCommitted);
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Location = new System.Drawing.Point(121, 224);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(98, 34);
            this.materialRaisedButton2.TabIndex = 54;
            this.materialRaisedButton2.Text = "Open with text editor";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // saveBut
            // 
            this.saveBut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.saveBut.Depth = 0;
            this.saveBut.Location = new System.Drawing.Point(23, 264);
            this.saveBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.saveBut.Name = "saveBut";
            this.saveBut.Primary = true;
            this.saveBut.Size = new System.Drawing.Size(196, 34);
            this.saveBut.TabIndex = 53;
            this.saveBut.Text = "Save";
            this.saveBut.UseVisualStyleBackColor = true;
            this.saveBut.Click += new System.EventHandler(this.saveBut_Click);
            // 
            // howToBut
            // 
            this.howToBut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.howToBut.Depth = 0;
            this.howToBut.Location = new System.Drawing.Point(23, 224);
            this.howToBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.howToBut.Name = "howToBut";
            this.howToBut.Primary = true;
            this.howToBut.Size = new System.Drawing.Size(98, 34);
            this.howToBut.TabIndex = 52;
            this.howToBut.Text = "How to use?";
            this.howToBut.UseVisualStyleBackColor = true;
            this.howToBut.Click += new System.EventHandler(this.howToBut_Click);
            // 
            // tenLab
            // 
            this.tenLab.AutoSize = true;
            this.tenLab.BackColor = System.Drawing.Color.Transparent;
            this.tenLab.Depth = 0;
            this.tenLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.tenLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.tenLab.Location = new System.Drawing.Point(239, 268);
            this.tenLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.tenLab.Name = "tenLab";
            this.tenLab.Size = new System.Drawing.Size(25, 19);
            this.tenLab.TabIndex = 51;
            this.tenLab.Text = "10";
            // 
            // nineLab
            // 
            this.nineLab.AutoSize = true;
            this.nineLab.BackColor = System.Drawing.Color.Transparent;
            this.nineLab.Depth = 0;
            this.nineLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.nineLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.nineLab.Location = new System.Drawing.Point(247, 239);
            this.nineLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.nineLab.Name = "nineLab";
            this.nineLab.Size = new System.Drawing.Size(17, 19);
            this.nineLab.TabIndex = 50;
            this.nineLab.Text = "9";
            // 
            // eightLab
            // 
            this.eightLab.AutoSize = true;
            this.eightLab.BackColor = System.Drawing.Color.Transparent;
            this.eightLab.Depth = 0;
            this.eightLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.eightLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.eightLab.Location = new System.Drawing.Point(247, 210);
            this.eightLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.eightLab.Name = "eightLab";
            this.eightLab.Size = new System.Drawing.Size(17, 19);
            this.eightLab.TabIndex = 49;
            this.eightLab.Text = "8";
            // 
            // sevenLab
            // 
            this.sevenLab.AutoSize = true;
            this.sevenLab.BackColor = System.Drawing.Color.Transparent;
            this.sevenLab.Depth = 0;
            this.sevenLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.sevenLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sevenLab.Location = new System.Drawing.Point(247, 181);
            this.sevenLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.sevenLab.Name = "sevenLab";
            this.sevenLab.Size = new System.Drawing.Size(17, 19);
            this.sevenLab.TabIndex = 48;
            this.sevenLab.Text = "7";
            // 
            // sixLab
            // 
            this.sixLab.AutoSize = true;
            this.sixLab.BackColor = System.Drawing.Color.Transparent;
            this.sixLab.Depth = 0;
            this.sixLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.sixLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.sixLab.Location = new System.Drawing.Point(247, 152);
            this.sixLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.sixLab.Name = "sixLab";
            this.sixLab.Size = new System.Drawing.Size(17, 19);
            this.sixLab.TabIndex = 47;
            this.sixLab.Text = "6";
            // 
            // fiveLab
            // 
            this.fiveLab.AutoSize = true;
            this.fiveLab.BackColor = System.Drawing.Color.Transparent;
            this.fiveLab.Depth = 0;
            this.fiveLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.fiveLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fiveLab.Location = new System.Drawing.Point(247, 123);
            this.fiveLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.fiveLab.Name = "fiveLab";
            this.fiveLab.Size = new System.Drawing.Size(17, 19);
            this.fiveLab.TabIndex = 46;
            this.fiveLab.Text = "5";
            // 
            // fourLab
            // 
            this.fourLab.AutoSize = true;
            this.fourLab.BackColor = System.Drawing.Color.Transparent;
            this.fourLab.Depth = 0;
            this.fourLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.fourLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fourLab.Location = new System.Drawing.Point(247, 94);
            this.fourLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.fourLab.Name = "fourLab";
            this.fourLab.Size = new System.Drawing.Size(17, 19);
            this.fourLab.TabIndex = 45;
            this.fourLab.Text = "4";
            // 
            // threeLab
            // 
            this.threeLab.AutoSize = true;
            this.threeLab.BackColor = System.Drawing.Color.Transparent;
            this.threeLab.Depth = 0;
            this.threeLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.threeLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.threeLab.Location = new System.Drawing.Point(247, 65);
            this.threeLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.threeLab.Name = "threeLab";
            this.threeLab.Size = new System.Drawing.Size(17, 19);
            this.threeLab.TabIndex = 44;
            this.threeLab.Text = "3";
            // 
            // twoLab
            // 
            this.twoLab.AutoSize = true;
            this.twoLab.BackColor = System.Drawing.Color.Transparent;
            this.twoLab.Depth = 0;
            this.twoLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.twoLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.twoLab.Location = new System.Drawing.Point(247, 36);
            this.twoLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.twoLab.Name = "twoLab";
            this.twoLab.Size = new System.Drawing.Size(17, 19);
            this.twoLab.TabIndex = 43;
            this.twoLab.Text = "2";
            // 
            // oneLab
            // 
            this.oneLab.AutoSize = true;
            this.oneLab.BackColor = System.Drawing.Color.Transparent;
            this.oneLab.Depth = 0;
            this.oneLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.oneLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.oneLab.Location = new System.Drawing.Point(247, 7);
            this.oneLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.oneLab.Name = "oneLab";
            this.oneLab.Size = new System.Drawing.Size(17, 19);
            this.oneLab.TabIndex = 42;
            this.oneLab.Text = "1";
            // 
            // ten
            // 
            this.ten.BackColor = System.Drawing.Color.White;
            this.ten.Depth = 0;
            this.ten.Hint = "";
            this.ten.Location = new System.Drawing.Point(270, 264);
            this.ten.MouseState = MaterialSkin.MouseState.HOVER;
            this.ten.Name = "ten";
            this.ten.PasswordChar = '\0';
            this.ten.SelectedText = "";
            this.ten.SelectionLength = 0;
            this.ten.SelectionStart = 0;
            this.ten.Size = new System.Drawing.Size(417, 23);
            this.ten.TabIndex = 41;
            this.ten.UseSystemPasswordChar = false;
            // 
            // nine
            // 
            this.nine.BackColor = System.Drawing.Color.White;
            this.nine.Depth = 0;
            this.nine.Hint = "";
            this.nine.Location = new System.Drawing.Point(270, 235);
            this.nine.MouseState = MaterialSkin.MouseState.HOVER;
            this.nine.Name = "nine";
            this.nine.PasswordChar = '\0';
            this.nine.SelectedText = "";
            this.nine.SelectionLength = 0;
            this.nine.SelectionStart = 0;
            this.nine.Size = new System.Drawing.Size(417, 23);
            this.nine.TabIndex = 40;
            this.nine.UseSystemPasswordChar = false;
            // 
            // eight
            // 
            this.eight.BackColor = System.Drawing.Color.White;
            this.eight.Depth = 0;
            this.eight.Hint = "";
            this.eight.Location = new System.Drawing.Point(270, 206);
            this.eight.MouseState = MaterialSkin.MouseState.HOVER;
            this.eight.Name = "eight";
            this.eight.PasswordChar = '\0';
            this.eight.SelectedText = "";
            this.eight.SelectionLength = 0;
            this.eight.SelectionStart = 0;
            this.eight.Size = new System.Drawing.Size(417, 23);
            this.eight.TabIndex = 39;
            this.eight.UseSystemPasswordChar = false;
            // 
            // seven
            // 
            this.seven.BackColor = System.Drawing.Color.White;
            this.seven.Depth = 0;
            this.seven.Hint = "";
            this.seven.Location = new System.Drawing.Point(270, 177);
            this.seven.MouseState = MaterialSkin.MouseState.HOVER;
            this.seven.Name = "seven";
            this.seven.PasswordChar = '\0';
            this.seven.SelectedText = "";
            this.seven.SelectionLength = 0;
            this.seven.SelectionStart = 0;
            this.seven.Size = new System.Drawing.Size(417, 23);
            this.seven.TabIndex = 38;
            this.seven.UseSystemPasswordChar = false;
            // 
            // six
            // 
            this.six.BackColor = System.Drawing.Color.White;
            this.six.Depth = 0;
            this.six.Hint = "";
            this.six.Location = new System.Drawing.Point(270, 148);
            this.six.MouseState = MaterialSkin.MouseState.HOVER;
            this.six.Name = "six";
            this.six.PasswordChar = '\0';
            this.six.SelectedText = "";
            this.six.SelectionLength = 0;
            this.six.SelectionStart = 0;
            this.six.Size = new System.Drawing.Size(417, 23);
            this.six.TabIndex = 37;
            this.six.UseSystemPasswordChar = false;
            // 
            // five
            // 
            this.five.BackColor = System.Drawing.Color.White;
            this.five.Depth = 0;
            this.five.Hint = "";
            this.five.Location = new System.Drawing.Point(270, 119);
            this.five.MouseState = MaterialSkin.MouseState.HOVER;
            this.five.Name = "five";
            this.five.PasswordChar = '\0';
            this.five.SelectedText = "";
            this.five.SelectionLength = 0;
            this.five.SelectionStart = 0;
            this.five.Size = new System.Drawing.Size(417, 23);
            this.five.TabIndex = 36;
            this.five.UseSystemPasswordChar = false;
            // 
            // four
            // 
            this.four.BackColor = System.Drawing.Color.White;
            this.four.Depth = 0;
            this.four.Hint = "";
            this.four.Location = new System.Drawing.Point(270, 90);
            this.four.MouseState = MaterialSkin.MouseState.HOVER;
            this.four.Name = "four";
            this.four.PasswordChar = '\0';
            this.four.SelectedText = "";
            this.four.SelectionLength = 0;
            this.four.SelectionStart = 0;
            this.four.Size = new System.Drawing.Size(417, 23);
            this.four.TabIndex = 35;
            this.four.UseSystemPasswordChar = false;
            // 
            // three
            // 
            this.three.BackColor = System.Drawing.Color.White;
            this.three.Depth = 0;
            this.three.Hint = "";
            this.three.Location = new System.Drawing.Point(270, 61);
            this.three.MouseState = MaterialSkin.MouseState.HOVER;
            this.three.Name = "three";
            this.three.PasswordChar = '\0';
            this.three.SelectedText = "";
            this.three.SelectionLength = 0;
            this.three.SelectionStart = 0;
            this.three.Size = new System.Drawing.Size(417, 23);
            this.three.TabIndex = 34;
            this.three.UseSystemPasswordChar = false;
            // 
            // two
            // 
            this.two.BackColor = System.Drawing.Color.White;
            this.two.Depth = 0;
            this.two.Hint = "";
            this.two.Location = new System.Drawing.Point(270, 32);
            this.two.MouseState = MaterialSkin.MouseState.HOVER;
            this.two.Name = "two";
            this.two.PasswordChar = '\0';
            this.two.SelectedText = "";
            this.two.SelectionLength = 0;
            this.two.SelectionStart = 0;
            this.two.Size = new System.Drawing.Size(417, 23);
            this.two.TabIndex = 33;
            this.two.UseSystemPasswordChar = false;
            // 
            // one
            // 
            this.one.BackColor = System.Drawing.Color.White;
            this.one.Depth = 0;
            this.one.Hint = "";
            this.one.Location = new System.Drawing.Point(270, 3);
            this.one.MouseState = MaterialSkin.MouseState.HOVER;
            this.one.Name = "one";
            this.one.PasswordChar = '\0';
            this.one.SelectedText = "";
            this.one.SelectionLength = 0;
            this.one.SelectionStart = 0;
            this.one.Size = new System.Drawing.Size(417, 23);
            this.one.TabIndex = 32;
            this.one.UseSystemPasswordChar = false;
            // 
            // saveLab
            // 
            this.saveLab.AutoSize = true;
            this.saveLab.BackColor = System.Drawing.Color.Transparent;
            this.saveLab.Depth = 0;
            this.saveLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.saveLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.saveLab.Location = new System.Drawing.Point(19, 7);
            this.saveLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.saveLab.Name = "saveLab";
            this.saveLab.Size = new System.Drawing.Size(98, 19);
            this.saveLab.TabIndex = 28;
            this.saveLab.Text = "Select profile";
            // 
            // vmtTab
            // 
            this.vmtTab.BackColor = System.Drawing.Color.White;
            this.vmtTab.Controls.Add(this.vmtPan);
            this.vmtTab.Location = new System.Drawing.Point(4, 22);
            this.vmtTab.Name = "vmtTab";
            this.vmtTab.Size = new System.Drawing.Size(704, 349);
            this.vmtTab.TabIndex = 2;
            this.vmtTab.Text = "VMT";
            // 
            // vmtPan
            // 
            this.vmtPan.Controls.Add(this.vmtP);
            this.vmtPan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vmtPan.Location = new System.Drawing.Point(0, 0);
            this.vmtPan.Name = "vmtPan";
            this.vmtPan.Size = new System.Drawing.Size(704, 349);
            this.vmtPan.TabIndex = 45;
            // 
            // vmtP
            // 
            this.vmtP.Controls.Add(this.classic);
            this.vmtP.Controls.Add(this._15);
            this.vmtP.Controls.Add(this._16);
            this.vmtP.Controls.Add(this._17);
            this.vmtP.Depth = 0;
            this.vmtP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vmtP.Location = new System.Drawing.Point(0, 0);
            this.vmtP.MouseState = MaterialSkin.MouseState.HOVER;
            this.vmtP.Name = "vmtP";
            this.vmtP.SelectedIndex = 0;
            this.vmtP.Size = new System.Drawing.Size(704, 349);
            this.vmtP.TabIndex = 0;
            // 
            // classic
            // 
            this.classic.BackColor = System.Drawing.Color.White;
            this.classic.Controls.Add(this.supportedWarning);
            this.classic.Location = new System.Drawing.Point(4, 22);
            this.classic.Name = "classic";
            this.classic.Size = new System.Drawing.Size(696, 323);
            this.classic.TabIndex = 4;
            this.classic.Text = "classic";
            // 
            // supportedWarning
            // 
            this.supportedWarning.AutoSize = true;
            this.supportedWarning.BackColor = System.Drawing.Color.Transparent;
            this.supportedWarning.Depth = 0;
            this.supportedWarning.Font = new System.Drawing.Font("Roboto", 11F);
            this.supportedWarning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.supportedWarning.Location = new System.Drawing.Point(160, 137);
            this.supportedWarning.MouseState = MaterialSkin.MouseState.HOVER;
            this.supportedWarning.Name = "supportedWarning";
            this.supportedWarning.Size = new System.Drawing.Size(372, 19);
            this.supportedWarning.TabIndex = 44;
            this.supportedWarning.Text = "VMT tab is only supported by F1 2015 VMT and newer.";
            this.supportedWarning.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _15
            // 
            this._15.BackColor = System.Drawing.Color.White;
            this._15.Controls.Add(this.rossiManor15);
            this._15.Controls.Add(this.magnussenMcl15);
            this._15.Controls.Add(this.rbrCamo15);
            this._15.Controls.Add(this.bianchiManor15);
            this._15.Location = new System.Drawing.Point(4, 22);
            this._15.Name = "_15";
            this._15.Padding = new System.Windows.Forms.Padding(3);
            this._15.Size = new System.Drawing.Size(696, 323);
            this._15.TabIndex = 1;
            this._15.Text = "2015";
            // 
            // rossiManor15
            // 
            this.rossiManor15.AutoSize = true;
            this.rossiManor15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.rossiManor15.Depth = 0;
            this.rossiManor15.Font = new System.Drawing.Font("Roboto", 10F);
            this.rossiManor15.Location = new System.Drawing.Point(17, 79);
            this.rossiManor15.Margin = new System.Windows.Forms.Padding(0);
            this.rossiManor15.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rossiManor15.MouseState = MaterialSkin.MouseState.HOVER;
            this.rossiManor15.Name = "rossiManor15";
            this.rossiManor15.Ripple = true;
            this.rossiManor15.Size = new System.Drawing.Size(344, 30);
            this.rossiManor15.TabIndex = 51;
            this.rossiManor15.Text = "Swap Roberto Merhi with Alexander Rossi in Manor";
            this.rossiManor15.UseVisualStyleBackColor = true;
            this.rossiManor15.Click += new System.EventHandler(this.rossiManor15_Click);
            // 
            // magnussenMcl15
            // 
            this.magnussenMcl15.AutoSize = true;
            this.magnussenMcl15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.magnussenMcl15.Depth = 0;
            this.magnussenMcl15.Font = new System.Drawing.Font("Roboto", 10F);
            this.magnussenMcl15.Location = new System.Drawing.Point(17, 109);
            this.magnussenMcl15.Margin = new System.Windows.Forms.Padding(0);
            this.magnussenMcl15.MouseLocation = new System.Drawing.Point(-1, -1);
            this.magnussenMcl15.MouseState = MaterialSkin.MouseState.HOVER;
            this.magnussenMcl15.Name = "magnussenMcl15";
            this.magnussenMcl15.Ripple = true;
            this.magnussenMcl15.Size = new System.Drawing.Size(385, 30);
            this.magnussenMcl15.TabIndex = 52;
            this.magnussenMcl15.Text = "Swap Fernando Alonso with Kevin Magnussen in McLaren";
            this.magnussenMcl15.UseVisualStyleBackColor = true;
            this.magnussenMcl15.Click += new System.EventHandler(this.magnussenMcl15_Click);
            // 
            // rbrCamo15
            // 
            this.rbrCamo15.AutoSize = true;
            this.rbrCamo15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.rbrCamo15.Depth = 0;
            this.rbrCamo15.Font = new System.Drawing.Font("Roboto", 10F);
            this.rbrCamo15.Location = new System.Drawing.Point(17, 19);
            this.rbrCamo15.Margin = new System.Windows.Forms.Padding(0);
            this.rbrCamo15.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rbrCamo15.MouseState = MaterialSkin.MouseState.HOVER;
            this.rbrCamo15.Name = "rbrCamo15";
            this.rbrCamo15.Ripple = true;
            this.rbrCamo15.Size = new System.Drawing.Size(154, 30);
            this.rbrCamo15.TabIndex = 49;
            this.rbrCamo15.Text = "Red Bull camo livery";
            this.rbrCamo15.UseVisualStyleBackColor = true;
            this.rbrCamo15.Click += new System.EventHandler(this.rbrCamo15_Click);
            // 
            // bianchiManor15
            // 
            this.bianchiManor15.AutoSize = true;
            this.bianchiManor15.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.bianchiManor15.Depth = 0;
            this.bianchiManor15.Font = new System.Drawing.Font("Roboto", 10F);
            this.bianchiManor15.Location = new System.Drawing.Point(17, 49);
            this.bianchiManor15.Margin = new System.Windows.Forms.Padding(0);
            this.bianchiManor15.MouseLocation = new System.Drawing.Point(-1, -1);
            this.bianchiManor15.MouseState = MaterialSkin.MouseState.HOVER;
            this.bianchiManor15.Name = "bianchiManor15";
            this.bianchiManor15.Ripple = true;
            this.bianchiManor15.Size = new System.Drawing.Size(327, 30);
            this.bianchiManor15.TabIndex = 50;
            this.bianchiManor15.Text = "Swap Roberto Merhi with Jules Bianchi in Manor";
            this.bianchiManor15.UseVisualStyleBackColor = true;
            this.bianchiManor15.Click += new System.EventHandler(this.bianchiManor15_Click);
            // 
            // _16
            // 
            this._16.BackColor = System.Drawing.Color.White;
            this._16.Controls.Add(this.swapKvyVer);
            this._16.Controls.Add(this.replaceAloVan);
            this._16.Location = new System.Drawing.Point(4, 22);
            this._16.Name = "_16";
            this._16.Size = new System.Drawing.Size(696, 323);
            this._16.TabIndex = 2;
            this._16.Text = "2016";
            // 
            // swapKvyVer
            // 
            this.swapKvyVer.AutoSize = true;
            this.swapKvyVer.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.swapKvyVer.Depth = 0;
            this.swapKvyVer.Font = new System.Drawing.Font("Roboto", 10F);
            this.swapKvyVer.Location = new System.Drawing.Point(18, 20);
            this.swapKvyVer.Margin = new System.Windows.Forms.Padding(0);
            this.swapKvyVer.MouseLocation = new System.Drawing.Point(-1, -1);
            this.swapKvyVer.MouseState = MaterialSkin.MouseState.HOVER;
            this.swapKvyVer.Name = "swapKvyVer";
            this.swapKvyVer.Ripple = true;
            this.swapKvyVer.Size = new System.Drawing.Size(291, 30);
            this.swapKvyVer.TabIndex = 47;
            this.swapKvyVer.Text = "Move Kvyat to STR and Verstappen to RBR";
            this.swapKvyVer.UseVisualStyleBackColor = true;
            this.swapKvyVer.Click += new System.EventHandler(this.swapKvyVer_Click);
            // 
            // replaceAloVan
            // 
            this.replaceAloVan.AutoSize = true;
            this.replaceAloVan.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.replaceAloVan.Depth = 0;
            this.replaceAloVan.Font = new System.Drawing.Font("Roboto", 10F);
            this.replaceAloVan.Location = new System.Drawing.Point(18, 50);
            this.replaceAloVan.Margin = new System.Windows.Forms.Padding(0);
            this.replaceAloVan.MouseLocation = new System.Drawing.Point(-1, -1);
            this.replaceAloVan.MouseState = MaterialSkin.MouseState.HOVER;
            this.replaceAloVan.Name = "replaceAloVan";
            this.replaceAloVan.Ripple = true;
            this.replaceAloVan.Size = new System.Drawing.Size(297, 30);
            this.replaceAloVan.TabIndex = 48;
            this.replaceAloVan.Text = "Replace Alonso with Vandoorne in McLaren";
            this.replaceAloVan.UseVisualStyleBackColor = true;
            this.replaceAloVan.Click += new System.EventHandler(this.replaceAloVan_Click);
            // 
            // _17
            // 
            this._17.BackColor = System.Drawing.Color.White;
            this._17.Controls.Add(this.replaceAloBut);
            this._17.Location = new System.Drawing.Point(4, 22);
            this._17.Name = "_17";
            this._17.Size = new System.Drawing.Size(696, 323);
            this._17.TabIndex = 3;
            this._17.Text = "2017";
            // 
            // replaceAloBut
            // 
            this.replaceAloBut.AutoSize = true;
            this.replaceAloBut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.replaceAloBut.Depth = 0;
            this.replaceAloBut.Font = new System.Drawing.Font("Roboto", 10F);
            this.replaceAloBut.Location = new System.Drawing.Point(17, 18);
            this.replaceAloBut.Margin = new System.Windows.Forms.Padding(0);
            this.replaceAloBut.MouseLocation = new System.Drawing.Point(-1, -1);
            this.replaceAloBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.replaceAloBut.Name = "replaceAloBut";
            this.replaceAloBut.Ripple = true;
            this.replaceAloBut.Size = new System.Drawing.Size(271, 30);
            this.replaceAloBut.TabIndex = 49;
            this.replaceAloBut.Text = "Replace Alonso with Button in McLaren";
            this.replaceAloBut.UseVisualStyleBackColor = true;
            // 
            // settingsTab
            // 
            this.settingsTab.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.settingsTab.Controls.Add(this.settings);
            this.settingsTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.settingsTab.Location = new System.Drawing.Point(4, 22);
            this.settingsTab.Name = "settingsTab";
            this.settingsTab.Size = new System.Drawing.Size(704, 349);
            this.settingsTab.TabIndex = 3;
            this.settingsTab.Text = "Settings";
            // 
            // settings
            // 
            this.settings.Controls.Add(this.main);
            this.settings.Controls.Add(this.games);
            this.settings.Controls.Add(this.updates);
            this.settings.Controls.Add(this.informations);
            this.settings.Controls.Add(this.devset);
            this.settings.Controls.Add(this.preferences);
            this.settings.Depth = 0;
            this.settings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settings.Location = new System.Drawing.Point(0, 0);
            this.settings.MouseState = MaterialSkin.MouseState.HOVER;
            this.settings.Name = "settings";
            this.settings.SelectedIndex = 0;
            this.settings.Size = new System.Drawing.Size(704, 349);
            this.settings.TabIndex = 35;
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            this.main.Controls.Add(this.preferencesSettings);
            this.main.Controls.Add(this.settingsDev);
            this.main.Controls.Add(this.settingAbout);
            this.main.Controls.Add(this.settingUpdate);
            this.main.Controls.Add(this.settingGames);
            this.main.Location = new System.Drawing.Point(4, 22);
            this.main.Name = "main";
            this.main.Size = new System.Drawing.Size(696, 323);
            this.main.TabIndex = 4;
            this.main.Text = "main";
            // 
            // preferencesSettings
            // 
            this.preferencesSettings.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.preferencesSettings.FlatAppearance.BorderSize = 0;
            this.preferencesSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.preferencesSettings.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.preferencesSettings.ForeColor = System.Drawing.Color.Black;
            this.preferencesSettings.Image = global::SimPT.Properties.Resources.personalisation;
            this.preferencesSettings.Location = new System.Drawing.Point(147, 4);
            this.preferencesSettings.Name = "preferencesSettings";
            this.preferencesSettings.Size = new System.Drawing.Size(124, 124);
            this.preferencesSettings.TabIndex = 67;
            this.preferencesSettings.Text = "Preferences";
            this.preferencesSettings.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.preferencesSettings.UseVisualStyleBackColor = true;
            this.preferencesSettings.Click += new System.EventHandler(this.preferencesSettings_Click);
            // 
            // settingsDev
            // 
            this.settingsDev.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.settingsDev.FlatAppearance.BorderSize = 0;
            this.settingsDev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingsDev.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.settingsDev.ForeColor = System.Drawing.Color.Black;
            this.settingsDev.Image = global::SimPT.Properties.Resources.dev;
            this.settingsDev.Location = new System.Drawing.Point(537, 4);
            this.settingsDev.Name = "settingsDev";
            this.settingsDev.Size = new System.Drawing.Size(124, 124);
            this.settingsDev.TabIndex = 66;
            this.settingsDev.Text = "Dev Settings";
            this.settingsDev.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.settingsDev.UseVisualStyleBackColor = true;
            this.settingsDev.Visible = false;
            this.settingsDev.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // settingAbout
            // 
            this.settingAbout.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.settingAbout.FlatAppearance.BorderSize = 0;
            this.settingAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingAbout.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.settingAbout.ForeColor = System.Drawing.Color.Black;
            this.settingAbout.Image = global::SimPT.Properties.Resources.credits;
            this.settingAbout.Location = new System.Drawing.Point(407, 4);
            this.settingAbout.Name = "settingAbout";
            this.settingAbout.Size = new System.Drawing.Size(124, 124);
            this.settingAbout.TabIndex = 65;
            this.settingAbout.Text = "About";
            this.settingAbout.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.settingAbout.UseVisualStyleBackColor = true;
            this.settingAbout.Click += new System.EventHandler(this.settingAbout_Click);
            // 
            // settingUpdate
            // 
            this.settingUpdate.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.settingUpdate.FlatAppearance.BorderSize = 0;
            this.settingUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingUpdate.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.settingUpdate.ForeColor = System.Drawing.Color.Black;
            this.settingUpdate.Image = global::SimPT.Properties.Resources.updates;
            this.settingUpdate.Location = new System.Drawing.Point(277, 4);
            this.settingUpdate.Name = "settingUpdate";
            this.settingUpdate.Size = new System.Drawing.Size(124, 124);
            this.settingUpdate.TabIndex = 64;
            this.settingUpdate.Text = "Updates";
            this.settingUpdate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.settingUpdate.UseVisualStyleBackColor = true;
            this.settingUpdate.Click += new System.EventHandler(this.settingUpdate_Click);
            // 
            // settingGames
            // 
            this.settingGames.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.settingGames.FlatAppearance.BorderSize = 0;
            this.settingGames.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingGames.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.settingGames.ForeColor = System.Drawing.Color.Black;
            this.settingGames.Image = global::SimPT.Properties.Resources.games;
            this.settingGames.Location = new System.Drawing.Point(17, 4);
            this.settingGames.Name = "settingGames";
            this.settingGames.Size = new System.Drawing.Size(124, 124);
            this.settingGames.TabIndex = 63;
            this.settingGames.Text = "Game";
            this.settingGames.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.settingGames.UseVisualStyleBackColor = true;
            this.settingGames.Click += new System.EventHandler(this.settingGames_Click);
            // 
            // games
            // 
            this.games.BackColor = System.Drawing.Color.White;
            this.games.Controls.Add(this.turnOffCheck);
            this.games.Controls.Add(this.showLogCheck);
            this.games.Location = new System.Drawing.Point(4, 22);
            this.games.Name = "games";
            this.games.Padding = new System.Windows.Forms.Padding(3);
            this.games.Size = new System.Drawing.Size(696, 323);
            this.games.TabIndex = 1;
            this.games.Text = "Games";
            // 
            // turnOffCheck
            // 
            this.turnOffCheck.AutoSize = true;
            this.turnOffCheck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.turnOffCheck.Depth = 0;
            this.turnOffCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.turnOffCheck.Location = new System.Drawing.Point(3, 3);
            this.turnOffCheck.Margin = new System.Windows.Forms.Padding(0);
            this.turnOffCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.turnOffCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.turnOffCheck.Name = "turnOffCheck";
            this.turnOffCheck.Ripple = true;
            this.turnOffCheck.Size = new System.Drawing.Size(297, 30);
            this.turnOffCheck.TabIndex = 25;
            this.turnOffCheck.Text = "Close VMT Engine when I press Play button";
            this.turnOffCheck.UseVisualStyleBackColor = true;
            this.turnOffCheck.Click += new System.EventHandler(this.turnOffCheck_Click);
            // 
            // showLogCheck
            // 
            this.showLogCheck.AutoSize = true;
            this.showLogCheck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.showLogCheck.Depth = 0;
            this.showLogCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.showLogCheck.Location = new System.Drawing.Point(3, 33);
            this.showLogCheck.Margin = new System.Windows.Forms.Padding(0);
            this.showLogCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.showLogCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.showLogCheck.Name = "showLogCheck";
            this.showLogCheck.Ripple = true;
            this.showLogCheck.Size = new System.Drawing.Size(330, 30);
            this.showLogCheck.TabIndex = 26;
            this.showLogCheck.Text = "Show log after closing the game with bug tracker";
            this.showLogCheck.UseVisualStyleBackColor = true;
            this.showLogCheck.Click += new System.EventHandler(this.materialCheckBox6_Click);
            // 
            // updates
            // 
            this.updates.AutoScroll = true;
            this.updates.BackColor = System.Drawing.Color.White;
            this.updates.Controls.Add(this.materialLabel1);
            this.updates.Controls.Add(this.materialLabel8);
            this.updates.Controls.Add(this.materialLabel7);
            this.updates.Controls.Add(this.prev);
            this.updates.Controls.Add(this.stable);
            this.updates.Controls.Add(this.changelogButton);
            this.updates.Controls.Add(this.autoUpdCheck);
            this.updates.Controls.Add(this.fupdButton);
            this.updates.Location = new System.Drawing.Point(4, 22);
            this.updates.Name = "updates";
            this.updates.Size = new System.Drawing.Size(696, 323);
            this.updates.TabIndex = 2;
            this.updates.Text = "Updates & Privacy";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(154, 123);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(394, 19);
            this.materialLabel1.TabIndex = 36;
            this.materialLabel1.Text = "With version 4.0.2 update functionality has been removed.";
            this.materialLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel8.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(35, 141);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(242, 57);
            this.materialLabel8.TabIndex = 35;
            this.materialLabel8.Text = "You\'ll receive updates much faster,\r\nbut they may be unstable \r\n(not recommendate" +
    "d for most).";
            this.materialLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel8.Visible = false;
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(34, 74);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(246, 38);
            this.materialLabel7.TabIndex = 34;
            this.materialLabel7.Text = "You\'ll receive updates slower\r\nbut they\'re much more safer to use.";
            this.materialLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialLabel7.Visible = false;
            // 
            // prev
            // 
            this.prev.AutoSize = true;
            this.prev.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.prev.Depth = 0;
            this.prev.Font = new System.Drawing.Font("Roboto", 10F);
            this.prev.Location = new System.Drawing.Point(20, 112);
            this.prev.Margin = new System.Windows.Forms.Padding(0);
            this.prev.MouseLocation = new System.Drawing.Point(-1, -1);
            this.prev.MouseState = MaterialSkin.MouseState.HOVER;
            this.prev.Name = "prev";
            this.prev.Ripple = true;
            this.prev.Size = new System.Drawing.Size(131, 30);
            this.prev.TabIndex = 33;
            this.prev.TabStop = true;
            this.prev.Text = "Preview Channel";
            this.prev.UseVisualStyleBackColor = true;
            this.prev.Visible = false;
            this.prev.Click += new System.EventHandler(this.prev_Click);
            // 
            // stable
            // 
            this.stable.AutoSize = true;
            this.stable.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.stable.Depth = 0;
            this.stable.Font = new System.Drawing.Font("Roboto", 10F);
            this.stable.Location = new System.Drawing.Point(20, 44);
            this.stable.Margin = new System.Windows.Forms.Padding(0);
            this.stable.MouseLocation = new System.Drawing.Point(-1, -1);
            this.stable.MouseState = MaterialSkin.MouseState.HOVER;
            this.stable.Name = "stable";
            this.stable.Ripple = true;
            this.stable.Size = new System.Drawing.Size(121, 30);
            this.stable.TabIndex = 32;
            this.stable.TabStop = true;
            this.stable.Text = "Stable Channel";
            this.stable.UseVisualStyleBackColor = true;
            this.stable.Visible = false;
            this.stable.Click += new System.EventHandler(this.stable_Click);
            // 
            // changelogButton
            // 
            this.changelogButton.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.changelogButton.Depth = 0;
            this.changelogButton.Location = new System.Drawing.Point(22, 253);
            this.changelogButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.changelogButton.Name = "changelogButton";
            this.changelogButton.Primary = true;
            this.changelogButton.Size = new System.Drawing.Size(106, 33);
            this.changelogButton.TabIndex = 30;
            this.changelogButton.Text = "Changelog";
            this.changelogButton.UseVisualStyleBackColor = true;
            this.changelogButton.Visible = false;
            this.changelogButton.Click += new System.EventHandler(this.materialRaisedButton20_Click);
            // 
            // autoUpdCheck
            // 
            this.autoUpdCheck.AutoSize = true;
            this.autoUpdCheck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.autoUpdCheck.Depth = 0;
            this.autoUpdCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.autoUpdCheck.Location = new System.Drawing.Point(9, 8);
            this.autoUpdCheck.Margin = new System.Windows.Forms.Padding(0);
            this.autoUpdCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.autoUpdCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.autoUpdCheck.Name = "autoUpdCheck";
            this.autoUpdCheck.Ripple = true;
            this.autoUpdCheck.Size = new System.Drawing.Size(147, 30);
            this.autoUpdCheck.TabIndex = 23;
            this.autoUpdCheck.Text = "Automatic updates";
            this.autoUpdCheck.UseVisualStyleBackColor = true;
            this.autoUpdCheck.Visible = false;
            this.autoUpdCheck.Click += new System.EventHandler(this.materialCheckBox8_Click);
            // 
            // fupdButton
            // 
            this.fupdButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.fupdButton.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.fupdButton.Depth = 0;
            this.fupdButton.Location = new System.Drawing.Point(22, 214);
            this.fupdButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.fupdButton.Name = "fupdButton";
            this.fupdButton.Primary = true;
            this.fupdButton.Size = new System.Drawing.Size(106, 33);
            this.fupdButton.TabIndex = 29;
            this.fupdButton.Text = "Check for updates";
            this.fupdButton.UseVisualStyleBackColor = false;
            this.fupdButton.Visible = false;
            this.fupdButton.Click += new System.EventHandler(this.materialRaisedButton19_Click);
            // 
            // informations
            // 
            this.informations.AutoScroll = true;
            this.informations.BackColor = System.Drawing.Color.White;
            this.informations.Controls.Add(this.GitBtn);
            this.informations.Controls.Add(this.vmtYTbut);
            this.informations.Controls.Add(this.vmtFBBut);
            this.informations.Controls.Add(this.materialLabel6);
            this.informations.Controls.Add(this.websiteNewBut);
            this.informations.Controls.Add(this.copyrightLab);
            this.informations.Controls.Add(this.infoLab);
            this.informations.Location = new System.Drawing.Point(4, 22);
            this.informations.Name = "informations";
            this.informations.Size = new System.Drawing.Size(696, 323);
            this.informations.TabIndex = 3;
            this.informations.Text = "Informations";
            // 
            // GitBtn
            // 
            this.GitBtn.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.GitBtn.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.GitBtn.Depth = 0;
            this.GitBtn.Location = new System.Drawing.Point(486, 151);
            this.GitBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.GitBtn.Name = "GitBtn";
            this.GitBtn.Primary = true;
            this.GitBtn.Size = new System.Drawing.Size(128, 33);
            this.GitBtn.TabIndex = 51;
            this.GitBtn.Text = "Project repository";
            this.GitBtn.UseVisualStyleBackColor = false;
            this.GitBtn.Click += new System.EventHandler(this.GitBtn_Click);
            // 
            // vmtYTbut
            // 
            this.vmtYTbut.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.vmtYTbut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.vmtYTbut.Depth = 0;
            this.vmtYTbut.Location = new System.Drawing.Point(486, 73);
            this.vmtYTbut.MouseState = MaterialSkin.MouseState.HOVER;
            this.vmtYTbut.Name = "vmtYTbut";
            this.vmtYTbut.Primary = true;
            this.vmtYTbut.Size = new System.Drawing.Size(128, 33);
            this.vmtYTbut.TabIndex = 50;
            this.vmtYTbut.Text = "VMT YouTube";
            this.vmtYTbut.UseVisualStyleBackColor = false;
            this.vmtYTbut.Click += new System.EventHandler(this.vmtYTbut_Click);
            // 
            // vmtFBBut
            // 
            this.vmtFBBut.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.vmtFBBut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.vmtFBBut.Depth = 0;
            this.vmtFBBut.Location = new System.Drawing.Point(486, 34);
            this.vmtFBBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.vmtFBBut.Name = "vmtFBBut";
            this.vmtFBBut.Primary = true;
            this.vmtFBBut.Size = new System.Drawing.Size(128, 33);
            this.vmtFBBut.TabIndex = 49;
            this.vmtFBBut.Text = "VMT Facebook";
            this.vmtFBBut.UseVisualStyleBackColor = false;
            this.vmtFBBut.Click += new System.EventHandler(this.vmtFBBut_Click);
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(521, 6);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(55, 19);
            this.materialLabel6.TabIndex = 48;
            this.materialLabel6.Text = "Social:";
            this.materialLabel6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // websiteNewBut
            // 
            this.websiteNewBut.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.websiteNewBut.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.websiteNewBut.Depth = 0;
            this.websiteNewBut.Location = new System.Drawing.Point(486, 112);
            this.websiteNewBut.MouseState = MaterialSkin.MouseState.HOVER;
            this.websiteNewBut.Name = "websiteNewBut";
            this.websiteNewBut.Primary = true;
            this.websiteNewBut.Size = new System.Drawing.Size(128, 33);
            this.websiteNewBut.TabIndex = 47;
            this.websiteNewBut.Text = "Dev\'s website";
            this.websiteNewBut.UseVisualStyleBackColor = false;
            this.websiteNewBut.Click += new System.EventHandler(this.websiteNewBut_Click);
            // 
            // copyrightLab
            // 
            this.copyrightLab.AutoSize = true;
            this.copyrightLab.BackColor = System.Drawing.Color.Transparent;
            this.copyrightLab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.copyrightLab.Depth = 0;
            this.copyrightLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.copyrightLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.copyrightLab.Location = new System.Drawing.Point(468, 196);
            this.copyrightLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.copyrightLab.Name = "copyrightLab";
            this.copyrightLab.Size = new System.Drawing.Size(167, 76);
            this.copyrightLab.TabIndex = 44;
            this.copyrightLab.Text = "Copyright C VMT 2016\r\n\r\nVMT Engine\r\nM.m.B.RRR (YYWWRRp)";
            this.copyrightLab.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.copyrightLab.Click += new System.EventHandler(this.copyrightLab_Click);
            // 
            // infoLab
            // 
            this.infoLab.AutoSize = true;
            this.infoLab.BackColor = System.Drawing.Color.Transparent;
            this.infoLab.Depth = 0;
            this.infoLab.Font = new System.Drawing.Font("Roboto", 11F);
            this.infoLab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.infoLab.Location = new System.Drawing.Point(14, 6);
            this.infoLab.MouseState = MaterialSkin.MouseState.HOVER;
            this.infoLab.Name = "infoLab";
            this.infoLab.Size = new System.Drawing.Size(380, 627);
            this.infoLab.TabIndex = 43;
            this.infoLab.Text = resources.GetString("infoLab.Text");
            this.infoLab.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // devset
            // 
            this.devset.AutoScroll = true;
            this.devset.BackColor = System.Drawing.Color.White;
            this.devset.Controls.Add(this.hideSplashes);
            this.devset.Controls.Add(this.forceStarters);
            this.devset.Controls.Add(this.hideDevAvailable);
            this.devset.Controls.Add(this.verNprofVisible);
            this.devset.Controls.Add(this.devCheck);
            this.devset.Controls.Add(this.materialLabel2);
            this.devset.Location = new System.Drawing.Point(4, 22);
            this.devset.Name = "devset";
            this.devset.Size = new System.Drawing.Size(696, 323);
            this.devset.TabIndex = 5;
            this.devset.Text = "devset";
            // 
            // hideSplashes
            // 
            this.hideSplashes.AutoSize = true;
            this.hideSplashes.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.hideSplashes.Depth = 0;
            this.hideSplashes.Font = new System.Drawing.Font("Roboto", 10F);
            this.hideSplashes.Location = new System.Drawing.Point(11, 65);
            this.hideSplashes.Margin = new System.Windows.Forms.Padding(0);
            this.hideSplashes.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hideSplashes.MouseState = MaterialSkin.MouseState.HOVER;
            this.hideSplashes.Name = "hideSplashes";
            this.hideSplashes.Ripple = true;
            this.hideSplashes.Size = new System.Drawing.Size(117, 30);
            this.hideSplashes.TabIndex = 69;
            this.hideSplashes.Text = "Hide splashes";
            this.hideSplashes.UseVisualStyleBackColor = true;
            this.hideSplashes.Click += new System.EventHandler(this.hideSplashes_Click_1);
            // 
            // forceStarters
            // 
            this.forceStarters.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.forceStarters.Depth = 0;
            this.forceStarters.Location = new System.Drawing.Point(11, 129);
            this.forceStarters.MouseState = MaterialSkin.MouseState.HOVER;
            this.forceStarters.Name = "forceStarters";
            this.forceStarters.Primary = true;
            this.forceStarters.Size = new System.Drawing.Size(164, 34);
            this.forceStarters.TabIndex = 67;
            this.forceStarters.Text = "Reload form";
            this.forceStarters.UseVisualStyleBackColor = true;
            this.forceStarters.Click += new System.EventHandler(this.forceStarters_Click);
            // 
            // hideDevAvailable
            // 
            this.hideDevAvailable.AutoSize = true;
            this.hideDevAvailable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.hideDevAvailable.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.hideDevAvailable.Depth = 0;
            this.hideDevAvailable.Location = new System.Drawing.Point(11, 172);
            this.hideDevAvailable.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.hideDevAvailable.MouseState = MaterialSkin.MouseState.HOVER;
            this.hideDevAvailable.Name = "hideDevAvailable";
            this.hideDevAvailable.Primary = false;
            this.hideDevAvailable.Size = new System.Drawing.Size(163, 36);
            this.hideDevAvailable.TabIndex = 61;
            this.hideDevAvailable.Text = "Disable dev settings";
            this.hideDevAvailable.UseVisualStyleBackColor = true;
            this.hideDevAvailable.Click += new System.EventHandler(this.hideDevAvailable_Click_1);
            // 
            // verNprofVisible
            // 
            this.verNprofVisible.AutoSize = true;
            this.verNprofVisible.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.verNprofVisible.Depth = 0;
            this.verNprofVisible.Font = new System.Drawing.Font("Roboto", 10F);
            this.verNprofVisible.Location = new System.Drawing.Point(11, 35);
            this.verNprofVisible.Margin = new System.Windows.Forms.Padding(0);
            this.verNprofVisible.MouseLocation = new System.Drawing.Point(-1, -1);
            this.verNprofVisible.MouseState = MaterialSkin.MouseState.HOVER;
            this.verNprofVisible.Name = "verNprofVisible";
            this.verNprofVisible.Ripple = true;
            this.verNprofVisible.Size = new System.Drawing.Size(216, 30);
            this.verNprofVisible.TabIndex = 57;
            this.verNprofVisible.Text = "Hide version and profile labels";
            this.verNprofVisible.UseVisualStyleBackColor = true;
            this.verNprofVisible.Click += new System.EventHandler(this.verNprofVisible_Click_1);
            // 
            // devCheck
            // 
            this.devCheck.AutoSize = true;
            this.devCheck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.devCheck.Depth = 0;
            this.devCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.devCheck.Location = new System.Drawing.Point(11, 95);
            this.devCheck.Margin = new System.Windows.Forms.Padding(0);
            this.devCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.devCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.devCheck.Name = "devCheck";
            this.devCheck.Ripple = true;
            this.devCheck.Size = new System.Drawing.Size(160, 30);
            this.devCheck.TabIndex = 58;
            this.devCheck.Text = "Extra developer tools";
            this.devCheck.UseVisualStyleBackColor = true;
            this.devCheck.Click += new System.EventHandler(this.devCheck_Click_1);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(87, 6);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(503, 19);
            this.materialLabel2.TabIndex = 30;
            this.materialLabel2.Text = "Those options are intended for debug purposes, use only at your own risk.";
            this.materialLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // preferences
            // 
            this.preferences.BackColor = System.Drawing.Color.White;
            this.preferences.Controls.Add(this.blueAndGrey);
            this.preferences.Controls.Add(this.materialLabel10);
            this.preferences.Controls.Add(this.greyOnly);
            this.preferences.Controls.Add(this.NoDarkModeCheck);
            this.preferences.Controls.Add(this.AlwaysDarkModeCheck);
            this.preferences.Controls.Add(this.materialLabel9);
            this.preferences.Controls.Add(this.hideSplash);
            this.preferences.Controls.Add(this.deskShortCheck);
            this.preferences.Location = new System.Drawing.Point(4, 22);
            this.preferences.Name = "preferences";
            this.preferences.Size = new System.Drawing.Size(696, 323);
            this.preferences.TabIndex = 6;
            this.preferences.Text = "preferences";
            // 
            // blueAndGrey
            // 
            this.blueAndGrey.AutoSize = true;
            this.blueAndGrey.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.blueAndGrey.Depth = 0;
            this.blueAndGrey.Font = new System.Drawing.Font("Roboto", 10F);
            this.blueAndGrey.Location = new System.Drawing.Point(9, 225);
            this.blueAndGrey.Margin = new System.Windows.Forms.Padding(0);
            this.blueAndGrey.MouseLocation = new System.Drawing.Point(-1, -1);
            this.blueAndGrey.MouseState = MaterialSkin.MouseState.HOVER;
            this.blueAndGrey.Name = "blueAndGrey";
            this.blueAndGrey.Ripple = true;
            this.blueAndGrey.Size = new System.Drawing.Size(99, 30);
            this.blueAndGrey.TabIndex = 58;
            this.blueAndGrey.TabStop = true;
            this.blueAndGrey.Text = "Blue + Grey";
            this.blueAndGrey.UseVisualStyleBackColor = true;
            this.blueAndGrey.Click += new System.EventHandler(this.blueAndGrey_Click);
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(5, 176);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(55, 19);
            this.materialLabel10.TabIndex = 57;
            this.materialLabel10.Text = "Theme";
            this.materialLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // greyOnly
            // 
            this.greyOnly.AutoSize = true;
            this.greyOnly.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.greyOnly.Depth = 0;
            this.greyOnly.Font = new System.Drawing.Font("Roboto", 10F);
            this.greyOnly.Location = new System.Drawing.Point(9, 195);
            this.greyOnly.Margin = new System.Windows.Forms.Padding(0);
            this.greyOnly.MouseLocation = new System.Drawing.Point(-1, -1);
            this.greyOnly.MouseState = MaterialSkin.MouseState.HOVER;
            this.greyOnly.Name = "greyOnly";
            this.greyOnly.Ripple = true;
            this.greyOnly.Size = new System.Drawing.Size(86, 30);
            this.greyOnly.TabIndex = 56;
            this.greyOnly.TabStop = true;
            this.greyOnly.Text = "Grey only";
            this.greyOnly.UseVisualStyleBackColor = true;
            this.greyOnly.Click += new System.EventHandler(this.greyOnly_Click);
            // 
            // NoDarkModeCheck
            // 
            this.NoDarkModeCheck.AutoSize = true;
            this.NoDarkModeCheck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.NoDarkModeCheck.Depth = 0;
            this.NoDarkModeCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.NoDarkModeCheck.Location = new System.Drawing.Point(9, 131);
            this.NoDarkModeCheck.Margin = new System.Windows.Forms.Padding(0);
            this.NoDarkModeCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.NoDarkModeCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.NoDarkModeCheck.Name = "NoDarkModeCheck";
            this.NoDarkModeCheck.Ripple = true;
            this.NoDarkModeCheck.Size = new System.Drawing.Size(156, 30);
            this.NoDarkModeCheck.TabIndex = 55;
            this.NoDarkModeCheck.Text = "Don\'t use dark mode";
            this.NoDarkModeCheck.UseVisualStyleBackColor = true;
            this.NoDarkModeCheck.Click += new System.EventHandler(this.NoDarkModeCheck_Click);
            // 
            // AlwaysDarkModeCheck
            // 
            this.AlwaysDarkModeCheck.AutoSize = true;
            this.AlwaysDarkModeCheck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.AlwaysDarkModeCheck.Depth = 0;
            this.AlwaysDarkModeCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.AlwaysDarkModeCheck.Location = new System.Drawing.Point(9, 101);
            this.AlwaysDarkModeCheck.Margin = new System.Windows.Forms.Padding(0);
            this.AlwaysDarkModeCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.AlwaysDarkModeCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.AlwaysDarkModeCheck.Name = "AlwaysDarkModeCheck";
            this.AlwaysDarkModeCheck.Ripple = true;
            this.AlwaysDarkModeCheck.Size = new System.Drawing.Size(168, 30);
            this.AlwaysDarkModeCheck.TabIndex = 54;
            this.AlwaysDarkModeCheck.Text = "Always use dark mode";
            this.AlwaysDarkModeCheck.UseVisualStyleBackColor = true;
            this.AlwaysDarkModeCheck.Click += new System.EventHandler(this.AlwaysDarkModeCheck_Click);
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(5, 82);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(82, 19);
            this.materialLabel9.TabIndex = 53;
            this.materialLabel9.Text = "Dark Mode";
            this.materialLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hideSplash
            // 
            this.hideSplash.AutoSize = true;
            this.hideSplash.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.hideSplash.Depth = 0;
            this.hideSplash.Font = new System.Drawing.Font("Roboto", 10F);
            this.hideSplash.Location = new System.Drawing.Point(9, 40);
            this.hideSplash.Margin = new System.Windows.Forms.Padding(0);
            this.hideSplash.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hideSplash.MouseState = MaterialSkin.MouseState.HOVER;
            this.hideSplash.Name = "hideSplash";
            this.hideSplash.Ripple = true;
            this.hideSplash.Size = new System.Drawing.Size(117, 30);
            this.hideSplash.TabIndex = 52;
            this.hideSplash.Text = "Hide splashes";
            this.hideSplash.UseVisualStyleBackColor = true;
            this.hideSplash.Click += new System.EventHandler(this.hideSplash_Click);
            // 
            // deskShortCheck
            // 
            this.deskShortCheck.AutoSize = true;
            this.deskShortCheck.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.deskShortCheck.Depth = 0;
            this.deskShortCheck.Font = new System.Drawing.Font("Roboto", 10F);
            this.deskShortCheck.Location = new System.Drawing.Point(9, 10);
            this.deskShortCheck.Margin = new System.Windows.Forms.Padding(0);
            this.deskShortCheck.MouseLocation = new System.Drawing.Point(-1, -1);
            this.deskShortCheck.MouseState = MaterialSkin.MouseState.HOVER;
            this.deskShortCheck.Name = "deskShortCheck";
            this.deskShortCheck.Ripple = true;
            this.deskShortCheck.Size = new System.Drawing.Size(178, 30);
            this.deskShortCheck.TabIndex = 51;
            this.deskShortCheck.Text = "Create desktop shortcut";
            this.deskShortCheck.UseVisualStyleBackColor = true;
            this.deskShortCheck.Click += new System.EventHandler(this.deskShortCheck_Click);
            // 
            // updateTab
            // 
            this.updateTab.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.updateTab.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.updateTab.Location = new System.Drawing.Point(4, 22);
            this.updateTab.Name = "updateTab";
            this.updateTab.Size = new System.Drawing.Size(756, 349);
            this.updateTab.TabIndex = 3;
            this.updateTab.Text = "Updates";
            // 
            // TabSelector
            // 
            this.TabSelector.BaseTabControl = this.Tab;
            this.TabSelector.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.TabSelector.Depth = 0;
            this.TabSelector.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TabSelector.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TabSelector.Location = new System.Drawing.Point(0, 64);
            this.TabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.TabSelector.Name = "TabSelector";
            this.TabSelector.Size = new System.Drawing.Size(712, 23);
            this.TabSelector.TabIndex = 2;
            this.TabSelector.Text = "materialTabSelector1";
            this.TabSelector.Click += new System.EventHandler(this.TabSelector_Click);
            // 
            // ver_label
            // 
            this.ver_label.AutoSize = true;
            this.ver_label.BackColor = System.Drawing.Color.Transparent;
            this.ver_label.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ver_label.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ver_label.ForeColor = System.Drawing.Color.White;
            this.ver_label.Location = new System.Drawing.Point(110, 32);
            this.ver_label.Name = "ver_label";
            this.ver_label.Size = new System.Drawing.Size(53, 15);
            this.ver_label.TabIndex = 4;
            this.ver_label.Text = "ver_label";
            this.ver_label.Click += new System.EventHandler(this.ver_label_Click);
            // 
            // splash
            // 
            this.splash.AutoSize = true;
            this.splash.BackColor = System.Drawing.Color.Transparent;
            this.splash.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.splash.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.splash.ForeColor = System.Drawing.Color.White;
            this.splash.Location = new System.Drawing.Point(247, 34);
            this.splash.Name = "splash";
            this.splash.Size = new System.Drawing.Size(383, 25);
            this.splash.TabIndex = 9;
            this.splash.Text = "splashsplashsplashsplashsplashsplashsplash";
            this.splash.MouseDown += new System.Windows.Forms.MouseEventHandler(this.splash_MouseDown);
            this.splash.MouseMove += new System.Windows.Forms.MouseEventHandler(this.splash_MouseMove);
            this.splash.MouseUp += new System.Windows.Forms.MouseEventHandler(this.splash_MouseUp);
            // 
            // ModName
            // 
            this.ModName.AutoSize = true;
            this.ModName.BackColor = System.Drawing.Color.Transparent;
            this.ModName.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ModName.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ModName.ForeColor = System.Drawing.Color.White;
            this.ModName.Location = new System.Drawing.Point(110, 47);
            this.ModName.Name = "ModName";
            this.ModName.Size = new System.Drawing.Size(32, 15);
            this.ModName.TabIndex = 10;
            this.ModName.Text = "mod";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(712, 462);
            this.Controls.Add(this.ModName);
            this.Controls.Add(this.splash);
            this.Controls.Add(this.ver_label);
            this.Controls.Add(this.TabSelector);
            this.Controls.Add(this.Tab);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(764, 462);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VMT Engine";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Home_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Tab.ResumeLayout(false);
            this.homeTab.ResumeLayout(false);
            this.gPanel.ResumeLayout(false);
            this.gPanel.PerformLayout();
            this.testPanel.ResumeLayout(false);
            this.playContext.ResumeLayout(false);
            this.logButContext.ResumeLayout(false);
            this.webPanel.ResumeLayout(false);
            this.gameTab.ResumeLayout(false);
            this.game.ResumeLayout(false);
            this.basic.ResumeLayout(false);
            this.basic.PerformLayout();
            this.tirePanel.ResumeLayout(false);
            this.tirePanel.PerformLayout();
            this.plr.ResumeLayout(false);
            this.plr.PerformLayout();
            this.quick_chat.ResumeLayout(false);
            this.quick_chat.PerformLayout();
            this.vmtTab.ResumeLayout(false);
            this.vmtPan.ResumeLayout(false);
            this.vmtP.ResumeLayout(false);
            this.classic.ResumeLayout(false);
            this.classic.PerformLayout();
            this._15.ResumeLayout(false);
            this._15.PerformLayout();
            this._16.ResumeLayout(false);
            this._16.PerformLayout();
            this._17.ResumeLayout(false);
            this._17.PerformLayout();
            this.settingsTab.ResumeLayout(false);
            this.settings.ResumeLayout(false);
            this.main.ResumeLayout(false);
            this.games.ResumeLayout(false);
            this.games.PerformLayout();
            this.updates.ResumeLayout(false);
            this.updates.PerformLayout();
            this.informations.ResumeLayout(false);
            this.informations.PerformLayout();
            this.devset.ResumeLayout(false);
            this.devset.PerformLayout();
            this.preferences.ResumeLayout(false);
            this.preferences.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabControl Tab;
        private System.Windows.Forms.TabPage homeTab;
        private System.Windows.Forms.TabPage gameTab;
        private MaterialSkin.Controls.MaterialTabSelector TabSelector;
        private System.Windows.Forms.TabPage settingsTab;
        private System.Windows.Forms.TabPage updateTab;
        private System.Windows.Forms.Label ver_label;
        private MaterialSkin.Controls.MaterialRaisedButton changelogButton;
        private MaterialSkin.Controls.MaterialRaisedButton fupdButton;
        private System.Windows.Forms.Panel testPanel;
        private System.Windows.Forms.Button chatTile;
        private System.Windows.Forms.Panel gPanel;
        private System.Windows.Forms.Button confTile;
        private System.Windows.Forms.Button logTile;
        private System.Windows.Forms.Button bugTrackTile;
        private System.Windows.Forms.Button plrTile;
        private System.Windows.Forms.Button telemetryTile;
        private System.Windows.Forms.Button gamedirTile;
        private System.Windows.Forms.Panel webPanel;
        private System.Windows.Forms.WebBrowser htmlView;
        private System.Windows.Forms.Button testUpdateWindowTile;
        private System.Windows.Forms.Button testWelcomeTile;
        private System.Windows.Forms.Label splash;
        private MaterialSkin.Controls.MaterialTabControl settings;
        private System.Windows.Forms.TabPage main;
        private System.Windows.Forms.Button settingsDev;
        private System.Windows.Forms.Button settingAbout;
        private System.Windows.Forms.Button settingUpdate;
        private System.Windows.Forms.Button settingGames;
        private System.Windows.Forms.TabPage games;
        private MaterialSkin.Controls.MaterialCheckBox turnOffCheck;
        private MaterialSkin.Controls.MaterialCheckBox showLogCheck;
        private System.Windows.Forms.TabPage updates;
        private MaterialSkin.Controls.MaterialCheckBox autoUpdCheck;
        private System.Windows.Forms.TabPage informations;
        private MaterialSkin.Controls.MaterialLabel copyrightLab;
        private MaterialSkin.Controls.MaterialLabel infoLab;
        private System.Windows.Forms.TabPage devset;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialRaisedButton forceStarters;
        private MaterialSkin.Controls.MaterialFlatButton hideDevAvailable;
        private MaterialSkin.Controls.MaterialCheckBox verNprofVisible;
        private MaterialSkin.Controls.MaterialCheckBox devCheck;
        private MaterialSkin.Controls.MaterialRaisedButton websiteNewBut;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialCheckBox antiAliasignF1CLab;
        private MaterialSkin.Controls.MaterialCheckBox windowedF1CLab;
        private MaterialSkin.Controls.MaterialCheckBox vsyncF1CLab;
        private MaterialSkin.Controls.MaterialLabel settingsF1CLab;
        private MaterialSkin.Controls.MaterialCheckBox mphTelF1CLab;
        private System.Windows.Forms.Panel tirePanel;
        private System.Windows.Forms.Label ModName;
        private System.Windows.Forms.TabPage vmtTab;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private System.Windows.Forms.ComboBox tireSelect;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialRadioButton prev;
        private MaterialSkin.Controls.MaterialRadioButton stable;
        private MaterialSkin.Controls.MaterialRaisedButton vmtYTbut;
        private MaterialSkin.Controls.MaterialRaisedButton vmtFBBut;
        private MaterialSkin.Controls.MaterialLabel supportedWarning;
        private System.Windows.Forms.Panel vmtPan;
        private MaterialSkin.Controls.MaterialTabControl vmtP;
        private System.Windows.Forms.TabPage _15;
        private System.Windows.Forms.TabPage _16;
        private System.Windows.Forms.TabPage _17;
        private System.Windows.Forms.TabPage classic;
        private MaterialSkin.Controls.MaterialCheckBox swapKvyVer;
        private MaterialSkin.Controls.MaterialCheckBox replaceAloVan;
        private MaterialSkin.Controls.MaterialCheckBox replaceAloBut;
        private MaterialSkin.Controls.MaterialCheckBox rbrCamo15;
        private MaterialSkin.Controls.MaterialCheckBox bianchiManor15;
        private MaterialSkin.Controls.MaterialCheckBox rossiManor15;
        private MaterialSkin.Controls.MaterialCheckBox magnussenMcl15;
        private MaterialSkin.Controls.MaterialCheckBox hideSplashes;
        private System.Windows.Forms.Button preferencesSettings;
        private System.Windows.Forms.TabPage preferences;
        private System.Windows.Forms.ContextMenuStrip playContext;
        private System.Windows.Forms.ToolStripMenuItem bugTrackerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playAndShutdownVMTEngineToolStripMenuItem;
        private MaterialSkin.Controls.MaterialCheckBox deskShortCheck;
        private System.Windows.Forms.ContextMenuStrip logButContext;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem setCompatibilityModeToolStripMenuItem;
        private MaterialSkin.Controls.MaterialCheckBox vidF1c;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.ComboBox langSelect;
        private System.Windows.Forms.Button playTile;
        private System.Windows.Forms.ToolStripMenuItem oNToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem didntHelpedToolStripMenuItem;
        private MaterialSkin.Controls.MaterialRadioButton blueAndGrey;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialRadioButton greyOnly;
        private MaterialSkin.Controls.MaterialCheckBox NoDarkModeCheck;
        private MaterialSkin.Controls.MaterialCheckBox AlwaysDarkModeCheck;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialLabel urOffline;
        private MaterialSkin.Controls.MaterialTabControl game;
        private System.Windows.Forms.TabPage basic;
        private System.Windows.Forms.TabPage plr;
        private System.Windows.Forms.TabPage quick_chat;
        private MaterialSkin.Controls.MaterialLabel saveLab;
        private MaterialSkin.Controls.MaterialLabel tenLab;
        private MaterialSkin.Controls.MaterialLabel nineLab;
        private MaterialSkin.Controls.MaterialLabel eightLab;
        private MaterialSkin.Controls.MaterialLabel sevenLab;
        private MaterialSkin.Controls.MaterialLabel sixLab;
        private MaterialSkin.Controls.MaterialLabel fiveLab;
        private MaterialSkin.Controls.MaterialLabel fourLab;
        private MaterialSkin.Controls.MaterialLabel threeLab;
        private MaterialSkin.Controls.MaterialLabel twoLab;
        private MaterialSkin.Controls.MaterialLabel oneLab;
        private MaterialSkin.Controls.MaterialSingleLineTextField ten;
        private MaterialSkin.Controls.MaterialSingleLineTextField nine;
        private MaterialSkin.Controls.MaterialSingleLineTextField eight;
        private MaterialSkin.Controls.MaterialSingleLineTextField seven;
        private MaterialSkin.Controls.MaterialSingleLineTextField six;
        private MaterialSkin.Controls.MaterialSingleLineTextField five;
        private MaterialSkin.Controls.MaterialSingleLineTextField four;
        private MaterialSkin.Controls.MaterialSingleLineTextField three;
        private MaterialSkin.Controls.MaterialSingleLineTextField two;
        private MaterialSkin.Controls.MaterialSingleLineTextField one;
        private System.Windows.Forms.ComboBox QuickChatProfiles;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private MaterialSkin.Controls.MaterialRaisedButton saveBut;
        private MaterialSkin.Controls.MaterialRaisedButton howToBut;
        private System.Windows.Forms.ComboBox ProfileChatProfiles;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialCheckBox ReconnaissanceLap;
        private MaterialSkin.Controls.MaterialCheckBox FormationLap;
        private MaterialSkin.Controls.MaterialCheckBox PreventAI;
        private MaterialSkin.Controls.MaterialCheckBox CompareTimeToFirstDriver;
        private MaterialSkin.Controls.MaterialCheckBox ShowChat;
        private MaterialSkin.Controls.MaterialCheckBox ExtraConfigurableButtons;
        private MaterialSkin.Controls.MaterialCheckBox ShowYourCar;
        private MaterialSkin.Controls.MaterialCheckBox ClosedMP;
        private MaterialSkin.Controls.MaterialRaisedButton OpenInNotepadPLR;
        private MaterialSkin.Controls.MaterialCheckBox hideSplash;
        private MaterialSkin.Controls.MaterialRaisedButton GitBtn;
        private System.Windows.Forms.Button TestMsgBoxBut;
        private System.Windows.Forms.Button BtnYesNo;
        private MaterialSkin.Controls.MaterialCheckBox TrackLoadCommentary;
        private MaterialSkin.Controls.MaterialCheckBox LCDMode;
        private MaterialSkin.Controls.MaterialCheckBox MovingSteeringWheeleck;
        private MaterialSkin.Controls.MaterialLabel langLab;
        private MaterialSkin.Controls.MaterialFlatButton DownloadLangPackBut;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
    }
}

