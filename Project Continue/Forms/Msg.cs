﻿using System;
using System.Drawing;
using System.Media;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Project_Continue
{
    public partial class Msg : MaterialForm
    {
        public Msg()
        {
            InitializeComponent();
        }

        static string Message { get; set; }
        static string Title { get; set; }

        public static void Show(string Msg, string title, MsgButtons btns = MsgButtons.OK)
        {
            Message = Msg;
            Title = title;
            Msg f = new Msg();

            if (btns == MsgButtons.YesNo)
            {
                f.BtnYes.Visible = true;
                f.BtnNo.Visible = true;
            }
            else if (btns == MsgButtons.OK)
            {
                f.BtnOK.Visible = true;
            }

            f.Text = title;
            f.ShowDialog();
        }

        private void Msg_Load(object sender, EventArgs e)
        {
            Yes = No = false;
            SystemSounds.Exclamation.Play();
            Label.AutoSize = true;
            Label.MaximumSize = new Size(309, 0);
            Label.Text = Message;
            Label.Location = new Point((Width - Label.Width) / 2, 86);

            Width = 395;
            Height = Label.Height + BtnOK.Height + 117;
            BtnOK.Location = new Point((Width - BtnOK.Width) / 2, Label.Location.Y + Label.Height + 20);
            BtnYes.Location = new Point((Width - BtnYes.Width) / 2 - BtnNo.Width / 2 - 5, BtnOK.Location.Y);
            BtnNo.Location = new Point(BtnYes.Location.X + BtnYes.Width + 5, BtnOK.Location.Y);
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        public static bool Yes { get; set; }
        public static bool No { get; set; }

        private void BtnYes_Click(object sender, EventArgs e)
        {          
            Yes = true;
            Close();
        }

        private void BtnNo_Click(object sender, EventArgs e)
        {
            No = true;
            Close(); 
        }
    }

    [Flags]
    public enum MsgButtons
    {
        OK = 1,
        YesNo = 2 
    }
}
