﻿using System;
using System.Windows.Forms;
using System.IO;
using MaterialSkin.Controls;
using MaterialSkin;
using Microsoft.Win32;
using System.Net;

namespace SimPTUpdateManager
{
    public partial class Home : MaterialForm
    {   
        public string ver = "18420";
        string Version = Application.ProductVersion;

        public Home()
        {
            InitializeComponent();
            RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\VMT\VMT Engine\Updater");
            if (Key == null)
            {
                MessageBox.Show("Registry Key is empty", "Error", MessageBoxButtons.OK);
                Application.Exit();
                return;
            }
            if (Key.GetValue("UpdaterUpdateOnly") != null && Key.GetValue("UpdaterUpdateOnly").Equals("true"))
            {
                WindowState = FormWindowState.Minimized;
                Hide();
            }
            else
            {
                ShowInTaskbar = true;
            }

            ver_lab.Text = String.Format(Version);

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue600, Primary.Blue700, Primary.Blue200, Accent.LightBlue200, TextShade.WHITE);           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string backup = @"VMT Engine.backup.exe";
            string updater = @"VMT Engine.exe";
            string update = @"update\VMT Engine.exe";
            RegistryKey Key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\VMT\VMT Engine\Updater", true);

            /*
            if (Key.GetValue("UpdaterUpdateOnly").ToString().Contains("true"))
            {
                if (File.Exists(@"update\updater.txt"))
                    File.Delete(@"update\updater.txt");

                Key.SetValue("UpdaterUpdateOnly", "false");
                DownloadFile("http://vmt.kkmr.pl/vmtengine/updates/updater.txt", @"update\updater.txt");

                if (File.ReadAllText(@"update\updater.txt").Contains(ver))
                {
                    Application.Exit();
                }
                else
                {
                    Key.SetValue("UpdaterUpdateNow", "true");
                    Application.Exit();
                }
                return;
            }
            else
            {
            */
                if (!File.Exists(update))
                {
                    MessageBox.Show("This program can be only executed by VMT Engine. You are not allowed to run it separately.", "Forbidden", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Application.Exit();
                }
                else
                {
                    if (File.Exists(backup))
                    {
                        File.Delete(backup);
                    }

                    File.Copy(updater, backup);
                    File.Delete(updater);
                    File.Move(update, updater);
                    File.Delete(update);
                }
            //}
            Application.Exit();
        }

        static void DownloadFile(string URL, string Directory)
        {
            try
            {
                using (WebClient Dnl = new WebClient())
                {
                    Dnl.DownloadFile(URL, Directory);
                    Dnl.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error has occured: " + Environment.NewLine + Environment.NewLine + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }     
    }
}
