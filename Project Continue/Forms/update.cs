﻿using System;
using System.Windows.Forms;
using MaterialSkin.Controls;
using System.Diagnostics;
using System.Drawing;

namespace Project_Continue
{
    public partial class update : MaterialForm
    {
        public update()
        {
            InitializeComponent();
        }

        private void update_Load(object sender, EventArgs e)
        {
            backBut.Location = new Point((this.Width - backBut.Width) / 2, 339);
            updateNowBut.Location = new Point((this.Width - updateNowBut.Width) / 2 - updateNowBut.Width / 2 - 2, 296);
            learnMoreBut.Location = new Point((this.Width - learnMoreBut.Width) / 2 + learnMoreBut.Width / 2 + 2, 296);

            if (Misc.IsDarkModeOn)
            {
                this.BackColor = Color.Black;
                backBut.BackColor = Color.DarkGray;
            }

            if (!Misc.IsDarkModeOn)
            {
                backBut.BackColor = Color.White;
            }

            htmlView.IsWebBrowserContextMenuEnabled = false;
            htmlView.AllowWebBrowserDrop = true;
            ((Control)htmlView).Enabled = true;
            htmlView.ScrollBarsEnabled = true;                

            if (Settings.FastRing)
            {
               // htmlView.Url = new Uri(String.Format("http://vmt.kkmr.pl/vmtengine/changprev.html"));
            }
            else
            {
                //htmlView.Url = new Uri(String.Format("http://vmt.kkmr.pl/vmtengine/changelog.html"));
            }
        }

        private void materialRaisedButton19_Click(object sender, EventArgs e)
        {
            UpdateDownload.DownloadUpdate();
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            Hide();
            Close();
        }
        
        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            ExternalLinks.Run("https://gitlab.com/aathlon/vmt-engine/blob/master/CHANGELOG.md");
        }
    }
}
