﻿using System.Reflection;
using System.Windows.Forms;

namespace Project_Continue
{
    class Version
    {
        public static string Full = Application.ProductVersion;

        // Major
        public static string Major
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();
            }
        }

        // Minor
        public static string Minor
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();
            }
        }


        // Build
        public static string Build
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();
            }
        }

        // RevVer
        public static string Revision
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Revision.ToString();
            }
        }

        //Copyright
        public static string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }
    }
}
