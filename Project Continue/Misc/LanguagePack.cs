﻿using System.IO;
using Ionic.Zip;

namespace Project_Continue
{
    class LanguagePack
    {
        public static void DownloadPack()
        {
            return;
            DownloadFile.GetFile("http://vmt.kkmr.pl/f1c/LangPack.zip", @"update\langpack.zip");
            ExtractFileToDirectory(@"update\langpack.zip", "Options");
            Settings.LanguagePackDownloaded = true;
            Msg.Show("Success!", "Done");
        }

        static void ExtractFileToDirectory(string zipFileName, string outputDirectory)
        {
            ZipFile zip = ZipFile.Read(zipFileName);
            Directory.CreateDirectory(outputDirectory);
            foreach (ZipEntry e in zip)
            {
                 e.Extract(outputDirectory, ExtractExistingFileAction.OverwriteSilently);
            }
            zip.Dispose();
            File.Delete(@"update\langpack.zip");
        }
    }
}
