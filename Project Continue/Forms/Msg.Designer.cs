﻿namespace Project_Continue
{
    partial class Msg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label = new MaterialSkin.Controls.MaterialLabel();
            this.BtnOK = new MaterialSkin.Controls.MaterialRaisedButton();
            this.BtnYes = new MaterialSkin.Controls.MaterialRaisedButton();
            this.BtnNo = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // Label
            // 
            this.Label.AutoSize = true;
            this.Label.BackColor = System.Drawing.Color.Transparent;
            this.Label.Depth = 0;
            this.Label.Font = new System.Drawing.Font("Roboto", 11F);
            this.Label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Label.Location = new System.Drawing.Point(39, 86);
            this.Label.MouseState = MaterialSkin.MouseState.HOVER;
            this.Label.Name = "Label";
            this.Label.Size = new System.Drawing.Size(309, 19);
            this.Label.TabIndex = 0;
            this.Label.Text = "texttexttexttexttexttexttexttexttexttexttexttext";
            this.Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // BtnOK
            // 
            this.BtnOK.BackColor = System.Drawing.Color.Transparent;
            this.BtnOK.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.BtnOK.Depth = 0;
            this.BtnOK.Location = new System.Drawing.Point(144, 244);
            this.BtnOK.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Primary = true;
            this.BtnOK.Size = new System.Drawing.Size(106, 34);
            this.BtnOK.TabIndex = 26;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = false;
            this.BtnOK.Visible = false;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // BtnYes
            // 
            this.BtnYes.BackColor = System.Drawing.Color.Transparent;
            this.BtnYes.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.BtnYes.Depth = 0;
            this.BtnYes.Location = new System.Drawing.Point(32, 244);
            this.BtnYes.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnYes.Name = "BtnYes";
            this.BtnYes.Primary = true;
            this.BtnYes.Size = new System.Drawing.Size(106, 34);
            this.BtnYes.TabIndex = 27;
            this.BtnYes.Text = "Yes";
            this.BtnYes.UseVisualStyleBackColor = false;
            this.BtnYes.Visible = false;
            this.BtnYes.Click += new System.EventHandler(this.BtnYes_Click);
            // 
            // BtnNo
            // 
            this.BtnNo.BackColor = System.Drawing.Color.Transparent;
            this.BtnNo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.BtnNo.Depth = 0;
            this.BtnNo.Location = new System.Drawing.Point(256, 244);
            this.BtnNo.MouseState = MaterialSkin.MouseState.HOVER;
            this.BtnNo.Name = "BtnNo";
            this.BtnNo.Primary = true;
            this.BtnNo.Size = new System.Drawing.Size(106, 34);
            this.BtnNo.TabIndex = 28;
            this.BtnNo.Text = "No";
            this.BtnNo.UseVisualStyleBackColor = false;
            this.BtnNo.Visible = false;
            this.BtnNo.Click += new System.EventHandler(this.BtnNo_Click);
            // 
            // Msg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(395, 299);
            this.ControlBox = false;
            this.Controls.Add(this.BtnNo);
            this.Controls.Add(this.BtnYes);
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.Label);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Msg";
            this.Sizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Msg";
            this.Load += new System.EventHandler(this.Msg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel Label;
        private MaterialSkin.Controls.MaterialRaisedButton BtnOK;
        private MaterialSkin.Controls.MaterialRaisedButton BtnYes;
        private MaterialSkin.Controls.MaterialRaisedButton BtnNo;
    }
}